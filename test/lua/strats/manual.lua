strat.name = "manual"
strat.team = 1
strat.required_peripherals = {"gamepad:15"}

strat.cache = 0

function strat:init ()
    local gamepad = self["gamepad:15"]
    gamepad:updateData()
    print(string.format("a=%d",gamepad.a))
end

function strat:main ()
    local gamepad = self["gamepad:15"]
    local run = true
    local i = 0

    while run do
        gamepad:updateData()
        self:sleep(100)
        --self:log("Bip!")
        self:log(" ".. gamepad.leftJoystick[1])
        if i == 500 then
            run = false
        end
        i=i+1
    end
end

function strat:onEnd ()
        self:log("Bye!")
end