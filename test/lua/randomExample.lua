function setup (peripheral)
    
    function peripheral:test ()
        self.a = 12
    end

    function peripheral:print ()
        if self.a == nil then
            print("No a")
        else
            print(self.a)
        end
    end

    return peripheral

end


a = {}

setup(a)
a:test()

function wrap (a) 

    b = {}
    i = 0

    function y (name)
        print("Appel de " .. name)
        i = i + 1
        if i == 2 then
            print("yo")
            error("Reached max calls")
        end
    end

    for name, f in pairs(a) do
        
        if type(f) == 'function' then 
            b[name] = function (...)
                y(name)
                f(...)
            end
        end

    end

    return b

end

c = wrap(a)

-- No message
a:test()
a:print()

-- Message
c:test()
-- c:print()
-- c:print()

-- Test buffer 
buf = HermesBuffer.new()
buf:add_argument("Le nombre est : ", 12)
data = buf:data()
for key, value in pairs(data) do
    print(tostring(key) .. "> " .. tostring(value))
end