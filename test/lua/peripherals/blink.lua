function peripheral:binary_display (number)
    
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 1, false)
    buffer:add_int8(number)

    self:send(buffer)

end

function peripheral:rgb_color (r, g, b)

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 2, false)
    buffer:add_int8(r,g,b)

    self:send(buffer)

end

function peripheral:read_binary () 

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 3, false)

    local result = self:request(buffer)

    if result == nil then
        self:log("Binary read request failed")
        return -1
    end

    local data = result:data()
    if #data < 1 then
        self:log("Invalid binary read data")
        return -2
    end

    return data[0]

end

function peripheral:read_user () 

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 4, false)

    local result = self:request(buffer)

    if result == nil then
        self:log("Button press value request failed")
        return -1
    end

    local data = result:data()
    if #data < 1 then
        self:log("Invalid button press value data")
        return -2
    end

    return data[0]

end

function peripheral:read_voltage () 

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 5, false)

    local result = self:request(buffer)

    if result == nil then
        self:log("Voltage request failed")
        return -1
    end

    local data = result:data()
    if #data < 1 then
        self:log("Invalid voltage data")
        return -2
    end

    return data[0]

end

function peripheral:play_internal (track_nb)

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 6, false)
    buffer:add_int8(track_nb)

    self:send(buffer)

end

function peripheral:set_loop_mode (mode)

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 8, false)
    buffer:add_int8(mode)

    self:send(buffer)

end

function peripheral:play_track (track, bpm, div)

    if #track > 64 then
        self:log("Track too long (max 64)")
        return -1
    end

    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 7, false)
    buffer:add_int8(#track, bpm, div)

    for i, value in ipairs(track) do
        buffer:add_int32(value)
    end

    self.song_done = false

    self:send(buffer)

    while self.song_done == false do
        -- Nothing needed here ^^
    end

end


peripheral.handlers = {

    [7] = function (self, buffer)
        self.song_done = true
        self:log("Done playing song")
    end

}