peripheral.a = 0
peripheral.b = 0
peripheral.x = 0
peripheral.y = 0
peripheral.select = 0
peripheral.menu = 0
peripheral.rb = 0
peripheral.lb = 0
peripheral.rightJoystickClick = 0
peripheral.leftJoystickClick = 0

peripheral.crossX = 0 --[-1, 0, +1]
peripheral.crossY = 0 --[-1, 0, +1]

peripheral.rightJoystick = {1,1,1}
peripheral.leftJoystick = {1,1,1}

function peripheral:updateData ()
    local buffer = HermesBuffer.new()
    buffer:clear()
    buffer:rewind()
    buffer:set_destination(self.hermesID, 0, false)
    buffer:add_int8(0)

    self:send(buffer)
end

peripheral.handlers = {

    [0] = function (self, buffer)
        local data = buffer:data()
        self.a = data[1]
        self.b = data[2]
        self.x = data[3]
        self.y = data[4]
        self.select = data[5]
        self.menu = data[6]
        self.rb = data[7]
        self.lb = data[8]
        self.rightJoystickClick = data[9]
        self.leftJoystickClick = data[10]

        self.crossX = data[11]
        self.crossY = data[12]

        self.rightJoystick = {data[13],data[14],data[15]}
        self.leftJoystick = {data[16],data[17],data[18]}
    end

}