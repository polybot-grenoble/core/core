# Core


## Dev environment

This repo is preconfigured to be used with VSCode and it's C/C++ and CMake 
extensions. 

The following dependencies will be needed:

### On Ubuntu 22.04

```sh
sudo apt install nlohmann-json3-dev lua5.3 liblua5.3-0 liblua5.3-dev luarocks build-essential cmake libc++-11-dev pkg-config
```

### On Raspberry OS

```sh
sudo apt install nlohmann-json3-dev lua5.3 liblua5.3-0 liblua5.3-dev luarocks
```

### On Fedora 39

```sh
sudo dnf install json-devel luarocks
```

### On both

To use the repl, you need to install a lua library called [croissant](https://github.com/giann/croissant/tree/master).
```sh
sudo luarocks-5.3 install croissant
```

## Executables

### Core

The main program

### Monitor

A simple app to see the trafic on a CAN bus while trying to decode
the frame IDs as a Hermes packet, using SocketCAN.

```
./monitor <iface>
```

The program needs as an argument the name of a SocketCAN interface (i.e. 
`vcan0`, `can0`, ...).


#### Attach a virtual SocketCAN interface

```sh
ip link add dev vcan0 type vcan
ip link set vcan0 mtu 16
ip link set up vcan0
```

# The Lua Objects

This part is a simple description of the available methods, not a full tutorial.

**NB**: In Lua, we call methods on instances using `instance:method(...)`, 
which is like `Constructor.method(instance, ...)`

## HermesBuffer

### Static methods

| Name | Type | Description |
|------|------|-------------|
| `new` | `() -> HermesBuffer` | Creates a new instance of HermesBuffer |

### Properties

| Name | Type | Description |
|------|------|-------------|
| `command` | `uint16` | The command ID |
| `remote` | `uint8` | The remote peripheral sending/receiving the buffer |
| `len` | `uint32` | The length of the buffer |
| `pos` | `uint32` | The current read/write head position |
| `isResponse` | `bool` | Is the buffer a response ? |
| `inUse` | `bool` | Is the buffer in use ? |

### Methods

| Name | Type | Description |
|------|------|-------------|
| `clear` | `() -> HermesBuffer` | Clears the buffer's data |
| `set_destination` | `(uint8, uint16, bool) -> HermesReturnCode` | Sets up the (remote, command, isResponse) fields of the buffer |
| `add_argument` | `(...) -> void` | Adds any arguments (Hermes-compatible) to the buffer's data |
| `data` | `() -> table` | Returns a table which contains __a copy__ of the buffer data |
| `rewind` | `() -> void` | Rewinds the read head to be able to parse the buffer data |

## Peripheral

### Static methods

| Name | Type | Description |
|------|------|-------------|
| `new` | `() -> Peripheral` | Creates a new instance of Peripheral |

### Properties

| Name | Type | Description |
|------|------|-------------|
| `properties` | `PeripheralProperties` | The peripheral internal properties manager |

The `__index` and `__new_index` metafunction have been overwritten so:
- When setting a property to be a boolean, number or string which is not defined in this specification, the value will be automatically stored in `properties`
- When getting a property which is not defined in this specification, the value will be automatically gotten from `properties`

```lua
-- x does not exists in Peripheral
-- The two assignments are equivalent
peripheral.x = 12
peripheral.properties.x = 12
-- The two retreivals are equivalent
print(peripheral.x)
print(peripheral.properties.x)
-- This should be printed
peripheral.properties:dump(true) -- Returns "{\"x\":12.0}"
```

### Methods

| Name | Type | Description |
|------|------|-------------|
| `log` | `(string) -> void` | Logs a log message to the console |
| `error` | `(string) -> void` | Logs an error message to the console |
| `debug` | `(string) -> void` | Logs a debug message to the console |
| `send` | `(HermesBuffer) -> void` | Sends a buffer to the (real) peripheral, does not wait for a response nor returns anything |
| `request` | `(HermesBuffer) -> HermesBuffer \| nil` | Sends a buffer to the (real) peripheral, waits for a response, returns the response. If nil, the request failed |

## Peripheral Properties

### Static methods

| Name | Type | Description |
|------|------|-------------|
| `new` | `() -> PeripheralProperties` | Creates a new instance of PeripheralProperties |

### Properties

The `__index` and `__new_index` metafunction have been overwritten so:
- When setting a property to be a boolean, number or string which is not defined in this specification, the value will be stored, else it will be discarded

### Methods

| Name | Type | Description |
|------|------|-------------|
| `dump` | `(bool) -> string` | Dumps the stored variables as JSON (the boolean specifies if the JSON shoud be minified) |


### Wild buffer handlers

Wild buffer := A buffer that arrives without being requested

The Peripheral internal Lua state shall load handlers by running a script 
containing the following table :

```lua
handlers = {
    ...,
    [n] = function (peripheral, buffer)

    end, 
    ...
}
```

With :
 - `n` the command ID (an integer < 255)
 - `function (peripheral, buffer) ... end` the handler
    * `peripheral` the peripheral calling the handler
    * `buffer` the wild buffer
