/**
 * @file hermescan_client.cpp
 * @author Julien Pierson
 * @brief This program is a simple HermesCAN client used to debug things on 
 * a CAN bus which uses Hermes.
 * @version 0.1
 * @date 2023-12-09
 */

#include <iostream>
#include <sys/signal.h>
#include <string>

#include <hermesUDP.hpp>
#include "timedLoop.hpp"
#include "peripheralManager.hpp"

/**
 * @brief Handler for SIGTERM and SIGINT signals
 */
void handle_SIGTERM (int);

// Global running flag
volatile bool running = true;

int main (int argc, char **argv) {

    // Local vars

    Core::Sockets::HermesUDP hermes;
    Core::Sockets::SocketReturnCode code;
    Core::Generic::TimedLoop loop;
    Core::Peripherals::Manager peripheralManager;    

    // Checking CLI args
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <port> <id>\r\n", argv[0]);
        return -1;
    }

    // We change the client ID
    hermes.hermes.id = atoi(argv[2]);

    // Opening the socket
    code = hermes.open(atoi(argv[1]));
    if (code) {
        fprintf(stderr, "Can't open %s (code %d)\r\n", argv[1], code);
        return code;
    }

    // Starting the reading thread
    hermes.start();

    // Indicating who we are
    printf(
        "> Opening %s as %d (0x%x)\r\n",
        argv[1], hermes.hermes.id, hermes.hermes.id
    );

    // Adding a dummy peripheral (n°12)
    auto Dummy = make_shared<Core::Peripherals::Peripheral>();
    Dummy->hermesID = 12;
    Dummy->family = "example";

    // We suppose the peripheral to be on 127.0.0.1:11000
    // hermes.setClient(Dummy.hermesID, "127.0.0.1", 11000);
    
    if (!peripheralManager.add(Dummy)) {
        cout << "Failed to add dummy peripheral n°12" << endl;
    }

    // Adding a way for the peripheral manager to send messages
    peripheralManager.bufferSendHandler = 
        [&hermes] (HermesBuffer *buffer) {

            return hermes.sendBuffer(buffer);

        };

    // The peripheral sends something
    HermesBuffer fakePacket;
    string lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies nec tellus et commodo. Nunc viverra, tortor eget porttitor accumsan.";
    const char *str = lorem.c_str();

    Hermes_clearBuffer(&fakePacket);
    Hermes_setBufferDestination(&fakePacket, Dummy->hermesID, 12, false);
    Hermes_addCommandArgument(&fakePacket, HERMES_T_STRING, (void *) str);
    fakePacket.pos = 0;
    
    Dummy->send(fakePacket);

    // Main loop
    int hermes_available_count;
    HermesBuffer *buffer;

    uint8_t i = 0;


    while (running) {

        // Get the number of available buffers
        hermes_available_count = Hermes_getCompleteBufferCount(&hermes.hermes);

        // We consume the available buffers
        for (; hermes_available_count; hermes_available_count--) {
            buffer = Hermes_nextBuffer(&hermes.hermes);
            peripheralManager.distribute(buffer);
        }

        // We produce a message -- THIS IS NOT HOW TO PROPERLY USE HERMES
        Hermes_setBufferDestination(&fakePacket, 12, 0xA, 0);
        fakePacket.data[0] = HERMES_T_INT8;
        fakePacket.data[1] = i++;
        fakePacket.len = 2;
        fakePacket.pos = 0;
        Dummy->send(fakePacket);

        // We send the next messages that the peripheral produced
        peripheralManager.sendToBus();

        // Slowing down this empty loop using a dummy tick-based loop
        loop.step();

    }

    // Stopping reading thread
    hermes.stop();

    return 0;

}
