#pragma once

#include <sol/sol.hpp>
#include "../strategies/stratManager.hpp"
#include "Strategie.hpp"
#include "config.hpp"

#include <map>

namespace Core::Lua {

	class StratManager: public Core::Strats::Manager {

		private:
			/**
			 * @brief Lua context
			 */
			sol::state& lua;
			/**
			 * @brief List of strategie candidates
			 */
			std::vector<std::string> candidates;

		public:
			StratManager (
				Peripherals::Manager& peripheralManager,
				sol::state &lua, 
				Generic::Config &config
			);

			/**
			 * @brief Resets StratManager, registers and tries to load all 
			 *			   the strategie candidates
			 * @param candidates The strategies candidates
			 */
			void load (vector<string>& candidates);

			/**
			 * @brief Tries to load all the strategie candidates
			 */
			void reload ();

	};

}

namespace Core::Lua::Wrap {

	/**
	 * @brief Wraps StratManager for use in Lua
	 * @param lua The Lua state
	 */
	void wrapStratManager(sol::state& lua);

}