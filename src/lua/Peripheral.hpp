#pragma once

#include <string>
#include <map>
#include <functional>
#include <filesystem>

#include <sol/sol.hpp>
extern "C" {
    #include <Hermes.h>
}
#include "../peripheral/peripheral.hpp"
#include "../peripheral/peripheralProperties.hpp"

namespace Core::Lua {


    class Peripheral : public Core::Peripherals::Peripheral {

        typedef std::function<void(Peripheral &, HermesBuffer &)> wildHandler;

        private:

        public:
        /**
         * @brief User-defined properties
         */
        std::map<std::string, sol::object> properties;

        /**
         * @brief Peripheral's methods storage
         */
        std::map<std::string, sol::protected_function> methods;

        /**
         * @brief Peripheral's wildHandlers storage
         */
        std::map<uint16_t, sol::protected_function> wildHandlers;

        /**
         * @brief The local Lua state to process wild buffers
         */
        sol::state &localState;

        /**
         * @brief Path to this peripheral script
         */
        std::string scriptPath;
        
        /**
         * @brief Construct a new Peripheral object
         */
        Peripheral (sol::state &lua);

        /**
         * @brief Construct a new Peripheral object
         */
        Peripheral (sol::state &lua, std::string family, std::string script);

        /**
         * @brief Construct a new Peripheral object
         */
        Peripheral (Peripheral &);

        /**
         * @brief Handler for wild buffers. A wild buffer is one that is not 
         * expected.
         * 
         * @param buffer 
         */
        void wildBuffer (HermesBuffer *buffer);

        /**
         * @brief Register the contianed handlers as wild handlers 
         * 
         * @param handlers The table containing the wild handlers
         */
        bool registerWildHandlers (sol::object handlers);

        /**
         * @brief Loads the Peripheral from it's lua script
         * 
         * @return true The script is found and loaded,
         * @return false The script is not found or failed to load
         */
        bool load ();
        
        /**
         * @brief Loads the Peripheral from it's lua script
         * 
         * @param script The path to the script to load
         * @return true The script is found and loaded,
         * @return false The script is not found or failed to load
         */
        bool load (std::string script);

        /**
         * @brief Method to print a Lua error during a load
         * 
         * @param err The Lua error
         */
        void loadError (sol::error err);

        /**
         * @brief Sets-up Lua's global variables for an easy load experience
         */
        void setupLoadEnv ();

        /**
         * @brief Undo the set-up environment 
         */
        void clearLoadEnv ();

        /**
         * @brief Calls a user-defined method if definded.
         * 
         * NB: The method is called without any arguments !
         * 
         * @param methodName The method to call
         */
        void call(std::string methodName);


    };

}

namespace Core::Lua::Wrap {

    /**
     * @brief Wraps the peripheral objects for Lua
     * @param lua The state to configure
     */
    void Peripheral (sol::state &lua);

    /**
     * @brief Wraps the peripheral properties object for Lua
     * @param lua The state to configure
     */
    void wrapPeripheralProperties (sol::state &lua);

    /**
     * @brief Wraps the peripheral object for Lua
     * @param lua The state to configure
     */
    void wrapPeripheral (sol::state &lua);

}