#include "resourceLoader.hpp"

namespace Core::Lua {

    ResourceLoader::ResourceLoader (sol::state &lua): lua(lua) {

        defaultPath = lua["package"]["path"];

    }

    ResourceLoader::ResourceLoader (ResourceLoader &resources): 
        lua(resources.lua), 
        defaultPath(resources.defaultPath),
        libraryFolders(resources.libraryFolders), 
        stratFolders(resources.stratFolders) {

    }

    void ResourceLoader::refreshPath () {

        std::string path = defaultPath;

        std::string bit;

        for (auto &folder : libraryFolders) {
            bit = folder;
            bit = bit + (bit.ends_with("/") ? "" : "/") + "?.lua";
            path += (path.empty() ? "" : ";") + bit;
        }

        if (addStratFoldersToPath) {
            for (auto &folder : stratFolders) {
                bit = folder;
                bit = bit + (bit.ends_with("/") ? "" : "/") + "?.lua";
                path += (path.empty() ? "" : ";") + bit;
            }
        }

        lua["package"]["path"] = path;

    }

    void ResourceLoader::addLibFolder (std::string folder) {
        libraryFolders.push_back(folder);
        refreshPath();
    }

    void ResourceLoader::addLibFolder (std::vector<std::string> folders) {
        libraryFolders.insert(
            libraryFolders.end(),
            folders.begin(),
            folders.end()
        );
        refreshPath();
    }
    
    void ResourceLoader::addStratFolder (std::string folder) {
        stratFolders.push_back(folder);
        refreshPath();
    }    
    
    void ResourceLoader::addStratFolder (std::vector<std::string> folders) {
        stratFolders.insert(
            stratFolders.end(),
            folders.begin(),
            folders.end()
        );
        refreshPath();
    }


    std::vector<std::string> ResourceLoader::getAllStrategieCandidates () {

        using namespace std;

        vector<string> output;

        for (auto &folder : stratFolders) {

            // Check if the path IS AN ACTUAL FOLDER
            if (!filesystem::is_directory(folder)) {
                // [TODO] Add logHandler
                continue;
            }

            auto paths = filesystem::directory_iterator(folder);
            for (auto &entry : paths) {
                // If not a file, we skip
                if (!filesystem::is_regular_file(entry)) {
                    continue;
                }

                filesystem::path path(entry);
                // Ignore non-Lua files
                if (path.extension() != ".lua") {
                    continue;
                }

                output.push_back(path.generic_string());

            }

        }

        return output;

    }

}