#pragma once
#include <sol/sol.hpp>
#include <map>
#include <string>
#include "../strategies/strategie.hpp"
#include "Peripheral.hpp"

namespace Core::Lua {

    class Strategie: public Core::Strats::Strategie {

        private:
        /** Lua state */
        sol::state &lua;     

        public:
        
        /** Lua-defined init function */
        std::optional<sol::protected_function> initFunction = nullopt;

        /** Lua-defined main function */
        std::optional<sol::protected_function> mainFunction = nullopt;
        
        /** Lua-defined loop function */
        std::optional<sol::protected_function> loopFunction = nullopt;
        
        /** Lua-defined panic function */
        std::optional<sol::protected_function> panicFunction = nullopt;

        /** Lua-defined onEnd function */
        std::optional<sol::protected_function> onEndFunction = nullopt;

        /**
         * @brief Strategie data cache
         */
        std::map<std::string, sol::object> data; 

        Strategie (
            Core::Peripherals::Manager &manager, 
            sol::state &lua
        );

        Strategie (Strategie &strat);

        /**
         * @brief Path to this strategie script
         */
        std::string scriptPath;

        /**
         * @brief Loads the Strategie from it's lua script
         * 
         * @return true The script is found and loaded,
         * @return false The script is not found or failed to load
         */
        bool load ();

        /**
         * @brief Loads the Strategie from it's lua script
         * 
         * @param script The path to the script to load
         * @return true The script is found and loaded,
         * @return false The script is not found or failed to load
         */
        bool load (std::string script);

        /**
         * @brief Method to print a Lua error during a load
         * 
         * @param err The Lua error
         */
        void loadError (sol::error err);

        /**
         * @brief Method to print a Lua error during a load
         *
         * @param err The Lua error
         * @param phase The function name which called the faulty code
         */
        void runError(sol::error err, std::string phase);
        
        /**
         * @brief Sets-up Lua's global variables for an easy load experience
         */
        void setupLoadEnv ();

        /**
         * @brief Undo the set-up environment 
         */
        void clearLoadEnv ();

        /**
         * @brief Checks if the strategie is properly loaded
         */
        bool properlyLoaded ();

        /**
         * Init function for the strategie, executed before the 
         * tirette is pulled
        */
        void init ();

        /**
         * Init function for a strategie ONLY when used as a fallback after
         * an other strategie just failed
         */
        void panic ();

        /**
         * Main program of the strategie, after the tirette is pulled
        */
        void main ();

        /**
         * Bit of code called each tick of the main loop
        */
        void loop ();

        /**
         * @brief Is this class wrapped by Lua ?
         */
        bool isLua();

    };

}

namespace Core::Lua::Wrap {

    /**
     * @brief Wraps the strategie object for Lua
     * @param lua The state to configure
     */
    void wrapStrategie (sol::state &lua);

}