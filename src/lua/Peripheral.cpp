#include "Peripheral.hpp"

namespace Core::Lua {

    Peripheral::Peripheral (sol::state &lua) : localState(lua) {

        kind = Core::Peripherals::PeripheralKind::LUA;
        family = "lua_peripheral";
        scriptPath = "";
        
    }

    Peripheral::Peripheral (Peripheral &p) : localState(p.localState) {
        // Copying properties
        kind = Core::Peripherals::PeripheralKind::LUA;
        family = p.family;
        hermesID = p.hermesID;
        maxTicksSinceLastHeartbeat = p.maxTicksSinceLastHeartbeat;
        scriptPath = p.scriptPath;
    } 

    Peripheral::Peripheral (
        sol::state &lua, std::string family, std::string script
    ): localState(lua) {

        kind = Core::Peripherals::PeripheralKind::LUA;
        this->family = family;
        scriptPath = script;

    }   

    void Peripheral::wildBuffer (HermesBuffer *buffer) {

        // Disabling the feature

        /* auto entry = wildHandlers.find(buffer->command);
        if (entry == wildHandlers.end()) {   
            log(
                "Lua wild handler " 
                + std::to_string(buffer->command)
                + " not defined", 
                Core::Generic::ERROR
            );
            // Default to standard behaviour
            try {
                Core::Peripherals::Peripheral::wildBuffer(buffer);
            }
            catch (sol::error& err) {
                log(
                    "Execution error in wild handler n°" + std::to_string(entry->first)
                    + " : " + err.what(), Generic::ERROR
                );
            }
            return;
        }
        
        auto handler = entry->second;
        try {
            handler(*this, *buffer);        
        }
        catch (sol::error& err) {
            log(err.what(), Generic::ERROR);
        } */

    }

    bool Peripheral::registerWildHandlers (sol::object handlers) {

        // shortcut for logs
        static const std::string prefix = "[Wild Handler Register] ";

        // Check that the object is valid
        if (!handlers.valid()) {
            log(
                prefix + "Tried to register an invalid lua object", 
                Core::Generic::ERROR
            );
            return false;
        }

        // Check that the object is a table
        if (!handlers.is<sol::table>()) {
            log(
                prefix + "The provided object is not a table",
                Core::Generic::ERROR
            );
            return false;
        }

        // Type tricks
        sol::table T = handlers;

        // Clearing the old handlers
        wildHandlers.clear();

        // Associating the handlers
        for (auto &[key, value] : T) {

            if (!key.is<uint16_t>()) {
                log(
                    prefix + "Tried to register using a non-integer index",
                    Core::Generic::ERROR
                );
                continue;
            }

            if (!value.is<sol::function>()) {
                log(
                    prefix 
                    + "Tried to register a non-function object as wild handler",
                    Core::Generic::ERROR
                );
                continue;
            }
            
            uint16_t k = key.as<uint16_t>();
            sol::function h = value.as<sol::function>();

            wildHandlers[k] = h;
            log(prefix + "Registered handler n°" + std::to_string(k));

        }

        return true;

    }

    bool Peripheral::load () {
        log("Loading peripheral");

        if (scriptPath == "") {
            log("Script not defined", Core::Generic::ERROR);
            return false;
        }

        // Sets-up the lua global vars to load the code
        setupLoadEnv();

        // Clears this peripheral
        state.setState(Talos::Mirror::INIT);
        wildHandlers.clear();
        methods.clear();

        // Loads the script
        bool valid = false;

        sol::protected_function_result result;
        try {
            result = 
                localState.safe_script_file(scriptPath);
            valid = true;
        }
        catch (sol::error& err) {
            std::string err_str (err.what());
            log("Error during load : " + err_str, Generic::ERROR);
        }

        clearLoadEnv();

        if (!valid) {
            return false;
        }

        if (!result.valid()) {
            loadError(result);
            return false;
        }

        return true;

    }

    bool Peripheral::load (std::string script) {
        scriptPath = script;
        return load();
    }

    void Peripheral::loadError (sol::error err) {
        std::string errMsg(err.what());
        log(
            "Error while loading script : " + errMsg, 
            Core::Generic::ERROR
        );
    }

    void Peripheral::setupLoadEnv () {
                
        localState["_peripheral"] = localState["peripheral"];
        localState["_path"] = localState["package"]["path"];
        
        localState["load_error"] = [&] (std::string str) {
            log("Caught load error : " + str, Core::Generic::ERROR);
        };
        localState["peripheral"] = this;

        // Configures the path
        // std::filesystem::path script = scriptPath;
        // localState["_addDirToPath"](script.parent_path().string());
        // Disabled: would need to clear require cache each time
        // Workaround: write everything in one whole file

    }

    void Peripheral::clearLoadEnv () {

        // Cleans up the lua global vars
        auto nil = sol::make_object(localState, sol::nil);

        localState["peripheral"] = localState["_peripheral"];
        localState["package"]["path"] = localState["_path"];
        localState["load_error"] = nil;

        localState["_peripheral"] = nil;
        localState["_path"] = nil;
    }

    void Peripheral::call(std::string methodName) {

        auto entry = methods.find(methodName);
        if (entry == methods.end()) {
            log("No method " + methodName + " to call", Generic::ERROR);
            return;
        }

        sol::protected_function f = entry->second;
        try {
            f(*this);
        }
        catch (sol::error& err) {
            log("Error calling method " + methodName + " : \n" + err.what());
        }

    }

}

namespace Core::Lua::Wrap {

    using Props = Core::Peripherals::PeripheralProperties;
    using LuaPeripheral = Core::Lua::Peripheral;

    void Peripheral (sol::state &lua) {
        wrapPeripheralProperties(lua);
        wrapPeripheral(lua);
    }

    void wrapPeripheralProperties (sol::state &lua) {

        lua.new_usertype<Props>("PeripheralProperties",
            // Overrides the __index accessor
            sol::meta_function::index, 
            [&lua] (Props &props, std::string key) -> sol::object {

                bool suc, _tb; 
                suc = props.get(key, &_tb);
                if (suc) {
                    return sol::make_object(lua, _tb);
                }
                
                double _td; 
                suc = props.get(key, &_td);
                if (suc) {
                    return sol::make_object(lua, _td);
                }
                
                std::string _ts;
                suc = props.get(key, &_ts);
                if (suc) {
                    return sol::make_object(lua, _ts);
                }

                return sol::make_object(lua, sol::lua_nil);
            },
            // Overrides the __newindex accessor
            sol::meta_function::new_index, 
            [&lua] (Props &props, std::string key, sol::object value) {

                if (value.is<bool>()) {
                    props.set(key, value.as<bool>());

                } else if (value.is<double>()) {
                    props.set(key, value.as<double>());

                } else if (value.is<std::string>()) {
                    props.set(key, value.as<std::string>());

                }

            },
            "dump", &Props::dump
        );

    }

    void wrapPeripheral (sol::state &lua) {
        
        lua.new_usertype<LuaPeripheral>("Peripheral",
            // Overrides the __index accessor
            sol::meta_function::index, 
            [&lua] (LuaPeripheral &peripheral, std::string key) -> sol::object {

                peripheral.yield();

                // Tries to find a peripheral
                auto method = peripheral.methods.find(key);
                if (method != peripheral.methods.end()) {
                    return method->second;
                }

                // Tries to find a property
                auto prop = peripheral.properties.find(key);
                if (prop != peripheral.properties.end()) {
                    return prop->second;
                }

                return sol::make_object(lua, sol::lua_nil);
            },
            // Overrides the __newindex accessor
            sol::meta_function::new_index, 
            [&lua] (
                LuaPeripheral &peripheral, std::string key, sol::object value
            ) {

                peripheral.yield();
                
                // Store as handlers table
                if (key == "handlers") {
                    peripheral.registerWildHandlers(value);
                    return;
                }

                // Store methods as methods
                if (value.is<std::function<void()>>()) {
                    peripheral.methods[key] = value;
                    return;
                }

                // Store as property
                peripheral.properties[key] = value;

            },
            // Print log
            "log", 
            [] (LuaPeripheral &p, std::string msg) { 
                p.log(msg, Core::Generic::LOG); 
            },        
            "error", 
            [] (LuaPeripheral &p, std::string msg) { 
                p.log(msg, Core::Generic::ERROR);
            },        
            "debug", 
            [] (LuaPeripheral &p, std::string msg) { 
                p.log(msg, Core::Generic::DEBUG); 
            },
            // Send
            "send", &LuaPeripheral::send,
            "request", 
            [&lua] (
                LuaPeripheral &peripheral, HermesBuffer &buffer, int duration
            ) -> sol::object {                
                HermesBuffer copy;
                memcpy(&copy, &buffer, sizeof(HermesBuffer));

                auto ok = peripheral.request(&copy, duration);
                if (ok) {
                    peripheral.yield();
                    return sol::make_object(lua, copy);
                }

                peripheral.yield();
                return sol::make_object(lua, sol::lua_nil);
            },
            // Static
            "id", &LuaPeripheral::id,
            "hermesID", &LuaPeripheral::hermesID,
            "family", &LuaPeripheral::family,
            "heartbeat", &LuaPeripheral::heartbeat,
            "reset", &LuaPeripheral::reset,
            "maxTicksSinceLastHeartbeat", &LuaPeripheral::maxTicksSinceLastHeartbeat
        );

    }

}
