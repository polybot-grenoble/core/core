#include "PeripheralManager.hpp"

namespace Core::Lua {

    Manager::Manager (sol::state &lua, Generic::YieldLock& globalLock) : Peripherals::Manager(globalLock), Lua(lua) {

    }

    void Manager::reload () {

        log("Reloading peripherals", Core::Generic::LOG);

        // We go through all registered peripherals
        for (auto &[key, peripheral]: peripherals) {
            // If the peripheral is not a Lua one, it can't load
            if (peripheral->kind != Core::Peripherals::LUA) {
                continue;
            }
            // C++ type magic
            auto p = dynamic_cast<Peripheral*>(peripheral.get());
            auto suc = p->load();
            if (!suc) {
                log(p->id() + " failed to load", Core::Generic::ERROR);
            }

            p->reset();
        }

        log("Reloaded peripherals", Core::Generic::LOG);

    }

    void Manager::setScript (std::string family, std::string path) {

        if (!std::filesystem::exists(path)) {
            log(
                "The script path '" + path + "' for peripheral family <" + 
                family + "> does not exist", 
                Core::Generic::ERROR
            );
            return;
        }

        // We register the script path
        scripts[family] = path;

        // We update existing peripherals
        for (auto &[key, periph]: peripherals) {
            // We skip the peripherals that are not part of the family
            if (
                   periph->kind != Core::Peripherals::LUA 
                || periph->family != family
            ) continue;
            // We change the script path for the peripherals
            // using C++ type magic
            auto p = dynamic_cast<Peripheral*>(periph.get());
            p->scriptPath = path;
        }

    }

    shared_ptr<Peripheral> Manager::create (
        std::string family, uint8_t hermesID
    ) {

        // Init peripheral
        auto peripheral = make_shared<Peripheral>(Lua);
        peripheral->family = family;
        peripheral->hermesID = hermesID;

        // Attach script if defined
        auto entry = scripts.find(family);
        if (entry != scripts.end()) {
            peripheral->scriptPath = entry->second;
        }

        // Register the peripheral
        add(peripheral);
        return peripheral;

    }

}

namespace Core::Lua::Wrap {

    void PeripheralManager (sol::state &lua) {

        lua.new_usertype<Manager>("PeripheralManager",
            // Lua-specific
            "reload", &Manager::reload,
            "set_script", &Manager::setScript,
            "create", &Manager::create,

            // C++ generic
            "exists", &Core::Peripherals::Manager::exists,
            "log", [](Manager &p, std::string log) {
                p.log(log, Core::Generic::LOG);
            },
            "error", [](Manager &p, std::string log) {
                p.log(log, Core::Generic::ERROR);
            },
            "debug", [](Manager &p, std::string log) {
                p.log(log, Core::Generic::DEBUG);
            },
            "hasPeripheral", &Manager::hasPeripheral,
            "list", &Manager::list,
            "get", [] (
                Manager &manager,
                std::string id
            ) -> std::optional<Lua::Peripheral*> {

                auto res = manager.get(id);
                if (res == std::nullopt) return nullopt;
                
                auto &p = res.value();
                if (p->kind != Core::Peripherals::LUA) return nullopt;

                return dynamic_cast<Lua::Peripheral*>(p.get());

            },
            "find", [] (
                Manager &manager,
                std::string id
            ) -> std::optional<Lua::Peripheral*> {

                auto res = manager.find(id);
                if (res == std::nullopt) return nullopt;
                
                auto &p = res.value();
                if (p->kind != Core::Peripherals::LUA) return nullopt;

                return dynamic_cast<Lua::Peripheral*>(p.get());

            },
            "force_find", [] (
                Manager &manager,
                std::string id
            ) -> std::optional<Lua::Peripheral*> {

                auto res = manager.force_find(id);
                if (res == std::nullopt) return nullopt;
                
                auto &p = res.value();
                if (p->kind != Core::Peripherals::LUA) return nullopt;

                return dynamic_cast<Lua::Peripheral*>(p.get());

            }
        );

    }

}