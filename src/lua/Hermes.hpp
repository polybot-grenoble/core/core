#pragma once

#include <string>
#include <sol/sol.hpp>
extern "C" {
    #include <Hermes.h>
}

namespace Core::Lua::Wrap {

    /**
     * @brief Wraps Hermes's structs for Lua
     * 
     * @param lua The context to setup
     */
    void Hermes (sol::state &lua);

    /**
     * @brief Wraps the HermesBuffer struct
     * 
     * @param lua The context to setup
     */
    void wrapHermesBuffer (sol::state &lua);

}