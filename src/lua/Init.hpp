#pragma once

#include <sol/sol.hpp>

#include "Hermes.hpp"
#include "Peripheral.hpp"
#include "PeripheralManager.hpp"
#include "Strategie.hpp"
#include "resourceLoader.hpp"
#include "StratManager.hpp"

namespace Core::Lua {

    /**
     * @brief Initializes a Lua state with all the necessary wrappers
     * 
     * @param lua The state to configure
     */
    void Init (sol::state &lua);

}