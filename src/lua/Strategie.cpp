#include "Strategie.hpp"

namespace Core::Lua {

    Strategie::Strategie (
        Core::Peripherals::Manager &manager, 
        sol::state &lua
    ):  Core::Strats::Strategie(manager), 
        lua(lua) {



    }

    Strategie::Strategie (Strategie &strat):
        Core::Strats::Strategie(strat),
        lua(strat.lua),
        initFunction(strat.initFunction),
        mainFunction(strat.mainFunction),
        loopFunction(strat.loopFunction),
        panicFunction(strat.panicFunction),
        onEndFunction(strat.onEndFunction),
        data(strat.data) {

    }

    bool Strategie::load () {

        log("Loading strategie : " + scriptPath);

        if (scriptPath == "") {
            log("Script not defined", Core::Generic::ERROR);
            return false;
        }

        // Sets-up the lua global vars to load the code
        setupLoadEnv();

        // Clears the cache
        errored = false;
        latestErrorMessage = "";

        // Mandatory
        name = "o7";
        initFunction  = nullopt;
        mainFunction  = nullopt;

        // Optionnal
        data.clear();
        fallback = "";
        requiredPeripherals.clear();
        panicFunction = nullopt;
        loopFunction  = nullopt;

        // Loads the script
        sol::protected_function_result result;
        bool valid = false;
        try {
            result = lua.safe_script_file(scriptPath);
            valid = true;
        }
        catch (sol::error& err) {
            loadError(err);
        }

        clearLoadEnv();

        if (!valid) {
            return false;
        }

        if (!result.valid()) {
            loadError(result);
            return false;
        }

        return properlyLoaded();

    }

    bool Strategie::load (std::string script) {
        scriptPath = script;
        return load();
    }

    void Strategie::loadError (sol::error err) {
        std::string errMsg(err.what());
        log(
            "Error while loading script : " + errMsg, 
            Core::Generic::ERROR
        );
        errored = true;
        latestErrorMessage = errMsg;
    }

    void Strategie::runError(sol::error err, std::string phase) {
        std::string errMsg(err.what());
        log(
            "Error while running script, in function " + phase + " : " + errMsg,
            Core::Generic::ERROR
        );
        errored = true;
        latestErrorMessage = errMsg;
    }

    void Strategie::setupLoadEnv () {

        lua["_strat"] = lua["strat"];
        lua["_log"] = lua["log"];

        lua["strat"] = this;
        lua["log"] = [&] (std::string message) {
            log(message, Generic::DEBUG);
        };

    }

    void Strategie::clearLoadEnv () {

        lua["log"] = lua["_log"];
        lua["strat"] = lua["_strat"];

    }

    bool Strategie::properlyLoaded () {

        auto p = "{" + scriptPath + "} ";

        if (name == "o7") {
            log(p + "Strategie name can't be empty", Generic::ERROR);
            return false;
        }

        if (initFunction == nullopt) {
            log(p + "Strategie must have an init function", Generic::ERROR);
            return false;
        }

        if (mainFunction == nullopt) {
            log(p + "Strategie must have a main function", Generic::ERROR);
            return false;
        }

        if (onEndFunction == nullopt) {
            log(p + "Strategie must have a onEnd function", Generic::ERROR);
            return false;
        }

        if (team == 0) {
            log(p + "Strategie must have a team defined", Generic::ERROR);
            return false;
        }

        log("Properly loaded !");
        
        return true;

    }

    void Strategie::init () {

        if (initFunction == nullopt) {
            log("Init function not defined", Generic::DEBUG);
            return;
        }

        auto& fn = initFunction.value(); // <- Get function

        auto& self = *this; // <- Force reference
        sol::protected_function_result result;
        try {
            result = fn(self);
        }
        catch (std::runtime_error& err) {
            log(err.what(), Generic::ERROR);
            return;
        }

        if (!result.valid()) {
            sol::error err = result;    // <- Get the error
            runError(err, "init");
        }

    }

    void Strategie::panic () {

        if (panicFunction == nullopt) {
            log("Panic function not defined", Generic::DEBUG);
            return;
        }

        auto& fn = panicFunction.value(); // <- Get function

        auto& self = *this; // <- Force reference
        sol::protected_function_result result;
        try {
            result = fn(self);
        }
        catch (std::runtime_error& err) {
            log(err.what(), Generic::ERROR);
            return;
        }

        if (!result.valid()) {
            sol::error err = result;    // <- Get the error
            runError(err, "panic");
        }

    }


    void Strategie::main () {

        if (mainFunction == nullopt) {
            log("Main function not defined", Generic::DEBUG);
            return;
        }

        auto& fn = mainFunction.value(); // <- Get function

        auto& self = *this; // <- Force reference
        sol::protected_function_result result;
        try {
            result = fn(self);
        }
        catch (std::runtime_error& err) {
            log(err.what(), Generic::ERROR);
            return;
        }

        if (!result.valid()) {
            sol::error err = result;    // <- Get the error
            runError(err, "main");
        }
    }

    void Strategie::loop () {

        // Disabling the feature

        /* if (loopFunction == nullopt) {
            //log("Loop function not defined", Generic::DEBUG);
            return;
        }

        auto& fn = loopFunction.value(); // <- Get function

        auto& self = *this; // <- Force reference
        sol::protected_function_result result;
        try {
            result = fn(self);
        }
        catch (std::runtime_error& err) {
            log(err.what(), Generic::ERROR);
            return;
        }

        if (!result.valid()) {
            sol::error err = result;    // <- Get the error
            runError(err, "loop");
        } */

    }

    bool Strategie::isLua() {
        return true;
    }

}


namespace Core::Lua::Wrap {

    void wrapStrategie (sol::state &lua) {

        lua.new_usertype<Strategie>("Strategie",
            // Index function for the strat object
            sol::meta_function::index, 
            [&lua] (Strategie &strat, std::string key) -> sol::object {

                strat.yield();
                
                // Get the function
                if (key == "init") {
                    return sol::make_object(lua, strat.initFunction);
                }                
                if (key == "panic") {
                    return sol::make_object(lua, strat.panicFunction);
                }                
                if (key == "loop") {
                    return sol::make_object(lua, strat.loopFunction);
                }                
                if (key == "main") {
                    return sol::make_object(lua, strat.mainFunction);
                }
                if (key == "onEnd") {
                    return sol::make_object(lua, strat.onEndFunction);
                }

                // Get cached peripheral
                auto entry = strat.peripherals.find(key);
                if (entry != strat.peripherals.end()) {
                    auto &p = entry->second;
                    if (p->kind != Peripherals::LUA) {
                        return sol::make_object(lua, sol::lua_nil);
                    }
                    auto lua_p = dynamic_cast<Lua::Peripheral*>(p.get());
                    return sol::make_object(lua, lua_p);
                }

                // Get in the cache
                auto data = strat.data.find(key);
                if (data != strat.data.end()) {
                    return data->second;
                }

                return sol::make_object(lua, sol::lua_nil);

            },

            // New index function for the strat object
            sol::meta_function::new_index,
            [&lua] (Strategie &strat, std::string key, sol::object obj) {
                
                strat.yield();

                // Setter for the main strategie functions
                if (key == "init") {
                    if (!obj.is<std::function<void(Strategie&)>>()) {
                        strat.log(
                            "'init' must be a callable method", 
                            Generic::ERROR
                        );
                    } else {
                        strat.initFunction = obj;
                    }
                    return;
                }                
                if (key == "panic") {
                    if (!obj.is<std::function<void(Strategie&)>>()) {
                        strat.log(
                            "'panic' must be a callable method", 
                            Generic::ERROR
                        );
                    } else {
                        strat.panicFunction = obj;
                    }
                    return;
                }                
                if (key == "loop") {
                    if (!obj.is<std::function<void(Strategie&)>>()) {
                        strat.log(
                            "'loop' must be a callable method", 
                            Generic::ERROR
                        );
                    } else {
                        strat.loopFunction = obj;
                    }
                    return;
                }                
                if (key == "main") {
                    if (!obj.is<std::function<void(Strategie&)>>()) {
                        strat.log(
                            "'main' must be a callable method", 
                            Generic::ERROR
                        );
                    } else {
                        strat.mainFunction = obj;
                    }
                    return;
                }
                if (key == "onEnd") {
                    if (!obj.is<std::function<void(Strategie&)>>()) {
                        strat.log(
                            "'onEnd' must be a callable method",
                            Generic::ERROR
                        );
                    }
                    else {
                        strat.onEndFunction = obj;
                    }
                    return;
                }

                // Checks if the user is trying to override a peripheral
                auto entry = strat.peripherals.find(key);
                if (entry != strat.peripherals.end()) {
                    strat.log(
                        "'" + key + "' is a read-only variable !", 
                        Generic::ERROR
                    );
                    return;
                }

                // Store the object in the cache
                strat.data[key] = obj;

            },

            // Other fields
            "name", &Strategie::name,
            "fallback", &Strategie::fallback,
            "required_peripherals", 
            sol::property(
                // Getter
                [&lua] (Strategie &strat) -> sol::table { 
                    strat.yield();
                    sol::table table = lua.create_table();
                    for (
                        size_t i = 0; 
                        i < strat.requiredPeripherals.size();
                        i++
                    ) {
                        table[i] = strat.requiredPeripherals[i];
                    }
                    return table;
                },
                // Setter
                [] (Strategie &strat, sol::table table) { 
                    strat.yield();
                    strat.requiredPeripherals.clear();
                    for (auto &[key, value] : table) {
                        if (!value.is<std::string>()) continue;
                        strat.requiredPeripherals.push_back(
                            value.as<std::string>()
                        );
                    }
                }
            ),
            "team", &Strategie::team,
            "log", [] (Strategie &strat, std::string message) {
                strat.log(message, Generic::LOG);
            },
            "requirements_met", &Strategie::requirementsMet,
            "sleep", &Strategie::sleep,
            "millis", &Strategie::millis
        );        

    }

}