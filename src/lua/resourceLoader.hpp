#pragma once

#include <sol/sol.hpp>
#include <vector>
#include <filesystem>

namespace Core::Lua {

    /**
     * @brief Manages the Lua require path and finds all files that may 
     * be a strategie.
     */
    class ResourceLoader {

        private:
        /** The lua state */
        sol::state &lua; 
        /** Default Lua require path */
        std::string defaultPath;
        /** List of the folder to add to Lua's require path */
        std::vector<std::string> libraryFolders;
        /** 
         * List of folders in which strategies may be found,
         * these folders are also added to Lua's require path (if enabled)
         */
        std::vector<std::string> stratFolders;

        public:
        /** Should the strategie folders be added to the require path ? */
        bool addStratFoldersToPath = false;

        /**
         * @brief Construct a new Resource Loader object
         * 
         * @param lua The Lua state to manage
         */
        ResourceLoader (sol::state &lua);
        ResourceLoader (ResourceLoader &resources);
        
        /**
         * @brief Updates Lua's require path based on the internal parameters
         */
        void refreshPath ();
        
        /**
         * @brief Adds a lib folder to the internal cache
         * @param folder The folder path to add
         */
        void addLibFolder (std::string folder);
        
        /**
         * @brief Adds a bunch of lib folders to the internal cache
         * @param folders The folder paths to add
         */
        void addLibFolder (std::vector<std::string> folders);

        /**
         * @brief Adds a strat folder to the internal cache
         * @param folder The folder path to add
         */
        void addStratFolder (std::string folder);

        /**
         * @brief Adds a bunch of strat folders to the internal cache
         * @param folders The folder paths to add
         */
        void addStratFolder (std::vector<std::string> folder);

        /**
         * @brief Returns the paths to all files that could be a strategie
         * @return The candidate paths
         */
        std::vector<std::string> getAllStrategieCandidates ();


    };

}