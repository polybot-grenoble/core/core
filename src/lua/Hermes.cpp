#include "Hermes.hpp"

namespace Core::Lua::Wrap {

    void Hermes (sol::state &lua) {

        // Setting up the enums
        lua["HermesErrorCode"] = lua.create_table_with(
            "unknown_error",        HERMES_ERR_UNKNOWN_ERROR,
            "invalid_command_id",   HERMES_ERR_INVALID_COMMAND_ID,
            "invalid_argument",     HERMES_ERR_INVALID_ARGUMENT,
            "busy",                 HERMES_ERR_BUSY
        );

        lua["HermesArgumentTypeIndicator"] = lua.create_table_with(
            "char",                 HERMES_T_CHAR,
            "int8",                 HERMES_T_INT8,
            "int16",                HERMES_T_INT16,
            "int32",                HERMES_T_INT32,
            "int64",                HERMES_T_INT64,
            "float",                HERMES_T_FLOAT,
            "double",               HERMES_T_DOUBLE,
            "string",               HERMES_T_STRING
        );

        lua["HermesReturnCode"] = lua.create_table_with(
            "ok",                   HERMES_OK,
            "invalid_id",           HERMES_INVALID_ID,
            "invalid_command",      HERMES_INVALID_COMMAND,
            "forbidden",            HERMES_FORBIDDEN,
            "fifo_full",            HERMES_FIFO_FULL,
            "fifo_empty",           HERMES_FIFO_EMPTY,
            "buffer_none",          HERMES_BUFFER_NONE,
            "buffer_not_found",     HERMES_BUFFER_NOT_FOUND,
            "buffer_too_big",       HERMES_BUFFER_TOO_BIG,
            "buffer_too_small",     HERMES_BUFFER_TOO_SMALL,
            "chunk_too_big",        HERMES_CHUNK_TOO_BIG,
            "chunk_too_small",      HERMES_CHUNK_TOO_SMALL,
            "invalid_arg_type",     HERMES_INVALID_ARG_TYPE
        );

        // Wrapping the structs
        wrapHermesBuffer(lua);

    }

    void wrapHermesBuffer (sol::state &lua) {
        
        lua.new_usertype<HermesBuffer>("HermesBuffer",
            // Initializing the new memory location
            "new", sol::initializers([] (HermesBuffer &buf) {
                new (&buf) HermesBuffer();
                Hermes_clearBuffer(&buf);
                return buf;
            }),
            // Wrapping the fields
            "command",      &HermesBuffer::command,
            "remote",       &HermesBuffer::remote,
            "len",          &HermesBuffer::len,
            "pos",          &HermesBuffer::pos,
            "isResponse",   &HermesBuffer::isResponse,
            "inUse",        &HermesBuffer::inUse,

            // Adding Hermes methods in OOP fasion
            // Hermes_clearBuffer
            "clear",    [](HermesBuffer &buf) -> HermesBuffer& { 
                Hermes_clearBuffer(&buf); 
                return buf;
            },
            // Hermes_setBufferDestination
            "set_destination", [](
                HermesBuffer &buf, uint8_t remote, 
                uint16_t command, bool isResponse
            ) -> HermesReturnCode {
                return Hermes_setBufferDestination(
                    &buf, remote, command, isResponse
                );
            },
            
            // Hermes_addCommandArgument
            "add_argument", [](
                HermesBuffer &buf, sol::variadic_args va, sol::object
            ) {
                HermesReturnCode code;
                // Containers
                char va_char; int8_t va_int8; int16_t va_int16; 
                int32_t va_int32; int64_t va_int64;
                double va_double; std::string va_str;

                // Going to the end of the data
                buf.pos = buf.len;

                // Filling the arguments
                for (sol::object obj : va) {

                    if (obj.is<int64_t>()) {

                        va_int64 = obj.as<int64_t>();
                        
                        if (va_int64 < std::numeric_limits<int8_t>::max()) {
                            va_int8 = (int8_t) va_int64;
                            code = Hermes_addCommandArgument(
                                &buf, HERMES_T_INT8, &va_int8
                            );
                        } else if (va_int64 < std::numeric_limits<int16_t>::max()) {
                            va_int16 = (int16_t) va_int64;
                            code = Hermes_addCommandArgument(
                                &buf, HERMES_T_INT16, &va_int16
                            );
                        } else if (va_int64 < std::numeric_limits<int32_t>::max()) {
                            va_int32 = (int32_t) va_int64;
                            code = Hermes_addCommandArgument(
                                &buf, HERMES_T_INT32, &va_int32
                            );
                        } else {
                            code = Hermes_addCommandArgument(
                                &buf, HERMES_T_INT64, &va_int64
                            );
                        }

                    } else if (obj.is<double>()) {
                        va_double = obj.as<double>();
                        code = Hermes_addCommandArgument(
                            &buf, HERMES_T_DOUBLE, &va_double
                        );
                    } else if (obj.is<std::string>()) {
                        va_str = obj.as<std::string>();
                        code = Hermes_addCommandArgument(
                            &buf, HERMES_T_STRING, (void *) va_str.c_str()
                        );
                    } else if (obj.is<char>()) {
                        va_char = obj.as<char>();
                        code = Hermes_addCommandArgument(
                            &buf, HERMES_T_CHAR, &va_char
                        );
                    }

                    if (code) break;
                }
                
                return code;
            },
            
            // Hermes_getCommandArgument / buffer.data
            "data", [&lua](
                HermesBuffer &buf
            ) -> sol::table {
                char va_char; int8_t va_int8; int16_t va_int16; 
                int32_t va_int32; int64_t va_int64; float va_float;
                double va_double; char va_str[HERMES_MAX_BUFFER_LEN];

                Hermes_rewind(&buf);
                sol::table out = lua.create_table();

                HermesArgumentTypeIndicator T;
                int i = 1;

                for (
                    T = Hermes_nextArgumentType(&buf);
                    (T != HEMRES_T_NONE) && (buf.len != buf.pos);
                    T = Hermes_nextArgumentType(&buf)
                ) {

                    switch (T)
                    {
                    case HERMES_T_CHAR:
                        Hermes_getCommandArgument(&buf, &va_char);
                        out[i] = va_char;
                        break;
                    case HERMES_T_INT8:
                        Hermes_getCommandArgument(&buf, &va_int8);
                        out[i] = va_int8;
                        break;
                    case HERMES_T_INT16:
                        Hermes_getCommandArgument(&buf, &va_int16);
                        out[i] = va_int16;
                        break;
                    case HERMES_T_INT32:
                        Hermes_getCommandArgument(&buf, &va_int32);
                        out[i] = va_int32;
                        break;
                    case HERMES_T_INT64:
                        Hermes_getCommandArgument(&buf, &va_int64);
                        out[i] = va_int64;
                        break;
                    
                    case HERMES_T_FLOAT:
                        Hermes_getCommandArgument(&buf, &va_float);
                        out[i] = va_float;
                        break;
                    case HERMES_T_DOUBLE:
                        Hermes_getCommandArgument(&buf, &va_double);
                        out[i] = va_double;
                        break;

                    case HERMES_T_STRING:
                        Hermes_getCommandArgument(&buf, va_str);
                        out[i] = std::string(va_str);
                        break;
                    
                    default:
                        break;
                    }

                    i++;
                }

                return out;
            },

            "rewind", [](HermesBuffer &buf) { 
                Hermes_rewind(&buf); return buf; 
            },

            "add_char", [](HermesBuffer &buf, std::string chars) {
                HermesReturnCode code = HERMES_OK;
                auto raw_str = chars.c_str();
                for (size_t i = 0; i < chars.length() && !code; i++) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_CHAR, (char *) (raw_str + i)
                    );
                }
                return code;
            },

            "add_int8", [](
                HermesBuffer &buf, sol::variadic_args args, int8_t
            ) {
                HermesReturnCode code = HERMES_OK;
                for (int8_t nb : args) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_INT8, &nb
                    );
                    if (code) break;
                }
                return code;
            },

            "add_int16", [](
                HermesBuffer &buf, sol::variadic_args args, int16_t
            ) {
                HermesReturnCode code = HERMES_OK;
                for (int16_t nb : args) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_INT16, &nb
                    );
                    if (code) break;
                }
                return code;
            },

            "add_int32", [](
                HermesBuffer &buf, sol::variadic_args args, int32_t
            ) {
                HermesReturnCode code = HERMES_OK;
                for (int32_t nb : args) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_INT32, &nb
                    );
                    if (code) break;
                }
                return code;
            },

            "add_int64", [](
                HermesBuffer &buf, sol::variadic_args args, int64_t
            ) {
                HermesReturnCode code = HERMES_OK;
                for (int64_t nb : args) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_INT64, &nb
                    );
                    if (code) break;
                }
                return code;
            },

            "add_float", [](
                HermesBuffer &buf, sol::variadic_args args, float
            ) {
                HermesReturnCode code = HERMES_OK;
                for (float nb : args) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_FLOAT, &nb
                    );
                    if (code) break;
                }
                return code;
            },

            "add_double", [](
                HermesBuffer &buf, sol::variadic_args args, double
            ) {
                HermesReturnCode code = HERMES_OK;
                for (double nb : args) {
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_DOUBLE, &nb
                    );
                    if (code) break;
                }
                return code;
            },

            "add_string", [](
                HermesBuffer &buf, sol::variadic_args args, std::string
            ) {
                HermesReturnCode code = HERMES_OK;
                for (std::string str : args) {
                    auto raw = (char *) str.c_str();
                    code = Hermes_addCommandArgument(
                        &buf, HERMES_T_STRING, raw
                    );
                    if (code) break;
                }
                return code;
            }

        );

    }

}
