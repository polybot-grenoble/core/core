#include "StratManager.hpp"

namespace Core::Lua {

	StratManager::StratManager(
		Peripherals::Manager& peripheralManager,
		sol::state &lua, 
		Generic::Config& config
	):
		Strats::Manager(
			peripheralManager,
			std::chrono::seconds(config.stratManager.timeout),
			config.stratManager.maxRetry,
            std::chrono::milliseconds(config.stratManager.timeBetweenRetryMs),
            config.stratManager.backupMode == "SIMPLE" ? Strats::BACKUP_MODE_SIMPLE : Strats::BACKUP_MODE_AUTO
            ),
		lua(lua) {

	}

	void StratManager::load(vector<string>& candidates) {

		this->candidates.clear();
		strategies.clear();

		std::copy(
			candidates.begin(),
			candidates.end(),
			back_inserter(this->candidates)
		);

		std::string message =  "Strategie candidates : \r\n";

		for (auto& candidate : candidates) {
			message += " - " + candidate + "\r\n";
		}

		log(message, Generic::DEBUG);

		reload();

	}

	void StratManager::reload() {

		log("Reloading strategies", Generic::LOG);

		for (auto& strat : candidates) {

			auto s = make_shared<Strategie>(peripheralManager, lua);

			s->logHandler = [&](std::string message, Generic::LogLevel level) {
				log(message, level);
			};

			auto success = s->load(strat);

			if (!success) {
				// We discard failed attempts
				continue;
			}

			s->yieldCallback = [&]() {
				globalLock.yield();
				};

			strategies[s->name] = s;

		}

		log("Reloaded strategies", Generic::LOG);

	}

}

namespace Core::Lua::Wrap {

	void wrapStratManager(sol::state& lua) {

		lua.new_usertype<StratManager>("StratManager",
			"set_current_strat", &StratManager::setCurrentStrategie,
			"get_current_strat", &StratManager::getCurrentStrategie,
			"find_backup_strat", &StratManager::findBackupStrategie,
			"list", &StratManager::list,
			"get", &StratManager::get,
			"log", [](StratManager& strat, std::string message) {
				strat.log(message, Generic::LOG);
			},
			"log_config", &StratManager::logConfiguration,
			"init", &StratManager::init,
			"run", &StratManager::run,
			"stop", &StratManager::stop,
			"reset", &StratManager::reset,
			"ready", sol::property(
				[](StratManager& strat) { return strat.isReady(); },
				[](StratManager& strat, bool t) {}
			),
			"running", sol::property(
				[](StratManager& strat) { return strat.isRunning(); },
				[](StratManager& strat, bool t) {}
			),
			"timeout", sol::property(
				[](StratManager& strat) { return strat.hasTimeout(); },
				[](StratManager& strat, bool t) {}
			),
			"backup", sol::property(
				[](StratManager& strat) { return strat.isBackupMode(); },
				[](StratManager& strat, bool t) {}
			),
			"reload", &StratManager::reload
		);

	}

}