#pragma once

#include <sol/sol.hpp>
#include "../peripheral/peripheralManager.hpp"
#include "Peripheral.hpp"

#include <map>
#include <filesystem>

namespace Core::Lua {

    class Manager : public Core::Peripherals::Manager {

        protected:
        std::map<std::string, std::string> scripts;

        public:
        /**
         * @brief Internal Lua state
         */
        sol::state &Lua;

        /**
         * @brief Construct a new Manager object
         * 
         * @param lua The Lua state
         */
        Manager (sol::state &lua, Generic::YieldLock& globalLock);

        /**
         * @brief Tries to reload all peripherals
         */
        void reload ();

        /**
         * @brief Set the script associated to a peripheral family
         * 
         * @param family The family name
         * @param path The absolute path to the script
         */
        void setScript (std::string family, std::string path);

        /**
         * @brief Creates a new peripheral
         * 
         * @param family 
         * @param hermesID 
         * @return The peripheral
         */
        shared_ptr<Peripheral> create (std::string family, uint8_t hermesID);

    };

}


namespace Core::Lua::Wrap {

    /**
     * @brief Wraps the Peripheral Manager object for Lua
     * @param lua The lua state to configure
     */
    void PeripheralManager (sol::state &lua);

}