#include "Init.hpp"

namespace Core::Lua {

    void Init (sol::state &lua) {

        // Basic libs
        lua.open_libraries(
            sol::lib::base, 
            sol::lib::count, 
            sol::lib::io,
            sol::lib::math,
            sol::lib::os,
            sol::lib::package,
            sol::lib::string,
            sol::lib::table,
            sol::lib::utf8,
            sol::lib::debug,
            sol::lib::coroutine
        );

        // Function to add a location to lua's path
        lua["_addDirToPath"] = [&lua] (std::string path) {
            path = path + (path.ends_with("/") ? "" : "/") + "?.lua";
            std::string luaPath = lua["package"]["path"];
            luaPath += (luaPath.empty() ? "" : ";") + path;
            lua["package"]["path"] = luaPath;
        };

        // Wrap Hermes
        Wrap::Hermes(lua);

        // Wrap Peripheral
        Wrap::Peripheral(lua);

        // Wrap Peripheral Manager
        Wrap::PeripheralManager(lua);

        // Wrap strategies
        Wrap::wrapStrategie(lua);
        
        // Wrap Strategie Manager
        Wrap::wrapStratManager(lua);

    }

}