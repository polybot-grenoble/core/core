#include <iostream>
#include <thread>
#include "generic/sync.hpp"

using namespace std;

int main(){
    Sync synchrotron;
    bool test = true;

    cout << "Beginning of test" << endl;

    int i = 0;

    std::thread tA = thread([&synchrotron, &test, &i]() {
        synchrotron.subscribe();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));


        while (test) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            i++;
            cout << "[A]: Bip! " << i << endl;
            synchrotron.yield();
        }

        synchrotron.unsubscribe();
    });

    std::thread tB = thread([&synchrotron, &test, &i]() {
        synchrotron.subscribe();
        while (test) {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            i++;
            cout << "[B]: Boop!" << i << endl;
            synchrotron.yield();
        }

        synchrotron.unsubscribe();
    });

    std::thread tC = thread([&synchrotron, &test, &i]() {
        synchrotron.subscribe();
        int a = 0;

        while (test) {
            std::this_thread::sleep_for(std::chrono::milliseconds(750));
            i--;
            cout << "[C]: Baap!" << i << endl;
            synchrotron.yield();
        }

        synchrotron.unsubscribe();
    });

    cout << "Threads started" << endl;

    cin >> i;

    cout << "Closing..." << endl;

    test = false;
    if(tA.joinable()){
        tA.join();
    }

    if(tB.joinable()){
        tB.join();
    }

    if(tC.joinable()){
        tC.join();
    }

    cout << "Closed." << endl;

    return 0;
}