/**
 * @file peripheral_test.cpp
 * @author Julien PIERSON
 * @brief This file is used to test the Peripheral class
 * @version 0.1
 * @date 2023-12-16
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <iostream>
#include <thread>
#include <chrono>

#include "peripheral.hpp"
#include "logLevel.hpp"

using namespace std;
using namespace Core::Peripherals;
using namespace Core::Generic;

int main () {

    cout << "Hello !" << endl;

    Peripheral P;

    // Dummy config
    P.family = "base_mobile";
    P.hermesID = 12;

    cout << P.properties.dump(false) << endl;

    // Test - Assigning manually the values
    P.properties.set("k", 12.0);
    P.properties.set("nom", "Julien");
    P.properties.set("test", false);
    P.properties.set("m", 1.0);

    cout << P.properties.dump(false) << endl;

    // Test - removing a value
    P.properties.clear("m");

    cout << P.properties.dump(false) << endl;

    // Test - loading a dump (successfull)
    string str = "{ \"bruh\": 42 }";
    P.properties.load(str);

    cout << P.properties.dump(false) << endl;

    // Test - reading a double from an int
    double d;
    bool ok = P.properties.get("bruh", &d);

    if (ok)
        cout << "Bruh = " << d << endl;
    else 
        cout << "Fail" << endl;

    // Test - failing to load
    ok = P.properties.load("{ k: 2 }"); // Missing "" around k

    cout << "Load status: " << ok << endl;
    cout << P.properties.dump(false) << endl;

    // Test - Log without handler
    P.log("Hello from nowhere");

    // Test - Log with handler
    P.logHandler = [](string msg, LogLevel level) {

        string s = "[Peripheral]" + msg;
        
        if      (level == ERROR) s = "\x1b[31m[ERROR]\x1b[0m " + s;
        else if (level == DEBUG) s = "\x1b[35m[DEBUG]\x1b[0m " + s;
        else                     s = "\x1b[32m[LOG  ]\x1b[0m " + s; 

        cout << s << endl;

    };

    P.log("Hello from the handler !", DEBUG);

    // Test - Handle a wild buffer
    HermesBuffer buf;
    buf.command = 42;
    P.handleBuffer(&buf);

    // Test - Handle a request (responded to by another thread)
    auto t = thread([&P]() {

        cout << "Thread sleeps for 1s" << endl;
        this_thread::sleep_for(chrono::milliseconds(1000));
        cout << "Thread wakes up" << endl;

        // THIS IS A FAKE USE EXAMPLE -- NOT A REFERENCE

        // 12 responds with the string "Hi !"
        HermesBuffer response;
        Hermes_clearBuffer(&response);
        Hermes_setBufferDestination(&response, 0, 1, true);
        Hermes_addCommandArgument(
            &response, HERMES_T_STRING, (void *) "Hi !"
        );
        Hermes_rewind(&response); // <- Resets the read head

        P.handleBuffer(&response);
    });

    P.log("Requesting 1 from 12 ...");

    // Requesting the "1" command from "12"
    Hermes_clearBuffer(&buf);
    Hermes_setBufferDestination(&buf, P.hermesID, 1, false);

    P.request(&buf);

    // Printing the recieved string
    char raw[42];
    Hermes_getCommandArgument(&buf, raw);
    string recieved(raw);

    P.log("Recieved response from command 1 to 12: " + recieved);

    t.join();

    return 0;

}