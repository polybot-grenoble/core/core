#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp> // or #include "sol.hpp", whichever suits your needs
#include <iostream>
#include <string>
#include <functional>
#include <thread>
#include <chrono>

#include "Init.hpp"

#include "hermesUDP.hpp"
#include "hermesCAN.hpp"

#include "peripheralManager.hpp"
#include "timedLoop.hpp"
#include "yieldLock.hpp"

#include "config.hpp"

std::string REPL_SCRIPT = "     \
    require \"luarocks.loader\" \
    require \"croissant.repl\"()";

int main(int argc, char* argv[]) {

    // Try to load config
    Core::Generic::Config config;

    auto home = std::getenv("HOME");

    if (config.setPath(
        strlen(home) ? 
            std::string(home) + "/.core/config.json" 
            : "/home/core_config.json"
        )
    ) {
        cout << "Config loaded successfully !" << "\r\n";
    } else {
        cout << "Config failed to load ..." << "\r\n";
        return -2;
    }

    // Log Handler
    Core::Generic::LogHandler log = [](
        std::string msg,
        Core::Generic::LogLevel level
        ) {
            std::string s = msg;

            if (level == Core::Generic::ERROR)
                s = "\x1b[31m[ERROR]\x1b[0m " + s;
            else if (level == Core::Generic::DEBUG)
                s = "\x1b[35m[DEBUG]\x1b[0m " + s;
            else
                s = "\x1b[32m[LOG  ]\x1b[0m " + s;

            std::cout << s << std::endl;
        };

    // Setup state
	sol::state lua;
    Core::Lua::Init(lua);
    Core::Lua::ResourceLoader resources(lua);

    resources.addLibFolder(config.lua.libraryFolders);
    resources.addStratFolder(config.lua.strategiesFolders);

    Core::Lua::Manager peripherals(lua);
    peripherals.logHandler = log;

    Core::Generic::TimedLoop dummy;

    for (auto &[family, file] : config.lua.families) {
        peripherals.setScript(family, file);
    }

    for (auto &[id, family] : config.lua.peripherals) {
        peripherals.create(family, id);
    }

    peripherals.reload();

    lua["manager"] = &peripherals; // <- THE POINTER MATTERS

    Core::Lua::StratManager stratManager(peripherals, lua, config);
    stratManager.logHandler = log;
    stratManager.logConfiguration();
    lua["strats"] = &stratManager;

    auto candidates = resources.getAllStrategieCandidates();
    stratManager.load(candidates);

    // Setup hermes
    Core::Sockets::HermesCAN hermesCAN(config.hermes.hermesID);
    Core::Sockets::HermesUDP hermesUDP(config.hermes.hermesID);

    // Opens vcan0
    if (hermesCAN.open((char *) config.hermes.canInterface.c_str())) {
        printf("%s not available\r\n", config.hermes.canInterface.c_str());
    } else if (hermesCAN.start()) {
        printf(
            "HermesCAN is running on %s\r\n", 
            config.hermes.canInterface.c_str()
        );
    } else {
        printf("HermesCAN Threads can't start\r\n");
    }

    // Opens 0.0.0.0:42069
    if (hermesUDP.open(config.hermes.UDPPort)) {
        printf("%d not available\r\n", config.hermes.UDPPort);
    } else if (hermesUDP.start()) {
        printf("HermesUDP is running on %d\r\n", config.hermes.UDPPort);
    } else {
        printf("HermesUDP Threads can't start\r\n");
    }

    for (auto &client : config.hermes.UDPClients) {
        hermesUDP.setClient(client.hermesID, client.address, client.port);
    }

    // Setup read/write
    peripherals.bufferSendHandler = [&] (HermesBuffer *buffer) -> bool {
        
        auto can = hermesCAN.sendBuffer(buffer);
        auto udp = hermesUDP.sendBuffer(buffer);
        
        return can || udp;
    };

    auto reader = std::thread([&] () {
        for (;;) {
            HermesBuffer received;
            
            while (hermesCAN.getBuffer(&received)) {
                peripherals.distribute(&received);
            }

            while (hermesUDP.getBuffer(&received)) {
                peripherals.distribute(&received);
            }

            peripherals.sendToBus();
            peripherals.tick();

            dummy.step(); // <- For timing purpose only
        }
    });

    // Print strat candidates
    /*std::cout << "Candidates : ";
    lua["_strats"] = lua.create_table();

    auto candidates = resources.getAllStrategieCandidates();
    std::vector<shared_ptr<Core::Lua::Strategie>> strats;
    for (auto &c : candidates) {
        std::cout << "\r\n - " << c;
        auto strat = make_shared<Core::Lua::Strategie>(
            peripherals, lua
        );
        strat->logHandler = peripherals.logHandler;
        strat->load(c);
        strats.push_back(strat);

        lua["_strats"][strat->name] = strat.get();

        strat->setup();
    }

    std::cout << "\r\n" << endl;*/

    //// -- TEST ZONE --

    // Just to have a breakpoint -- call `show()` in repl
    lua["show"] = [&lua, &peripherals]() {
        printf("Breakpoint !\n");
    };

    // Testing yield lock
    // bool run_yield_test = true;
    // Core::Generic::YieldLock lock;
    // std::thread yieldTest ([&run_yield_test, &lock]() {
    //     while (run_yield_test) {    
    //         lock.yield();
    //         printf("[Bip !]\r\n");
    //         std::this_thread::sleep_for(std::chrono::milliseconds(250));
    //     }
    // });

    // std::thread yieldTestBis ([&run_yield_test, &lock]() {
    //     while (run_yield_test) {    
    //         lock.yield();
    //         printf("[Boop !]\r\n");
    //         std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    //     }
    // });

    // lua["lock_pause"] = [&lock] () {
    //     return lock.pause();
    // };

    // lua["lock_resume"] = [&lock] () {
    //     lock.resume();
    // };

    //// -- END TEST ZONE --

    // Running the script
    lua.script(REPL_SCRIPT);

    // Stop
    hermesCAN.stop();
    hermesUDP.stop();

    pthread_cancel(reader.native_handle());
    if (reader.joinable()) {
        reader.join();
    }

    // run_yield_test = false;
    // lock.resume();
    // if (yieldTest.joinable()) {
    //     yieldTest.join();
    // }
    // if(yieldTestBis.joinable()){
    //     yieldTestBis.join();
    // }

	return 0;
}