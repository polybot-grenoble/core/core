#include "loop-v1.hpp"

namespace Core::Loops {

	LoopV1::LoopV1(Generic::Config& config, sol::state& Lua, Cockpit::Cockpit& cockpit, Generic::LogHandler logHandler) :
        Generic::TimedLoop(),
        Talos::gigaTalos::gigaTalos(),
        Lua(Lua),
        config(config),
		resources(Lua),
		peripherals(Lua, globalLock),
		strategies(peripherals, Lua, config),
		hermesCAN(config.hermes.hermesID),
		hermesUDP(config.hermes.hermesID),
		cockpit(cockpit),
		logHandler(logHandler) {

		log("Instantiating Loop V1");

		// Opening Hermes
		log("Opening Hermes", Generic::DEBUG);
		
		if (hermesCAN.open((char*)config.hermes.canInterface.c_str())) {
			log(config.hermes.canInterface + " can't be opened", Generic::ERROR);
			changeCommand(Talos::gigaTalos::init_err);
			is_init_ok = false;
            return;
        }
        else if(!hermesCAN.start()){
            log("HermesCAN thread cannot start.", Generic::ERROR);
            changeCommand(Talos::gigaTalos::init_err);
            is_init_ok = false;
            return;
        }
        else {
			log(config.hermes.canInterface + " opened", Generic::DEBUG);
		}

		if (hermesUDP.open(config.hermes.UDPPort)) {
			log("Port " + std::to_string(config.hermes.UDPPort) + " can't be opened", Generic::ERROR);
			changeCommand(Talos::gigaTalos::init_err);
			is_init_ok = false;
            return;
        }
        else if(!hermesUDP.start()){
            log("HermesUDP thread cannot start.", Generic::ERROR);
            changeCommand(Talos::gigaTalos::init_err);
            is_init_ok = false;
            return;
        }
		else {
			log("Port " + std::to_string(config.hermes.UDPPort) + " opened", Generic::DEBUG);
		}




        for(auto& client: config.hermes.UDPClients){
            hermesUDP.setClient(client.hermesID, client.address, client.port);
        }

		// Set up Lua
		log("Initializing Lua", Generic::DEBUG);
		Lua["_core_peripherals"] = &peripherals;
		Lua["_core_strategies"] = &strategies;

		// Loading resources
		log("Loading Lua resources", Generic::DEBUG);
		resources.addLibFolder(config.lua.libraryFolders);
		resources.addStratFolder(config.lua.strategiesFolders);

		// Loading peripherals
		log("Initializing Lua", Generic::DEBUG);
		peripherals.logHandler = logHandler;

		for (auto& [family, file] : config.lua.families) {
			peripherals.setScript(family, file);
		}

		for (auto& [id, family] : config.lua.peripherals) {
			peripherals.create(family, id);
		}

		peripherals.peripheralLoss = [&] (std::string id) {
			log("Lost peripheral " + id, Generic::ERROR);
			changeCommand(Talos::gigaTalos::peripheral_loss);
			cockpit.send("lost_peripheral", id);
		};

        peripherals.bufferSendHandler = [&] (HermesBuffer *buffer) -> bool {
            auto can = hermesCAN.sendBuffer(buffer);
            auto udp = hermesUDP.sendBuffer(buffer);

            return can || udp;
        };

		// Loading strategies
		log("Initializing Strategies", Generic::DEBUG);
		strategies.logHandler = logHandler;
		strategies.logConfiguration();
		auto candidates = resources.getAllStrategieCandidates();
		strategies.load(candidates);
        strategies.onFailure = [&] () {
            // Called when a peripheral is definitively lost. This is managed by the strategieManager
            // and not PERIPHERAL_LOSS.
            strategies.log("Aborting current strategie.", Generic::ERROR);
            changeCommand(Talos::gigaTalos::peripheral_loss);
        };

		setupHMI();

	}

	LoopV1::~LoopV1() {

		// Unloading
		log("Destroying Loop V1");

		hermesCAN.stop();
		hermesUDP.stop();

	}


	bool LoopV1::loop() {

		bool can_run = true;

		// We pause the strat 
		if (strategies.isRunning()) {
			can_run = peripherals.pause();
		}

		if (!can_run) {

			log("Can't pause the strategie execution", Generic::ERROR);
			changeCommand(Talos::gigaTalos::strat_stop_exec_error);
		}

		// We read Hermes
		HermesBuffer received;

		while (hermesCAN.getBuffer(&received)) {
			peripherals.distribute(&received);
		}

		while (hermesUDP.getBuffer(&received)) {
			peripherals.distribute(&received);
		}

		// We send the pending messages
		peripherals.sendToBus();

		// We tick all peripherals
		peripherals.tick();

		// We resume the strat
		peripherals.resume();

		// Run Talos - Which runs the new current state
		exec();

		// We run until in the EXIT_CORE state
		return main_run;

	}

	void LoopV1::log(std::string message, Generic::LogLevel level) {

		if (logHandler) {
			logHandler(message, level);
			return;
		}

		std::string s = message;

		if (level == Core::Generic::ERROR)
			s = "\x1b[31m[ERROR]\x1b[0m " + s;
		else if (level == Core::Generic::DEBUG)
			s = "\x1b[35m[DEBUG]\x1b[0m " + s;
		else
			s = "\x1b[32m[LOG  ]\x1b[0m " + s;

		std::cout << s << std::endl;

	}


	void LoopV1::init() {

		log("Reached Talos init state", Generic::DEBUG);

		// We check if we are properly initialized
		if (!is_init_ok) {
			// If not, we are errored
			changeCommand(Talos::gigaTalos::init_err);
			return;

		}

		// We can continue executing
		changeCommand(Talos::gigaTalos::init_ok);

		log("Welcome to Core V1.0 !");

	}


	void LoopV1::initFail() {

		log(
			"Entered Init Failed state. This version of Core can't re-initialize itself",
			Generic::ERROR
		);

		changeCommand(Talos::gigaTalos::quit);

	}


	void LoopV1::endCore() {

		log("Quitting ...");

		main_run = false;

	}


	void LoopV1::idle() {

		if (firstTime()) {
			// Reload peripherals at each reset
			peripherals.reload();
		}

		// Waiting for HMI input
		// this must be handled by a Cockpit router

	}


	void LoopV1::remoteControl() {

		// [TODO] Not defined yet.

		// Should start a croissant thread of which the input and output are
		// redirected to cockpit to allow the use of an external terminal
		// for debug purposes

	}


	void LoopV1::stratChange() {

		// [TODO] Need to rework get(), setCurrent() and related because some functions are called
        // multiple times below.

		log("Looking for a strategie", Generic::DEBUG);

		strategies.logConfiguration();

        optional<shared_ptr<Strats::Strategie>> strategie;

        if(strategies.isBackupMode()){
            // Ok, we come from a failed strat, we must be quick.
            strategie = strategies.findBackupStrategie();
        }else{
            // [TODO] Change it to strategie selected by the user.
            strategie = strategies.get(selectedStrategie);
        }

        if(strategie == nullopt || !strategies.setCurrentStrategie(strategie.value()->name)){
            // Damn, for some reason we could not select the strat.
            // Either it does not exist or another one was already selected.
            changeCommand(Talos::gigaTalos::strat_not_found);
            return;
        }

        changeCommand(Talos::gigaTalos::strat_found);

    }



	void LoopV1::stratChangeBis() {

		// We choose if we follow the bright path of the Tirette
		// or the dark path of madness (panic mode)

		log("Checking if we should be panicking", Generic::DEBUG);

        if(strategies.isBackupMode()){
            // We are panicking!
            changeCommand(Talos::gigaTalos::strat_skip_setup);
            return;
        }

        // Bright path.
        changeCommand(Talos::gigaTalos::clear);

	}



	void LoopV1::stratNoneLeft() {

		if (firstTime()) {

			log("No matching strategie available", Generic::ERROR);
            strategies.reset();

		}

		// [TODO] Discuss about life

	}




	void LoopV1::waitingTiretteInsert() {

		if (firstTime()) {
			log("Waiting for tirette insertion", Generic::DEBUG);
		}

		// [TODO] Cockpit is used to change Talos' state using tirette_on
		//		  OR - We poll an internal flag changed by cockpit ?

		// We should add a request packet in case the tirette is already there
		// Or ignore this problem and force people to remove and re-insert the tirette ? THIS.

		if (tirette) {
			changeCommand(Talos::gigaTalos::tirette_on);
		}

	}



	void LoopV1::stratSetup() {

        if(firstTime()){
            log("Setting up the strategie", Generic::DEBUG);
            strategies.init();
            // [TODO] What if it fails to init? We should set the ready flag and let it fail in RUN?
        }

        //Wait for strategie to be ready.
        if(strategies.isReady()) {
            changeCommand(Talos::gigaTalos::strat_setup_ok);
        }

	}



	void LoopV1::waitingTirettePull() {

		if (firstTime()) {
			log("Waiting for tirette removal", Generic::DEBUG);
		}
		// [TODO] Cockpit is used to change Talos' state using tirette_off
		//		  OR - We poll an internal flag changed by cockpit ?

		if (!tirette) {
			changeCommand(Talos::gigaTalos::tirette_off);
		}

	}



	void LoopV1::running() {
		// [TODO] To discuss with @baptistemht
        // This must be non-blocking
        //
        // If the strategie is not running but was never started, we start it
        //	+ ADD FLAG strategie started
        //
        // If the strategie is not running was started
        //	- We check for errors ?
        //  - We check for end reach before timeout ?
        //  - We check for timeout

        // Tries to run the strategie.
        if(firstTime()){
            if(!strategies.run()){
                //either the strat is not ready or already started.
                changeCommand(Talos::gigaTalos::strat_stop_exec_error);
                return;
            }
        }

        // Strategie failure is handled by the overloaded onFailure() function.
        if(strategies.isRunning()){
            if(strategies.hasTimeout()){
                changeCommand(Talos::gigaTalos::timeout);
            }
        }else{
            // How did it die?
            if(strategies.hasTimeout()){
                changeCommand(Talos::gigaTalos::timeout);
            }else{
                changeCommand(Talos::gigaTalos::strat_done_timeout);
            }
        }
	}



	void LoopV1::stratStopExecError() {

		// We have an exec error
		log("Strategie failed to execute properly", Generic::ERROR);

		// [TODO] Doin' some stuff
		//  -> Ask the peripherals to enter the errored state ? Or to reset ?

		// This is maybe too strict, we should modify the state machin to allow
		// Core to choose another strategie
		// This implies a mechanism to red-flag a strategie (which should be easy to add)

		changeCommand(Talos::gigaTalos::strat_exec_error);

	}



	void LoopV1::stratExecError() {

		log("Waiting for user input");

		// [TODO] Cockpit integration

	}


	void LoopV1::waitingMatchEnd() {

		if (firstTime()) {
			log("Waiting for match end", Generic::DEBUG);
		}

        // Change state after timeout.
        if(strategies.hasTimeout()){
            changeCommand(Talos::gigaTalos::timeout);
        }
	}



	void LoopV1::done() {

		if (firstTime()) {
			log("The match is done !");

			strategies.stop(); // Do we need the stop if we reset? TBT with different ending scenario.
			strategies.reset(); //We only reset when the program ended properly. Otherwise, backup won't happen.
		}

		// [TODO] Print calculated score to the screen
		// -> Add this mechanism to strategie
		//    (a simple public int, don't care about read-only safety or whatever)

		// We wait for user input using Cockpit

	}



	void LoopV1::stratSkipSetup() {
        if(firstTime()){
            log("ENTERING PANIC MODE", Generic::ERROR);
            strategies.init();
            // [TODO] What if it fails to init? We should set the ready flag and let it fail in RUN?
        }

        //Wait for strategie to be ready.
        if(strategies.isReady()) {
            changeCommand(Talos::gigaTalos::strat_skipped);
        }

	}



	void LoopV1::peripheralLoss() {

        // Right now, this part is handled internally by the strategie manager.
        // Therefore, we go straight to STRAT_STOP_PERIPHERAL.
        changeCommand(Talos::gigaTalos::peripheral_required);

/*		// We pause the strategie
		peripherals.pause();

		log("Checking if we shoud stop the current strategie", Generic::DEBUG);

		// We check if the strategie cares about the lost peripheral
		if (false  <- OBVIOUSLY WE MUST CHANGE THAT ) {

			// The peripheral was required
			changeCommand(Talos::gigaTalos::peripheral_required);
			return;

		}

		// The peripheral was not required
		changeCommand(Talos::gigaTalos::peripheral_unused);

		// We resume the strategie
		peripherals.resume();*/

	}


	void LoopV1::stratStopPeripheral() {

		log("We must change the strategie, the lost peripheral was required", Generic::ERROR);

		// We resume the strategie
		peripherals.resume();
        // Why do we need to resume?

        strategies.stop();

		changeCommand(Talos::gigaTalos::strat_restart);

	}



	void LoopV1::setupHMI() {

		// Stop command
		cockpit.main.declare(
			"stop", "Stops Core",
			[&] (Cockpit::Request req) {
				stop();
				req.respond("Stopping", "stop");
			}
		);

		cockpit.main.declare("tps", "Returns the number of ticks per second", [&](Cockpit::Request req) {
			req.respond(std::to_string(tps), "tps");
			});

		// Hook Talos controls
		auto controls = cockpit.main.use("talos", "Talos controls");

		controls->declare("start", "Start the strategie", [&](Cockpit::Request req) {
			changeCommand(Talos::gigaTalos::strat_start);

			if (selectedStrategie == "") {
				req.error(124, "No strategie selected");
				log("No strategie selected, cannot start", Generic::ERROR);
				return;
			}

			req.respond("Stratring the strategie called '" + selectedStrategie + "'");
			log("Stratring the strategie called '" + selectedStrategie + "'");
			});

		controls->declare("tirette_on", "The tirette is inserted", [&](Cockpit::Request req) {
			tirette = true;
			req.respond("Set the tirette to on state");
			log("Received tirette_on signal");
			});

		controls->declare("tirette_off", "The tirette is removed", [&](Cockpit::Request req) {
			tirette = false;
			req.respond("Set the tirette to off state");
			log("Received tirette_off signal");
			});

		controls->declare("quit", "Tries to quit Core", [&](Cockpit::Request req) {
			changeCommand(Talos::gigaTalos::quit);
			req.respond("Asked Core to quit");
			log("Received quit signal");
			});

		controls->declare("reset", "Tries to reset Core", [&](Cockpit::Request req) {
			changeCommand(Talos::gigaTalos::reset);
			req.respond("Sent the reset signal");
			log("Received reset signal");

			// Reset the flags
			selectedStrategie = "";
			});

		controls->declare("show", "Shows Talos' current state", [&](Cockpit::Request req) {
			req.respond(printCurrentState(), "talos_state");
			});

		// Strategie specific
		auto strat = cockpit.main.use("strat", "Stratgie-specific controls");

		strat->declare("stop", "Stops the currently-running strategie", [&](Cockpit::Request req) {
			strategies.stop();
			req.respond("Strat stopped");
			});

		strat->declare("list", "List all available strats", [&](Cockpit::Request req) {
			req.respond(strategies.list(), "strat_list");
			});

		auto cmd = strat->declare("set_team", "Defines the team selected", [&](Cockpit::Request req) {
			auto team = req.arg(1);
			int n = atoi(team.c_str());
			if (n <= 0) {
				req.error(123, "Invalid argument, must be a number > 0");
				return;
			}
			strategies.team = n;
			req.respond("Team set to " + std::to_string(n));
			});
		cmd->declaration.addArgument("number", true);

		cmd = strat->declare("select", "Selects a strategie based on its name",
			[&](Cockpit::Request req) {
				auto name = req.arg(1);
				auto strat = strategies.get(name);
				if (strat == std::nullopt) {
					req.error(200, "Strategie " + name + " not found");
					return;
				}
				selectedStrategie = name;
				req.respond("Selected " + name);
			});
		cmd->declaration.addArgument("name", true);
		
		strat->declare("show", "Shows the state of the strat manager", [&](Cockpit::Request req) {
			
			});

		strat->declare("reload", "Reloads all strategies", [&](Cockpit::Request req) {
			strategies.reload();
			req.respond("Reloaded strategies");
			});

		// Peripheral specific
		auto periph = cockpit.main.use("peripheral", "Peripheral-specific methods");
		periph->declare("list", "Lists the peripherals and their states", [&](Cockpit::Request req) {
			req.respond(peripherals.list(), "periph_list");
			});

		periph->declare("reload", "Reloads all peripherals", [&](Cockpit::Request req) {
			peripherals.reload();
			req.respond("Reloaded peripherals");
			});

		cmd = periph->declare("call", "Calls a method of a lua-defined peripheral. The method can't receive arguments", [&](Cockpit::Request req) {
			
			std::string id		= req.arg(1);
			std::string method	= req.arg(2);

			auto p = peripherals.get(id);
			if (p == nullopt) {
				req.error(125, "Peripheral " + id + " not found");
				return;
			}

			auto peripheral = p.value();
			if (peripheral->kind != Peripherals::LUA) {
				req.error(126, "Peripheral " + id + " is not defined using Lua");
				return;
			}

			Lua::Peripheral* lua_peripheral = dynamic_cast<Lua::Peripheral*>(peripheral.get());
			lua_peripheral->call(method);

			req.respond("Called method " + method + " from peripheral " + id);
				
			});
		cmd->declaration.addArgument("id", true);
		cmd->declaration.addArgument("method", true);

		auto conf = cockpit.main.use("config", "Configuration file related commands");
		conf->declare("save", "Saves the config file", [&](Cockpit::Request req) {
			config.save();
			req.respond("Saved the config file");
			});

	
	}


}
