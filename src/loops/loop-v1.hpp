#pragma once
#include "gigaTalos.hpp"
#include "Init.hpp"
#include "cockpit.hpp"
#include "timedLoop.hpp"
#include "hermesCAN.hpp"
#include "hermesUDP.hpp"
#include "yieldLock.hpp"
#include "config.hpp"

#include <iostream>

namespace Core::Loops {

	class LoopV1 :
		public Generic::TimedLoop,
		public Talos::gigaTalos::gigaTalos {

	private:

		/**
		 * @brief Lock to sync threads
		 */
		Generic::YieldLock globalLock;

		/**
		 * @brief Main lua state
		 */
		sol::state& Lua;

		/**
		 * @brief Configuration file
		 */
		Generic::Config config;

		/**
		 * @brief Lua resources manager
		 */
		Lua::ResourceLoader resources;

		/**
		 * @brief Peripheral manager
		 */
		Lua::Manager peripherals;

		/**
		 * @brief Strategie Manager
		 */
		Lua::StratManager strategies;

		/**
		 * @brief Hermes CAN
		 */
		Sockets::HermesCAN hermesCAN;

		/**
		 * @brief Hermes UDP
		 */
		Sockets::HermesUDP hermesUDP;

		/**
		 * @brief Cockpit - The HMI link
		 */
		Cockpit::Cockpit &cockpit;

		/**
		 * @brief [TEMP] Log handler
		 */
		Generic::LogHandler logHandler = nullptr;

		/**
		 * @brief Must this loop still run ?
		 */
		bool main_run = true;

		/**
		 * @brief Self-explanatory
		 */
		bool is_init_ok = true;

		/**
		 * @brief Status of the tirette
		 */
		bool tirette = false;

		/**
		 * @brief 
		 */
		std::string selectedStrategie = "";

	public:

		/**
		 * @brief Creates the Core main loop V1
		 * @param cockpit The cockpit instance
		 */
		LoopV1(Generic::Config& config, sol::state& Lua, Cockpit::Cockpit& cockpit, Generic::LogHandler logHandler);

		/**
		 * @brief Destroys the main loop
		 */
		~LoopV1();

		/**
		 * @brief Main loop 
		 */
		bool loop();

		/**
		 * @brief Logs a message
		 * @param message Message to log
		 * @param level Level of the log
		 */
		void log(std::string message, Generic::LogLevel level = Generic::LOG);

		
		/**
		 * @brief Initializing internal classes
		 */
		void init();

		/**
		 * @brief Initialization failed
		 */
		void initFail();

		/**
		 * @brief We quit Core
		 */
		void endCore();

		/**
		 * @brief We wait for user input
		 */
		void idle();

		// UNDEFINED FOR NOW
		void remoteControl();
		
		/**
		 * @brief We must change the strat
		 */
		void stratChange();

		/**
		 * @brief We check if we are in panic mode
		 */
		void stratChangeBis();

		/**
		 * @brief We found no strat
		 */
		void stratNoneLeft();

		/**
		 * @brief We are waiting for the tirette
		 */
		void waitingTiretteInsert();

		/**
		 * @brief We set up the strat (normal flow)
		 */
		void stratSetup();

		/**
		 * @brief We wait for the tirette pull
		 */
		void waitingTirettePull();

		/**
		 * @brief Main loop
		 */
		void running();

		/**
		 * @brief The strat failed to run - we stop 
		 */
		void stratStopExecError();

		/**
		 * @brief We wait for user input to know if we quit Core or re-init
		 */
		void stratExecError();

		/**
		 * @brief The strat is done but not the timeout
		 */
		void waitingMatchEnd();

		/**
		 * @brief The strat is DONE
		 */
		void done();

		/**
		 * @brief We strat a strat in panic mode
		 */
		void stratSkipSetup();

		/**
		 * @brief We lost a peripheral
		 */
		void peripheralLoss();

		/**
		 * @brief We stop the strat due to required peripheral loss
		 */
		void stratStopPeripheral();

		/**
		 * @brief Creates the Cockpit arborescence
		 */
		void setupHMI();

	};

}