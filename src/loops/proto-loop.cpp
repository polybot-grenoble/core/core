#include "proto-loop.hpp"

namespace Core::Loops {

    using namespace Core::Generic;

    ProtoLoop::ProtoLoop (
            Core::Cockpit::Cockpit &cockpit,
            uint8_t hermesID,
            std::string CANIface,
            int UDPPort,
            std::string examplePeripheral
    ):  cockpit(cockpit), 
        hermesCAN(hermesID), hermesUDP(hermesID), 
        peripherals(Lua, globalLock) {

        cockpit.log("Initializing Prototype Loop");

        // Init Lua
        cockpit.log("Initializing Lua");
        Core::Lua::Init(Lua);
        Lua["manager"] = &peripherals;

        // Init Hermes
        cockpit.log("Starting Hermes");
        cockpit.log("Hermes ID : " + to_string(hermesID), Generic::DEBUG);
        char *rawCAN = (char *) CANIface.c_str();

        // Opening sockets
        if (hermesCAN.open(rawCAN) == Core::Sockets::SOCK_OK) {
            cockpit.log("HermesCAN : " + CANIface, Generic::DEBUG);
        } else {
            cockpit.log("HermesCAN cannot open " + CANIface, Generic::ERROR);
        }

        if (hermesUDP.open(UDPPort) == Core::Sockets::SOCK_OK) {
            cockpit.log("HermesUDP : " + to_string(UDPPort), Generic::DEBUG);
        } else {
            cockpit.log("HermesUDP cannot open " + to_string(UDPPort), Generic::ERROR);
        }

        // Starting Threads
        if (!hermesCAN.start()) {
            cockpit.log("Failed to start HermesCAN", Generic::ERROR);
        }
        if (!hermesUDP.start()) {
            cockpit.log("Failed to start HermesUDP", Generic::ERROR);
        }
        
        // Configuring Peripheral Manager
        peripherals.bufferSendHandler = [&] (HermesBuffer *buffer) -> bool {
            // Would benefit to have something to properly route UDP and CAN
            // buffers separately, for the time beeing, this works 
            auto can = hermesCAN.sendBuffer(buffer);
            auto udp = hermesUDP.sendBuffer(buffer);
            
            return can || udp;
        };

        peripherals.logHandler = [&] (
            std::string message, 
            Core::Generic::LogLevel level
        ) {
                cockpit.log(message, level);
        };

        // Init example Peripheral
        cockpit.log("Creating the example peripheral");
        example = std::make_shared<Core::Lua::Peripheral> (
            Lua, "example", examplePeripheral
        );

        // Attaches Peripheral to Manager
        peripherals.add(example);

        if (example->load()) {
            cockpit.log("Created example peripheral", Generic::DEBUG);
        } else {
            cockpit.log("Failed to create the example peripheral", Generic::ERROR);
        }

    }

    bool ProtoLoop::loop () {

        // We pause the strat
        // ...

        // We read Hermes
        HermesBuffer received;
        
        while (hermesCAN.getBuffer(&received)) {
            peripherals.distribute(&received);
        }

        while (hermesUDP.getBuffer(&received)) {
            peripherals.distribute(&received);
        }
        
        // We send the pending messages
        peripherals.sendToBus();

        // We tick all peripherals
        peripherals.tick();

        // We resume the strat 
        // ...
        
        // For now, run until cockpit closes
        return cockpit.isRunning();

    };

    void ProtoLoop::end () {

        hermesCAN.stop();
        hermesUDP.stop();

    }

}