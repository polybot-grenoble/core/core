#pragma once

#include "timedLoop.hpp"
#include "cockpit.hpp"

#include <hermesCAN.hpp>
#include <hermesUDP.hpp>

#include "peripheralManager.hpp"

#include "Init.hpp"

namespace Core::Loops {

    using namespace Core::Generic;

    /**
     * @brief This prototype of a loop aims to think about
     * how to implement the Peripherals. 
     */
    class ProtoLoop: public TimedLoop {

        private:
        /** Indicates if the loop was properly initialized */
        bool initialized = false;

        public:
        /**
         * @brief Lock to sync threads
         */
        Generic::YieldLock globalLock;

        /** Internal Cockpit reference */
        Core::Cockpit::Cockpit &cockpit;

        /** Internal SocketCAN reference */
        Core::Sockets::SocketCAN can;

        /** Internal Hermes CAN Reference */
        Core::Sockets::HermesCAN hermesCAN;

        /** Internal UDP reference */
        Core::Sockets::HermesUDP hermesUDP;

        /** Internal Talos Reference */

        /** Internal Lua */
        sol::state Lua;

        /** Internal Peripheral Manager Reference */
        Core::Lua::Manager peripherals;

        /** Example peripheral */
        shared_ptr<Core::Lua::Peripheral> example;

        /**
         * @brief Construct a new Proto Loop object 
         */
        ProtoLoop (
            Core::Cockpit::Cockpit &cockpit,
            uint8_t hermesID,
            std::string CANIface,
            int UDPPort,
            std::string examplePeripheral
        );

        /**
         * @brief The loop of the program
         * 
         * @return true  The loop must continue
         * @return false The loop must stop
         */
        bool loop ();

        /**
         * @brief Gracefully stops Hermes 
         */
        void end ();

    };

}