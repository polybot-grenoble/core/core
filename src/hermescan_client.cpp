/**
 * @file hermescan_client.cpp
 * @author Julien Pierson
 * @brief This program is a simple HermesCAN client used to debug things on 
 * a CAN bus which uses Hermes.
 * @version 0.1
 * @date 2023-12-09
 */

#include <iostream>
#include <sys/signal.h>
#include <string>

#include <hermesCAN.hpp>
#include "timedLoop.hpp"
#include "peripheralManager.hpp"
#include "yieldLock.hpp"

// Global running flag
volatile bool running = true;

int main (int argc, char **argv) {

    // Local vars
    Core::Sockets::HermesCAN hermes;
    Core::Sockets::SocketReturnCode code;
    Core::Generic::TimedLoop loop;
    Core::Generic::YieldLock yieldLock;
    Core::Peripherals::Manager peripheralManager(yieldLock);    

    // Checking CLI args
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <can interface> <id>\r\n", argv[0]);
        return -1;
    }

    // We change the client ID
    hermes.hermes.id = atoi(argv[2]);

    // Opening the can bus
    code = hermes.open(argv[1]);
    if (code) {
        fprintf(stderr, "Can't open %s (code %d)\r\n", argv[1], code);
        return code;
    }

    // Starting the reading thread
    hermes.start();

    // Indicating who we are
    printf(
        "> Opening %s as %d (0x%x)\r\n",
        argv[1], hermes.hermes.id, hermes.hermes.id
    );

    // Adding a dummy peripheral (n°12)
    auto Dummy = make_shared<Core::Peripherals::Peripheral>();
    Dummy->hermesID = 12;
    Dummy->family = "example";
    
    if (!peripheralManager.add(Dummy)) {
        cout << "Failed to add dummy peripheral n°12" << endl;
    }

    // Adding a way for the peripheral manager to send messages
    peripheralManager.bufferSendHandler = 
        [&hermes] (HermesBuffer *buffer) {

            return hermes.sendBuffer(buffer);

        };

    // The peripheral sends something
    HermesBuffer fakePacket;
    string lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies nec tellus et commodo. Nunc viverra, tortor eget porttitor accumsan.";
    const char *str = lorem.c_str();

    Hermes_setBufferDestination(&fakePacket, Dummy->hermesID, 12, false);
    fakePacket.len = strlen(str) + 1;
    memcpy(fakePacket.data, str, fakePacket.len);
    fakePacket.pos = 0;
    
    Dummy->send(fakePacket);

    // Main loop
    int hermes_available_count;
    HermesBuffer *buffer;

    uint8_t i = 0;

    while (running) {

        // Get the number of available buffers
        hermes_available_count = Hermes_getCompleteBufferCount(&hermes.hermes);

        // We consume the available buffers
        for (; hermes_available_count; hermes_available_count--) {
            buffer = Hermes_nextBuffer(&hermes.hermes);
            peripheralManager.distribute(buffer);
        }

        // We produce a message -- THIS IS NOT HOW TO PROPERLY USE HERMES
        Hermes_setBufferDestination(&fakePacket, 12, 0xA, 0);
        fakePacket.data[0] = HERMES_T_INT8;
        fakePacket.data[1] = i++;
        fakePacket.len = 2;
        fakePacket.pos = 0;
        Dummy->send(fakePacket);

        // We send the next messages that the peripheral produced
        peripheralManager.sendToBus();

        // Slowing down this empty loop using a dummy tick-based loop
        loop.step();

    }

    // Stopping reading thread
    hermes.stop();

    return 0;

}
