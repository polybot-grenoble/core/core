#include "gigaTalos.hpp"
using namespace Core::Talos::gigaTalos;

//This file is only present to test gigaTalos

int main(){
    gigaTalos testGigaTalos;
    testGigaTalos.printCurrentState();
    cout << endl;
    int newCommand;
    while(1) {
        cout << "Rentrez une commande :\n";
        cin >> newCommand;
        testGigaTalos.changeCommand((commands) newCommand);
        testGigaTalos.exec();
        cout << "Etat actuel : ";
        testGigaTalos.printCurrentState();
        cout << endl;
    }
    return 0;
}