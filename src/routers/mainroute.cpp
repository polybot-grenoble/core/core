#include "mainroute.hpp"

using namespace Core::Cockpit;

namespace Core::Cockpit::Routers {

    MainRoute::MainRoute (
        Cockpit& c
    ) : Router("robot", "Core", c) {

        cockpit.log("Main Cockpit route initializing", Generic::DEBUG);

        // Starting the main thread
        ProcessThread = std::thread(&MainRoute::Thread, this);
                    
    }

    MainRoute::~MainRoute () {

        // Stops the main pthread
        if (ProcessThread.joinable()) {
            pthread_cancel(ProcessThread.native_handle());
            ProcessThread.join();
        }

        cockpit.log("Main route destroyed", Generic::DEBUG);

    }

    void MainRoute::Thread () {

        Packet p, ret;

        cockpit.log("Started Main route thread", Generic::DEBUG);

        /*while (cockpit.waitNextMessage(p)) {

            if (p.type == "stop") {

                cockpit.stop();
                return;

            } else if (p.type == "request") {

                Request req (p);
                
                ret = route(req);
                cockpit.sendMessage(ret);

            } else {

                string err_msg = "Unknown packet type: " + p.type;
                cockpit.log_error(err_msg);

            }

        }   */

        cockpit.log("Stopped Main route thread", Generic::DEBUG);

    }

}
