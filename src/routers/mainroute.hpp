#pragma once
#include "route.hpp"


namespace Core::Cockpit::Routers {

    /**
     * @brief This is the main Cockpit Route. It has to have all the references 
     * needed to handle all of the possible user requests 
     */
    class MainRoute : public Core::Cockpit::Router {

        private:
            /** The ID of the pthread of the main route */
            std::thread ProcessThread;

        public:

            /**
             * @brief Construct a new Main Route object
             * 
             * @param cockpit The reference to cockpit
             */
            MainRoute (Cockpit& cockpit);

            /**
             * @brief Destroy the Main Route object
             */
            ~MainRoute();

            void Thread ();

    };

};

