#include "luaRoute.hpp"

namespace Core::Cockpit::Routers {

    LuaRoute::LuaRoute (Cockpit &cockpit, sol::state &Lua):
        Core::Cockpit::Router("lua", "Use the Lua state", cockpit), 
        Lua(Lua) {
        
        // Declare the commands
        auto ln = declare(
            "run", 
            "Run the following argument as a script (not a file)"
        );
        ln->addArgument("script", true);

    }

    Packet LuaRoute::handle (Request req, std::string command) {

        if (command == "run") {
            return run(req);
        }

        // Placeholder packet
        return req.notYetImplemented();

    }

    Packet LuaRoute::run (Request req) {

        std::string script = req.arg(0);

        // Would benefit to have a semaphore protecting the lua state 
        sol::protected_function_result ret;

        ret = Lua.safe_script(
            script,
            &sol::script_pass_on_error
        );

        if (ret.valid()) {
            std::string response = "";
            sol::object obj = ret;
            if (obj.is<std::string>()) {
                response = obj.as<std::string>();
            }
            return req.respond(response, "response");
        } else {
            sol::error err = ret;
            return req.error(200, err.what());
        }

    }

}

// lua run 'return "a" .. "b"'