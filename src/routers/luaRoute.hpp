#pragma once
#include "route.hpp"
#include "Init.hpp"


namespace Core::Cockpit::Routers {

    class LuaRoute : public Core::Cockpit::Router {

        private:
        sol::state &Lua;

        public:

        /**
         * @brief Construct a new Lua Route object
         * @param cockpit 
         */
        LuaRoute (Cockpit &cockpit, sol::state &Lua);

        /**
         * @brief Handler for the requests
         * 
         * @param req The request to handle
         * @return Packet The response to the request
         */
        Packet handle (Request req, std::string command);    


        Packet run (Request req);

    };

}