#pragma once

extern "C" {
#include "Hermes.h"
}
#include "peripheralProperties.hpp"

#include "talos_mirror.hpp"
#include "logLevel.hpp"

#include <string>
#include <queue>
#include <map>
#include <semaphore>
#include <functional>
#include <chrono>

namespace Core::Peripherals {

    /**
     * @brief Kind of peripheral implementation
     */
    typedef enum {
        /** Peripheral has no implementation */
        NONE,
        /** Peripheral written in C++ */
        PURE,
        /** Peripheral written in Lua */
        LUA,
    } PeripheralKind;

    class PeripheralRequest {
        public:
        /** Semaphore to sync threads */
        std::binary_semaphore lock{1};
        /** Semaphore to block requesting thread until the buffer is recieved */
        std::binary_semaphore data{0};
        
        /** The request has been sent */
        bool sent = false;
        /** The data has been recieved */
        bool fullfilled = false;
        /** The buffer of the request */
        HermesBuffer *buffer;

        /** Time at which the request has been made */
        std::chrono::time_point<std::chrono::system_clock> requestTime;

        /** Maximum time */
        std::chrono::milliseconds duration;

    };

    /**
     * @brief Base class to represent a peripheral 
     */
    class Peripheral {

        private:
        /** Internal semaphore for request operations */
        std::binary_semaphore lock{1};

        /** Queue of outgoing buffers */
        std::queue<HermesBuffer> outgoing;

        /** Map of pending requests */
        std::map<uint16_t, PeripheralRequest*> requests;

        /** Ticks since last Heartbeat */
        uint32_t ticksSinceLastHeartbeat = 0;

        public:
        /** -- Static properties -- */

        /** Human-readable ID of the peripheral */
        std::string id ();

        /** Hermes ID of the peripheral */
        uint8_t hermesID = 0;

        /** The implementation used by the peripheral */
        std::string family = "peripheral";
        
        /** Kind of peripheral implementation */
        PeripheralKind kind = NONE;

        /** Properties of the peripheral */

        /** Maximum duration before considered faulty (in ticks) */
        uint32_t maxTicksSinceLastHeartbeat = 100;

        /** Talos */
        Core::Talos::Mirror::Mirror state;

        /** Error message for the latest error that occured */
        std::string lastErrorMessage = "";

        /**
         * @brief Construct a new Peripheral object
         */
        Peripheral();

        /**
         * @brief Destroy the Peripheral object.
         */
        ~Peripheral ();

        /** -- Definable Handlers -- */

        /**
         * Method that allows the peripheral to log things 
         */
        Generic::LogHandler logHandler = nullptr;

        /**
         * Callback for the yield function
        */
        std::function<void(void)> yieldCallback = nullptr;

        /** -- Static handlers -- */

        void yield ();

        /**
         * @brief Tries to handle a buffer. ASSUMES THAT THE POINTER IS NOT NULL
         * 
         * @param buffer The buffer to handle
         */
        void handleBuffer (HermesBuffer *buffer);

        /**
         * @brief Handles any buffer with command ID >= 4080
         * 
         * @param buffer 
         */
        void handleReservedBuffer (HermesBuffer *buffer);

        /**
         * @brief Logs a message. Defaults to printing to the console if
         * the log handler is undefined.
         * 
         * @param message Message to log
         * @param level Log level
         */
        void log (std::string message, Generic::LogLevel level);
        
        /**
         * @brief Logs a message. Defaults to printing to the console if
         * the log handler is undefined.
         * 
         * @param message Message to log
         */
        void log (std::string message);

        /**
         * @brief Handler for wild buffers. A wild buffer is one that is not 
         * expected.
         * 
         * @param buffer 
         */
        void virtual wildBuffer (HermesBuffer *buffer);

        /**
         * @brief Sends a reset request through Hermes.
        */
        void reset();

        /**
         * @brief Adds a buffer to the send queue. Overwrites the ID to match
         * with the internal peripheral ID.
         * 
         * @param buffer The buffer to send
         */
        void send (HermesBuffer &buffer);

        /**
         * @brief Creates a data request through Hermes. 
         * 
         * __THIS FUNCTION BLOCKS THE THREAD UNTIL THE REQUEST IS FULLFILLED !__
         * 
         * @param buffer The buffer to send AND to recieve data
         * @param duration The maximum duration (in ms) to receive the response
         * 
         * @return - true The data has been recieved, 
         * @return - false The data has not been recieved 
         *               (probably a timeout or the peripheral is dead)
         */
        bool request (
            HermesBuffer *buffer,
            int duration = 0
        );

        /**
         * @brief Indicates if the peripheral waits for data from Hermes.
         * 
         * @return - true Yes,
         * @return - false No
         */
        bool hasPendingRequests ();

        /**
         * @brief Indicates if the peripheral waits to send some data through
         * Hermes.
         * 
         * @return true Yes,
         * @return false No
         */
        bool hasPendingOutgoingBuffers ();

        /**
         * @brief Get the Next Outgoing Buffer object.
         *  
         * @param buffer The buffer to be sent
         * @return true A buffer has been given
         * @return false No buffer was given
         */
        bool getNextOutgoingBuffer (HermesBuffer *buffer);

        /**
         * @brief Unlocks all pending requests without fullfilling them 
         */
        void abortAllRequests ();

        /**
         * @brief Used to count the number of ticks since the last heartbeat
         */
        void tick ();

        /**
         * @brief Is the peripheral connected 
         */
        bool connected ();
        
        /**
         * @brief Sends a heartbeat request for this peripheral
         */
        void heartbeat ();

    };

}