#include "peripheral.hpp"

#include <iostream>

namespace Core::Peripherals {

    Peripheral::Peripheral () {
        
    }

    Peripheral::~Peripheral () {

        // We unlock all requests
        abortAllRequests();

    }

    std::string Peripheral::id () {
        return family + ":" + std::to_string(hermesID);
    }

    void Peripheral::yield () {
        if (yieldCallback != nullptr) {
            yieldCallback();
        }
    }

    void Peripheral::handleBuffer (HermesBuffer *buffer) {

        // We check if this is a reserved command
        if (buffer->command >= 4080) {
            handleReservedBuffer(buffer);
            return;
        };

        // We check if this is a response to a request
        auto requestInMap = requests.find(buffer->command);
        if (requestInMap != requests.end()) {
            
            // This is a request, and we handle it
            PeripheralRequest *req = requestInMap->second;

            // We copy the buffer into the request
            memcpy(req->buffer, buffer, sizeof(HermesBuffer));

            // Take the semaphore from the peripheral
            lock.acquire();

            // We mark it as fullfilled and unlock it
            req->fullfilled = true;
            req->data.release();

            // Releasing the semaphore from the peripheral
            lock.release();

            return;
        }

        // We let wild buffer handler handle the buffer
        wildBuffer(buffer);

    }

    void Peripheral::handleReservedBuffer (HermesBuffer *buffer) {

        std::string message;
        char rawMessage[HERMES_MAX_BUFFER_LEN];

        switch (buffer->command)
        {
        case HERMES_CMD_HEARTBEAT:
            // We reset the tick since last heartbeat counter
            ticksSinceLastHeartbeat = 0;
            state.changeCommand(Talos::Mirror::CANConnected);
            break;

        case HERMES_CMD_PARTIAL_BEGIN:
        case HERMES_CMD_PARTIAL_CONTENT:
            break;

        case HERMES_CMD_SIGNAL: {
            Talos::Mirror::commands command;
            Hermes_getCommandArgument(buffer, &command);
            state.changeCommand(command);
            break;
        }
            

        case HERMES_CMD_LOG:
            // Printing the log message to the console
            // [Structure: just a string]
            Hermes_getCommandArgument(buffer, rawMessage);
            message = std::string(rawMessage);
            log(message);
            break;
        
        case HERMES_CMD_ERROR: { // <- Necessary when declaring vars in case
            // Managing error message
            uint16_t code, command;
            Hermes_getCommandArgument(buffer, &code);     // Retrieving code
            Hermes_getCommandArgument(buffer, &command);  // Retrieving command
            Hermes_rewind(buffer);                        // Rewinding read head

            // If any request matches, we unlock it but not fullfilled
            auto entry = requests.find(command);    // C++ type magic
            if (entry != requests.end()) {
                // Getting the request pointer
                auto req = entry->second;

                // Take the semaphore from the peripheral
                lock.acquire();

                req->fullfilled = false;
                req->data.release();

                // Releasing the semaphore from the peripheral
                lock.release();
            }

            // We log the error
            message = "Recieved error code " + std::to_string(code)
                    + " using command " + std::to_string(command);
            log(message, Generic::ERROR);
            lastErrorMessage = message;

            // We send a signal to #Talos
            state.changeCommand(Talos::Mirror::error);
            break;
        }

        case HERMES_CMD_STATUS: {
            Talos::Mirror::states new_state;
            Hermes_getCommandArgument(buffer, &new_state);        
            state.setState(new_state);
            break;
        }
        
        default:
            break;
        }

    }

    void Peripheral::log (std::string message, Generic::LogLevel level) {

        yield();

        std::string msg = "[" + id() + "] " + message;

        // If the log hanlder is not available, we log to the console
        if (logHandler == nullptr) {
            
            if      (level == Generic::ERROR) msg = "[ERROR] " + msg;
            else if (level == Generic::DEBUG) msg = "[DEBUG] " + msg;
            else                     msg = "[LOG  ] " + msg;

            std::cout << msg << std::endl;

            return;
        }

        // We let logHandler handle the message
        logHandler(msg, level);

    }

    void Peripheral::log (std::string message) {
        log(message, Generic::LOG);
    }

    void Peripheral::wildBuffer (HermesBuffer *buffer) {

        std::string msg;
        msg  = "Unable to handle command " + std::to_string(buffer->command);
        msg += ": Wild buffer handler not defined";

        log(msg, Generic::ERROR);

    }

    void Peripheral::reset(){
        HermesBuffer buffer;
        Hermes_clearBuffer(&buffer);
        Hermes_setBufferDestination(&buffer, hermesID, 4080, 0); //4080 : Talos reset command
        send(buffer);
    }

    void Peripheral::send (HermesBuffer &buffer) {

        // We lock the peripheral
        lock.acquire();
        
        // We push the request to the queue 
        // (which copies it, preventing SEGFAULTs)
        outgoing.push(buffer);

        // We unlock the peripheral
        lock.release();

        yield();

    }

    bool Peripheral::request(HermesBuffer* buffer, int duration) {

        PeripheralRequest *req = new PeripheralRequest();

        // Getting the lock (paranoid measure)
        req->lock.acquire();

        // Attaching the buffer
        req->buffer = buffer;
        req->fullfilled = false;
        req->requestTime = std::chrono::system_clock::now();
        req->duration = std::chrono::milliseconds(duration);

        // Locking the peripheral while modifying the map/queue
        lock.acquire();
        // Putting the request into the map
        requests[buffer->command] = req;

        // Releasing the peripheral
        lock.release();

        // Releasing the lock 
        req->lock.release();

        // Putting the request into the queue
        send(*buffer);

        yield();

        // Waiting for data
        req->data.acquire();

        // Locking the peripheral
        lock.acquire();
        // Clearing the request from the map
        requests.erase(buffer->command);
        // Releasing the peripheral
        lock.release();

        auto ok = req->fullfilled;
        if (!ok) {
            // Talos -> Error
            state.changeCommand(Talos::Mirror::error);
            lastErrorMessage = "Request timed out (or cancelled)";
            log(lastErrorMessage, Generic::ERROR);
        }

        delete req;

        return ok;

    }

    bool Peripheral::hasPendingRequests () {
        return !requests.empty();
    }

    bool Peripheral::hasPendingOutgoingBuffers () {
        return !outgoing.empty();
    }

    bool Peripheral::getNextOutgoingBuffer (HermesBuffer *buffer) {

        // If none available, it failed
        if (!hasPendingOutgoingBuffers()) return false;

        lock.acquire();

        // Retrieving the request to send
        HermesBuffer toSend;
        toSend = outgoing.front();
        outgoing.pop();

        lock.release();

        // Copying the buffer to the one provided
        memcpy(buffer, &toSend, sizeof(HermesBuffer));

        return true;

    }

    void Peripheral::abortAllRequests () {
        
        // Locking the peripheral
        lock.acquire();

        // Emptying the outgoing queue
        while (outgoing.size()) outgoing.pop();

        // Releasing all the requests
        for (auto [id, req] : requests) {
            req->fullfilled = false;
            req->data.release();
        }

        // Emptying the Map
        requests.clear();

        // Unlocks the peripheral
        lock.release();

    }

    void Peripheral::tick () {
        
        lock.acquire();

        // Incrementing the ticks
        ticksSinceLastHeartbeat++;

        // Checking if we timed out
        if (
            ticksSinceLastHeartbeat > maxTicksSinceLastHeartbeat
            && state.getState() == Talos::Mirror::RUNNING
        ) {
            // Transition Talos ...
            state.changeCommand(Talos::Mirror::CANDisconnected);
            lastErrorMessage = "Heartbeat timeout";
            log(lastErrorMessage, Generic::ERROR);
        }

        // Checking if a request timed out
        for (auto &r: requests) {
            auto req = r.second;
            auto now = std::chrono::system_clock::now();

            if (    req->duration > 0ms 
                && (now - req->requestTime) > req->duration
            ) {
                req->fullfilled = false;
                req->data.release();
            }
        }

        state.nextState();

        lock.release();

        // Sending a Heartbeat request periodically
        // HERE TO PREVENT INTERLOCK
        auto shouldHeartbeat = 
               (ticksSinceLastHeartbeat % maxTicksSinceLastHeartbeat) 
            == (maxTicksSinceLastHeartbeat / 3);

        if (shouldHeartbeat) {
            heartbeat();
        }

    }

    bool Peripheral::connected () {
        return state.getState() == Talos::Mirror::RUNNING;
    }

    void Peripheral::heartbeat () {

        HermesBuffer hbPacket;

        // Initializing the buffer
        Hermes_clearBuffer(&hbPacket);

        // Configure as heartbeat
        Hermes_setBufferDestination(
            &hbPacket, hermesID, HERMES_CMD_HEARTBEAT, 0
        );

        send(hbPacket);

    }

}
