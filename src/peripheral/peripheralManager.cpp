#include "peripheralManager.hpp"

#include <iostream>

namespace Core::Peripherals {

    Manager::Manager (Generic::YieldLock& globalLock) : globalLock(globalLock) {
        // Nothing yet
    }

    Manager::~Manager () {
        // Nothing yet
    }

    void Manager::distribute (HermesBuffer *buffer) {

        // We rewind the buffer to be able to read the arguments
        Hermes_rewind(buffer); 
        
        // We look for the peripheral matching the buffer
        auto ref = peripherals.find(buffer->remote);
        if (ref != peripherals.end()) {

            // We let the peripheral handle its buffer
            shared_ptr<Peripheral> P = ref->second;
            P->handleBuffer(buffer);

        } else {
            // There is none
            unknownPeripheral(buffer);
        }

        // We clear the buffer 
        Hermes_clearBuffer(buffer);

    }

    void Manager::unknownPeripheral (HermesBuffer *buffer) {

        // We log to the console the fact that a random dude came here
        // and sent us a message. Who does that ??!

        std::string message = 
            "Recieved message with the ID " + to_string(buffer->command)
            + " sent by peripheral N°" + to_string(buffer->remote)
            + " which is not known by the peripheral manager.";

        log(message, Generic::ERROR);

    }

    bool Manager::exists (uint8_t hermesID) {

        return peripherals.find(hermesID) != peripherals.end();

    }

    bool Manager::add (shared_ptr<Peripheral> P) {
        
        // Check if ID aleready in use
        if (exists(P->hermesID)) return false;

        // Add the peripheral
        peripherals.insert({ P->hermesID, P });

        // Attaches the peripheral log function to the manager's one
        P->logHandler = 
            [this](
                std::string message, 
                Generic::LogLevel level
            ) {
                this->log(message, level);
            };

        P->yieldCallback = [&](){
            globalLock.yield();
        };

        return true;

    }

    void Manager::remove (uint8_t hermesID) {

        auto ref = peripherals.find(hermesID);

        // This peripheral does not exist
        if (ref == peripherals.end()) return;

        // We delete the peripheral form the manager
        peripherals.erase(hermesID);

    }

    void Manager::log (std::string message, Generic::LogLevel level) {

        std::string msg = "[Peripheral Manager] " + message;

        // If the log hanlder is not available, we log to the console
        if (logHandler == nullptr) {
            
            if      (level == Generic::ERROR) msg = "[ERROR] " + msg;
            else if (level == Generic::DEBUG) msg = "[DEBUG] " + msg;
            else                     msg = "[LOG  ] " + msg;

            std::cout << msg << std::endl;

            return;
        }

        // We let logHandler handle the message
        logHandler(msg, level);

    }

    void Manager::sendToBus () {

        // If there is no method to send the buffer, we don't try
        if (!bufferSendHandler) return;

        // We check if each of the peripherals has to send something
        for (auto &[key, peripheral] : peripherals) {

            // If the peripheral has no buffer to send, we skip it
            if (!peripheral->hasPendingOutgoingBuffers()) continue;
                
            HermesBuffer temp;
            while (peripheral->getNextOutgoingBuffer(&temp)) {
                // Making sure to send to the peripheral
                temp.remote = peripheral->hermesID;

                // If the send fails, we loose the buffer :/
                bool success = bufferSendHandler(&temp);

                if (!success) {
                    log("Unable to send a buffer. It's lost now.", Generic::ERROR);
                }
            }

        }

    }

    void Manager::tick () {

        for (auto &[key, p]: peripherals) {
            p->tick();

            // If there is no state change, we go on
            if (p->state.isQueueEmpty()) continue;

            // We check the change
            auto change = p->state.takeFirstInfos();

            if (change.currentState != Talos::Mirror::RUNNING) {
                if (peripheralLoss == nullptr) {
                    log("Lost peripheral " + p->id(), Generic::ERROR);
                } else {
                    peripheralLoss(p->id());
                }
            } else if (change.currentState == Talos::Mirror::RUNNING) {
                log(p->id() + " connected", Generic::LOG);
            }

        }

    }

    bool Manager::hasPeripheral (std::string id) {

        std::function<bool(shared_ptr<Peripheral>)> filter;

        if (id.find(':') != id.npos) {
            filter = [&id] (shared_ptr<Peripheral> p) { 
                return (p->id() == id) && p->connected(); 
            };
        } else {
            filter = [&id] (shared_ptr<Peripheral> p) { 
                return (p->family == id) && p->connected(); 
            };
        }

        for (auto &[key, p] : peripherals) {

            if (filter(p)) {
                return true;
            }

        }

        return false;

    }

    std::optional<shared_ptr<Peripheral>> Manager::get (std::string id) {
        
        if (!hasPeripheral(id)) return std::nullopt;

        for (auto &[key, value] : peripherals) {
            if (
                   value->id() == id
                && value->connected()
            ) {
                return value;
            }
        }

        return std::nullopt;

    }
    
    std::optional<shared_ptr<Peripheral>> Manager::find (std::string search) {
        
        if (!hasPeripheral(search)) return std::nullopt;

        for (auto &[key, value] : peripherals) {
            if (
                   (value->family == search || value->id() == search)
                && value->connected()
            ) {
                return value;
            }
        }

        return std::nullopt;

    }

    std::optional<shared_ptr<Peripheral>> Manager::force_find (
        std::string search
    ) {
        
        for (auto &[key, value] : peripherals) {
            if (
                (value->family == search || value->id() == search)
            ) {
                return value;
            }
        }

        return std::nullopt;

    }

    std::string Manager::list () {

        std::stringstream s;

        for (auto &[key, peripheral] : peripherals) {

            s << peripheral->id() 
              << "@" 
              << peripheral->state.getStateText()
              << ";"; 

        }

        return s.str();

    }

    void Manager::setPeripheralsTimeoutTicksNumber (int ticks) {
        for (auto &[key, peripheral]: peripherals) {
            peripheral->maxTicksSinceLastHeartbeat = ticks;
        }
    }

    bool Manager::pause() {
        return globalLock.pause();
    }

    void Manager::resume() {
        globalLock.resume();
    }

    void Manager::abort() {
        for (auto& [key, p] : peripherals) {
            p->abortAllRequests();
        }
    }

}

/**
Yb        dP 88  88 888888 88b 88     Yb  dP  dP"Yb  88   88     
 Yb  db  dP  88  88 88__   88Yb88      YbdP  dP   Yb 88   88     
  YbdPYbdP   888888 88""   88 Y88       8P   Yb   dP Y8   8P     
   YP  YP    88  88 888888 88  Y8      dP     YbodP  `YbodP'     
                                                                                             
888888 88 88b 88 8888b.       dP"Yb  88   88 888888              
88__   88 88Yb88  8I  Yb     dP   Yb 88   88   88                
88""   88 88 Y88  8I  dY     Yb   dP Y8   8P   88                
88     88 88  Y8 8888Y"       YbodP  `YbodP'   88                
                                                                 
Yb  dP  dP"Yb  88   88     .dP"Y8 888888 88 88     88            
 YbdP  dP   Yb 88   88     `Ybo."   88   88 88     88            
  8P   Yb   dP Y8   8P     o.`Y8b   88   88 88  .o 88  .o        
 dP     YbodP  `YbodP'     8bodP'   88   88 88ood8 88ood8        
                                                                 
 dP""b8    db    88b 88        db    8888b.  8888b.              
dP   `"   dPYb   88Yb88       dPYb    8I  Yb  8I  Yb             
Yb       dP__Yb  88 Y88      dP__Yb   8I  dY  8I  dY             
 YboodP dP""""Yb 88  Y8     dP""""Yb 8888Y"  8888Y"              
                                                                 
8b    d8 888888 8b    d8 888888 .dP"Y8     888888  dP"Yb         
88b  d88 88__   88b  d88 88__   `Ybo."       88   dP   Yb        
88YbdP88 88""   88YbdP88 88""   o.`Y8b       88   Yb   dP        
88 YY 88 888888 88 YY 88 888888 8bodP'       88    YbodP         
                                                                 
Yb  dP  dP"Yb  88   88 88""Yb      dP""b8  dP"Yb  8888b.  888888 
 YbdP  dP   Yb 88   88 88__dP     dP   `" dP   Yb  8I  Yb 88__   
  8P   Yb   dP Y8   8P 88"Yb      Yb      Yb   dP  8I  dY 88""   
 dP     YbodP  `YbodP' 88  Yb      YboodP  YbodP  8888Y"  888888 
 */

//                 .
//                .;;:,.
//                 ;iiii;:,.                                   .,:;.
//                 :i;iiiiii:,                            .,:;;iiii.
//                  ;iiiiiiiii;:.                    .,:;;iiiiii;i:
//                   :iiiiiiiiiii:......,,,,,.....,:;iiiiiiiiiiii;
//                    ,iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii:
//                     .:iii;iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii;,
//                       .:;;iiiiiiiiiiiiiiiiiiiiiiiiiii;;ii;,
//                        :iiii;;iiiiiiiiiiiiiii;;iiiiiii;:.
//                       ,iiii;1f:;iiiiiiiiiiii;if;:iiiiiii.
//                      .iiiii:iL..iiiiiiiiiiii;:f: iiiiiiii.
//                      ;iiiiii:.,;iiii;iiiiiiii:..:iiiiiiii:
//                     .i;;;iiiiiiiiii;,,;iiiiiiiiiiii;;iiiii.
//                     ::,,,,:iiiiiiiiiiiiiiiiiiiiii:,,,,:;ii:
//                     ;,,,,,:iiiiiiii;;;;;;;iiiiii;,,,,,,;iii.
//                     ;i;;;;iiiiiiii;:;;;;;:iiiiiii;::::;iiii:
//                     ,iiiiiiiiiiiiii;;;;;;:iiiiiiiiiiiiiiiiii.
//                      .iiiiiiiiiiiiii;;;;;iiiiiiiiiiiiiiiiiii:
//                       .;iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii;
//                        ;iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii.
//                       .;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,
