#pragma once

extern "C" {
#include "Hermes.h"
}
#include "peripheral.hpp"
#include "logLevel.hpp"
#include "yieldLock.hpp"

#include <string>
#include <queue>
#include <map>
#include <optional>

using namespace std;

namespace Core::Peripherals {

    class Manager {

        protected:
            /** 
             * Map containing all known perpiherals, indexed by their Hermes ID
             */
            map<uint8_t, shared_ptr<Peripheral>> peripherals;

        public:
            /**
             * Method that allows the peripheral manager to log things 
             */
            Generic::LogHandler logHandler = nullptr;

            /**
            * @brief Lock to pause a peripheral call
            */
            Generic::YieldLock &globalLock;
            
            /**
             * @brief Method that allows the peripheral manager to send 
             * the peripherals' pending buffers 
             */
            std::function<
                bool(HermesBuffer *buffer)
            > bufferSendHandler = nullptr;

            /**
             * @brief Method that is called when a peripheral goes out of 
             * its running state
             */
            std::function<void(std::string id)> peripheralLoss = nullptr;

            /**
             * @brief Construct a new Manager object
             */
            Manager (Generic::YieldLock& globalLock);

            /**
             * @brief Destroy the Manager object. Unlocks all 
             * pending mutexs in the peripherals
             */
            ~Manager();

            /**
             * @brief Distributes the buffer to the associated peripheral.
             * Clears the buffer when done. 
             * ASSERTS THAT THE BUFFER POINTER IS NOT NULL !
             * 
             * @param buffer The buffer to distribute
             */
            void distribute (HermesBuffer *buffer);

            /**
             * @brief Default behaviour when a peripheral is not found
             * 
             * @param buffer The buffer that comes from somewhere nebulous
             */
            void unknownPeripheral (HermesBuffer *buffer);

            /**
             * @brief Checks if a peripheral aleready uses an ID
             * 
             * @param hermesID The ID to check
             * @return true In use,
             * @return false Not in use
             */
            bool exists (uint8_t hermesID);

            /**
             * @brief Tries to add a peripheral to the ones aleready managed
             * 
             * @param P The peripheral to manage
             * @return true The peripheral has been added,
             * @return false The Hermes ID is already in use,  
             *               or the pointer to the peripheral is null
             */
            bool add (shared_ptr<Peripheral> P);

            /**
             * @brief Removes a peripheral from the managed ones
             * 
             * @param hermesID The Hermes ID of the peripheral to remove
             */
            void remove (uint8_t hermesID);

            /**
             * @brief Logs a message. Defaults to printing to the console if
             * the log handler is undefined.
             * 
             * @param message Message to log
             * @param level Log level
             */
            void log (std::string message, Generic::LogLevel level);

            /**
             * @brief Sends all pending peripheral buffers 
             */
            void sendToBus ();

            /**
             * @brief Trigger all peripherals ticks
             */
            void tick ();

            /**
             * Checks if a peripheral is known and working
             * based on it's id
             * 
             * @param id The id or family id of the peripheral
            */
            bool hasPeripheral (std::string id);

            /**
             * Gets a specific peripheral based on it's id
             * 
             * @param id The peripheral id
            */
            std::optional<shared_ptr<Peripheral>> get (std::string id);

            /**
             * Tries to find the requested peripheral based on 
             * it's family id or peripheral id
            */
            std::optional<shared_ptr<Peripheral>> find (std::string search);

            /**
             * Tries to find the requested peripheral based on 
             * it's family id or peripheral id, even if the peripheral is not
             * connected (used for debug purposes)
            */
            std::optional<shared_ptr<Peripheral>> force_find (
                std::string search
            );

            /**
             * @brief Lists all declared peripherals 
             * with their states, separated using semicolons
             */
            std::string list ();

            /**
             * @brief Set the Peripherals Timeout Ticks Number for Heartbeat
             * or Request timeout
             * 
             * @param ticks Number of ticks after which the timeout is triggered
             */
            void setPeripheralsTimeoutTicksNumber (int ticks);

            /**
             * @brief Pauses the execution of the next peripheral method call
             * @return true if paused, false if failed
             */
            bool pause();

            /**
             * @brief Resumes all peripheral calls
             */
            void resume();

            /**
             * @brief Aborts all pending peripheral request
             */
            void abort();

    };

}