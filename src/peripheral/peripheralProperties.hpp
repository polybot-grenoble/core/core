#pragma once

#include <string>
#include <semaphore>
#include <nlohmann/json.hpp>
#include <functional>

namespace Core::Peripherals {
    
    /**
     * @brief Object use to store Peripheral properties using JSON.
     */
    class PeripheralProperties {

        private:
        /** The value used to store the properties */
        nlohmann::json vault;

        /** The internal semaphore to stay thread-safe */
        std::binary_semaphore lock {1};

        public:

        /**
         * Callback for the yield function
        */
        std::function<void(void)> yieldCallback = nullptr;

        /**
         * Checks if this execution flow should be paused
        */
        void yield ();

        /**
         * @brief Construct a new Peripheral Properties object
         */
        PeripheralProperties ();

        /**
         * @brief Construct a new Peripheral Properties object
         */
        PeripheralProperties (PeripheralProperties &);

        /** 
         * Dumps the stored JSON as a string 
         * 
         * @param compact Should the JSON be compacted or be human readable ?
         * 
         * @return The JSON string
         */
        std::string dump(bool compact);

        /**
         * @brief Loads a dump into the vault
         * 
         * @param raw The dump
         * @return true The load is successfull
         * @return false The load failed
         */
        bool load (std::string raw);

        /**
         * @brief Sets associates a number to a key
         * 
         * @param key The key of the value
         * @param value The number
         */
        void set (std::string key, double value);

        /**
         * @brief Sets associates a null-terminated string to a key
         * 
         * @param key The key of the value
         * @param value The null-terminated string
         */
        void set (std::string key, std::string value);

        /**
         * @brief Sets associates a boolean to a key
         * 
         * @param key The key of the value
         * @param value The boolean
         */
        void set (std::string key, bool value);

        /**
         * @brief Tries to get a number from the properties
         * 
         * @param key The key to retrieve
         * @param value The pointer to the memory to store the value
         * 
         * @return true The value has been retrieved
         * @return false The value does not exists, or is not a number
         */
        bool get (std::string key, double *value);

        /**
         * @brief Tries to get a null-terminated string from the properties
         * 
         * @param key The key to retrieve
         * @param value The pointer to the memory to store the value
         * 
         * @return true The value has been retrieved
         * @return false The value does not exists, or is not a string
         */
        bool get (std::string key, std::string *value);

        /**
         * @brief Tries to get a boolean from the properties
         * 
         * @param key The key to retrieve
         * @param value The pointer to the memory to store the value
         * 
         * @return true The value has been retrieved
         * @return false The value does not exists, or is not a boolean
         */
        bool get (std::string key, bool *value);

        /**
         * @brief Removes a value and it's key
         * 
         * @param key The key to remove
         */
        void clear (std::string key);

    };

}