#include "peripheralProperties.hpp"

namespace Core::Peripherals {

    void PeripheralProperties::yield () {
        if (yieldCallback != nullptr) {
            yieldCallback();
        }
    }

    PeripheralProperties::PeripheralProperties () {
        // Initializing as an empty object
        vault = nlohmann::json::object();
        lock.release();
    }

    PeripheralProperties::PeripheralProperties (PeripheralProperties &p) {
        vault = p.vault;
        lock.release();
    }

    std::string PeripheralProperties::dump (bool compact) {
                
        // Gets the semaphore
        lock.acquire();

        // If not compact, add 4 spaces indentation
        std::string str = vault.dump(compact ? -1 : 4);
        
        // Release the semaphore
        lock.release();

        return str;

    }

    bool PeripheralProperties::load (std::string raw) {

        nlohmann::json j;
        
        // Tries to load the JSON
        try {
            j = nlohmann::json::parse(raw);
        } catch (nlohmann::json::parse_error& ex) {
            // Silently fails
            return false;
        }

        // Succeds

        // Gets the semaphore
        lock.acquire();

        // Writes
        vault = j;

        // Release the semaphore
        lock.release();

        return true;

    }

    void PeripheralProperties::set (std::string key, double value) {

        yield();

        // Gets the semaphore
        lock.acquire();

        vault[key] = value;

        // Release the semaphore
        lock.release();
    }

    void PeripheralProperties::set (std::string key, std::string value) {

        yield();

        // Gets the semaphore
        lock.acquire();

        vault[key] = value;

        // Release the semaphore
        lock.release();
    }

    void PeripheralProperties::set (std::string key, bool value) {

        yield();
        
        // Gets the semaphore
        lock.acquire();

        vault[key] = value;

        // Release the semaphore
        lock.release();
    }

    bool PeripheralProperties::get (std::string key, double *value) {

        yield();

        // Gets the semaphore
        lock.acquire();

        // Check if the key exists
        if (!vault.contains(key)) {
            lock.release();
            return false;
        }

        // Check if the value is a number
        if (!vault[key].is_number()) {
            lock.release();
            return false;
        }

        // We give the value to the user
        *value = vault[key];

        // Release the semaphore
        lock.release();

        return true;

    }

    bool PeripheralProperties::get (std::string key, std::string *value) {

        yield();

        // Gets the semaphore
        lock.acquire();

        // Check if the key exists
        if (!vault.contains(key)) {
            lock.release();
            return false;
        }

        // Check if the value is a string
        if (!vault[key].is_string()) {
            lock.release();
            return false;
        }

        // We give the value to the user
        *value = vault[key];

        // Release the semaphore
        lock.release();

        return true;        

    }
    

    bool PeripheralProperties::get (std::string key, bool *value) {

        yield();

        // Gets the semaphore
        lock.acquire();

        // Check if the key exists
        if (!vault.contains(key)) {
            lock.release();
            return false;
        }

        // Check if the value is a number
        if (!vault[key].is_boolean()) {
            lock.release();
            return false;
        }

        // We give the value to the user
        *value = vault[key];

        // Release the semaphore
        lock.release();

        return true;

    }

    void PeripheralProperties::clear (std::string key) {

        yield();

        // Gets the semaphore
        lock.acquire();

        vault.erase(key);

        // Release the semaphore
        lock.release();

    }
    

}