#include <iostream>

#include "peripheral/peripheralManager.hpp"
#include "strategies/stratManager.hpp"
#include "strategies/strategie.hpp"

using namespace std;
using namespace Core;

int main(int argc, char* argv[]) {
    Peripherals::Manager peripheralManager;
    Strats::Manager strategiesManager(
        peripheralManager,
        chrono::seconds(20),
        5,
        chrono::milliseconds(250),
        Strats::BACKUP_MODE_SIMPLE
    );

    shared_ptr<Strats::Strategie> stratA =
        make_shared<Strats::Strategie>(peripheralManager);

    shared_ptr<Strats::Strategie> stratB =
            make_shared<Strats::Strategie>(peripheralManager);

    shared_ptr<Peripherals::Peripheral> P = make_shared<Peripherals::Peripheral>();
    P->hermesID = 12;
    P->family = "base_mobile";
    peripheralManager.add(P);

    stratB->name = "Strat B";
    stratB->setup();

    stratA->name = "Strat A";
    stratA->requiredPeripherals.assign(1, "base_mobile");
    stratA->setup();
    stratA->fallback = stratB->name;

    strategiesManager.add(stratA);
    strategiesManager.add(stratB);

    cout << peripheralManager.list() << endl;
    cout << strategiesManager.list() << endl;

    strategiesManager.onFailure = [&]() {
        //TEST ONLY. IN REALITY, WE NEED TO CHANGE GIGA TALOS STATE.
        strategiesManager.log("Aborting current strategie.", Generic::ERROR);
        strategiesManager.stop();

        auto backupStrat = strategiesManager.findBackupStrategie();

        if (backupStrat == nullopt) {
            strategiesManager.log("No backup strategie found.", Generic::ERROR);
            strategiesManager.reset();
        }
        else {
            strategiesManager.setCurrentStrategie(backupStrat.value()->name);
            strategiesManager.init();

            while (!strategiesManager.isReady()) {
                this_thread::yield();
            }

            strategiesManager.run();

        }

    };

    /** END OF TEST SETUP ---------------------------------------------------*/

    strategiesManager.setCurrentStrategie(stratA.get()->name);

    strategiesManager.init();

    cout << "TEST 1 : MAKE SURE WATCHDOG CAN STOP EVERYTHING." << endl;

    int i = 0;
    cin >> i;
    //You must leave enough time between init() and run() because run() requires isReady() to be true.

    strategiesManager.run();

    while(strategiesManager.isRunning()){
        peripheralManager.tick();
        this_thread::yield();
    }

    cin >> i;
    cout << "end of program" << endl;

    strategiesManager.stop();
    strategiesManager.reset();

    /**
    cout << "TEST 2 FORCE STOP" << endl;

    if (!strategiesManager.setCurrentStrategie(stratA.get()->name)) {
        cout << "Strat not set" << endl;
    }else{
        cout << "Strat is set" << endl;
    }

    if (!strategiesManager.init()) {
        cout << "Strat failed to init" << endl;
    }else{
        cout << "Strat init" << endl;
    }

    cin >> i;

    if (!strategiesManager.run()) {
        cout << "Strat failed to run" << endl;
    }else{
        cout << "Strat running" << endl;
    }

    cin >> i;

    strategiesManager.stop();
    strategiesManager.reset();

    cout << "TEST 3 : RESET AFTER INIT" << endl;

    cin >> i;

    if (!strategiesManager.setCurrentStrategie(stratA.get()->name)) {
        cout << "Strat not set" << endl;
    }else{
        cout << "Strat is set" << endl;
    }

    if (!strategiesManager.init()) {
        cout << "Strat failed to init" << endl;
    }else{
        cout << "Strat init" << endl;
    }

    strategiesManager.reset();
    */

    return 0;
}