#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

using namespace std;
using json = nlohmann::json;

namespace Core::Cockpit { 
    
    struct Packet {
        /**
         * @brief Unique identifier for this packet
         */
        string uid;
        /**
         * @brief Type of packet
         */
        string type;
        /**
         * @brief Payload carried by the packet
         */
        string payload;
        /**
         * @brief Client which emitted the packet
         */
        string client;

        /**
         * @return The packet as a JSON object
         */
        json raw ();

        /**
         * @brief Tries to create a packet from a JSON object
         * @param data The JSON object to import
         */
        bool from (json data);
    };

    /**
     * @brief Overriding << to print Packet to the console
     * @param out The output stream
     * @param p The packet to print
     * @return The output stream
     */
    std::ostream& operator<<(std::ostream& out, Packet p);


    /**
     * @brief Function that handles a packet
     */
    typedef std::function<void(Packet&)> PacketHandler;

}