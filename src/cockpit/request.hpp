#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "commandParser.hpp"
#include "packet.hpp"

using namespace std;

namespace Core::Cockpit {
    
    class Request {
        private:
            /**
             * @brief Depth travelled in the command
             */
            int depth = 0;

        public:
            /**
             * @brief Function used to send the response to the request
             */
            PacketHandler responseTunnel = nullptr;

            /**
             * @brief Command path to follow
             */
            Command command;
            
            /**
             * @brief Unique ID of the request
             */
            string uid;

            /**
             * @brief Client which sent the request
             */
            string client;
            
            /**
             * @brief Responds to the request
             * @param payload The payload to respond
             * @param type The type of packet for the response
             */
            void respond (string payload, string type = "ok");

            /**
             * @brief Responds to the request with an error packet
             * @param code The code of the error
             * @param payload The description of the error
             */
            void error (int code, string payload);

            /**
             * @brief Sends a "Not found" error
             */
            void notFound ();

            /**
             * @brief Sends a "too few arguments" error
             */
            void tooFewArgs ();

            /**
             * @brief Sends a "not yet implemented" error
             */
            void notYetImplemented ();

            /**
             * @return The ID of the command
             */
            string id ();

            /**
             * @return The number of arguments left in the command 
             */
            int argsLeft ();

            /**
             * @brief Returns the n-th argument left. If n > argsLeft, 
             * this function returns an empty string
             * - arg(0) is the command ID
             * 
             * @param n The index of the argument
             * @return string The argument
             */
            string arg (int n);

            /**
             * @brief Increases the depth variable
             */
            void dive();

            /**
             * @brief Constructor for the request
             */
            Request();

            /**
             * @brief Constructor for the request
             * @param p The request packet
             */
            Request (Packet p);
    };

}