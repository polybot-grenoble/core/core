#include "packet.hpp"

#include <iostream>

namespace Core::Cockpit {

    /*
    88""Yb    db     dP""b8 88  dP 888888 888888 
    88__dP   dPYb   dP   `" 88odP  88__     88   
    88"""   dP__Yb  Yb      88"Yb  88""     88   
    88     dP""""Yb  YboodP 88  Yb 888888   88   
    */

    json Packet::raw () {
        
        json j;

        j["uid"] = this->uid;
        j["type"] = this->type;
        j["payload"] = this->payload;
        j["client"] = this->client;

        return j;

    }

    bool Packet::from (json data) {

        if (!data["uid"].is_string()) {
            return false;
        }
        this->uid = data["uid"];

        if (!data["type"].is_string()) {
            return false;
        }
        this->type = data["type"];

        if (!data["payload"].is_string()) {
            return false;
        }
        this->payload = data["payload"];

        if (!data["client"].is_string()) {
            return false;
        }
        this->client = data["client"];

        return true;
        
    }

    std::ostream& operator<<(std::ostream& out, Packet p) {
        
        if (p.client != "") {
            out << "[" << p.client << "] ";
        }
        else {
            out << "[unknown] ";
        }

        if (p.uid != "") {
            out << "(" << p.uid << ") ";
        }

        out << p.type << " : " << p.payload;

    }

}