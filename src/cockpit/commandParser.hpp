#pragma once

#include <vector>
#include <string>

namespace Core::Cockpit {

    /// @brief A Cockpit command that has been parsed 
    typedef std::vector<std::string> Command;

    /**
     * Parses a Cockpit command string the same manner as it's Typescript
     * counterpart.
     * 
     * @param cmd The string to parse
     * @return The parsed command
     * 
     * @note `peripheral list` will return `[ peripheral, list ]`
     * @note `string test "It's an \\"orange\\""` 
     *        will return `[ string, test, It's an "orange" ]`
     * 
     */
    Command parseCommand (std::string cmd);    

}