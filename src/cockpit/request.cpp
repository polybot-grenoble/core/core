#include "request.hpp"

using namespace std;

namespace Core::Cockpit {

    /*         
    88""Yb 888888  dP"Yb  88   88 888888 .dP"Y8 888888 
    88__dP 88__   dP   Yb 88   88 88__   `Ybo."   88   
    88"Yb  88""   Yb b dP Y8   8P 88""   o.`Y8b   88   
    88  Yb 888888  `"YoYo `YbodP' 888888 8bodP'   88                      
    */

    Request::Request() {
        command = parseCommand("");
        uid = "";
        client = "";
    }

    Request::Request (Packet p) {

        // Setting up the request
        this->command   = parseCommand(p.payload);
        this->uid       = p.uid;
        this->client    = p.client;

    }

    void Request::respond (string payload, string type) {
        
        Packet p;

        p.uid = this->uid;
        p.type = type;
        p.payload = payload;
        p.client = this->client;

        if (responseTunnel) {
            responseTunnel(p);
        }

    }

    void Request::error (int code, string message) {

        Packet p;
        json j;

        j["code"] = code;
        j["message"] = message;

        p.uid = this->uid;
        p.type = "error";
        p.payload = j.dump();

        responseTunnel(p);

    }

    void Request::notFound () {
        error(100, "Command not found");
    }

    void Request::tooFewArgs () {
        error(101, "Too few arguments");
    }

    void Request::notYetImplemented () {
        error(103, "Not yet implemented");
    }

    string Request::id () {

        return command[depth];

    }

    int Request::argsLeft () {
        
        return command.size() - depth;

    }

    string Request::arg (int n) {

        string out = "";
        if (n > argsLeft()) {
            return out;
        }
        out = command[depth + n];
        return out;
        
    }

    void Request::dive() {
        depth++;
    }

}