#pragma once

#include <string>
#include <vector>
#include <map>

#include <nlohmann/json.hpp>

#include "commandParser.hpp"
#include "packet.hpp"
#include "request.hpp"

using namespace std;

namespace Core::Cockpit {
        
    typedef enum { CommandRoute, NestedRoute } RouteType;

    /**
     * @brief Argument of a Help message
     */
    struct HelpArg {
        /**
         * @brief Is the argument required ?
         */
        bool required;
        /**
         * @brief Name of the argument
         */
        string name;

        /**
         * @return JSON representation of the argument
         */
        nlohmann::json json();
    };

    /**
     * @brief Represents the help for a command
     */
    struct HelpLine {
        /**
         * @brief Name of the command
         */
        string id;
        /**
         * @brief Is it a command or a nested router ?
         */
        RouteType type;
        /**
         * @brief Description about the command
         */
        string description;
        /**
         * @brief Arguments used by the command
         */
        vector<HelpArg> args;

        /**
         * @return JSON representation of the Help line
         */
        nlohmann::json json();

        /**
         * @brief Adds an argument to the command help line
         * @param name Name of the argument
         * @param required Is the argument required ?
         */
        void addArgument(string name, bool required);

        /**
         * @return The minimum number of arguments expected by the command
         */
        int requiredArgsCount ();

    };

    /**
     * @brief Represents a route's help message
     */
    struct HelpMessage {
        /**
         * @brief Name of the route
         */
        string title;
        /**
         * @brief Short description of the route
         */
        string description;
        /**
         * @brief Help lines of the commands of the route
         */
        vector<HelpLine> lines;

        /**
         * @return JSON representation of the route's Help
         */
        nlohmann::json json();
    };

    /**
     * @brief Function which handles a request command
     */
    typedef std::function<void(Request &)> RequestCommandHandler;
    
    struct RouterCommand {
        /**
         * @brief Declaration of the command
         */
        HelpLine declaration;

        /**
         * @brief Handler for the command
         */
        RequestCommandHandler handler;

    };

    class Router {

        private:
            /**
             * @brief Nested routers
             */
            map<std::string, shared_ptr<Router>> nested;

            /**
             * @brief Commands stored in this route
             */
            map<std::string, shared_ptr<RouterCommand>> commands;

            /**
             * @brief This route's ID
             */
            string id;

        public:

            /**
             * @brief This route's description
             */
            const string description;

            /**
             * @brief Constructor
             * @param id ID for this route
             * @param description Short description for this route
             */
            Router (string id, string description);

            /**
             * @brief Destructor.
             */
            virtual ~Router ();

            /**
             * @brief Handles an incoming request
             * @param req The request to handle
             */
            void route (Request& req);

            /**
             * @brief Creates a nested router
             * @return The nested router
             */
            shared_ptr<Router> use (std::string id, std::string description);

            /**
             * @brief Adds a command to this router
             * @param id The ID (name) of the command
             * @param description The description of the command
             * @param handler The handler function for the command
             * @return The command (to append arguments to)
             */
            shared_ptr<RouterCommand> declare (string id, string description, RequestCommandHandler handler);

            /**
             * @brief Creates this route's help message
             * @return The help message
             */
            HelpMessage help ();

            /**
             * @brief Adds a suffix to this route's name
             * @param suffix 
             */
            void addSuffix(std::string suffix);

    };
    
}