#pragma once

#include <string>
#include <vector>
#include <queue>
#include <nlohmann/json.hpp>
#include <pthread.h>

#include <thread>
#include <semaphore>

#include "packet.hpp"
#include "request.hpp"
#include "route.hpp"

#include "../generic/logLevel.hpp"
#include "../generic/config.hpp"
#include "../generic/timedLoop.hpp"
#include "udpsocket.hpp"

using namespace std;


namespace Core::Cockpit {

    class Cockpit : public Sockets::UDPSocket, public Generic::TimedLoop {

        private:
            /**
             * @brief Queue of input packets to be processed
             */
            queue<Packet> input;
            /**
             * @brief Queue of output packets to be processed
             */
            queue<Packet> output;
                                    
            /**
             * @brief Reads data from the UDP port and decodes it
             */
            std::thread inputThread;

            /**
             * @brief Processes data that has been read, writes pending packets to
             * the clients
             */
            std::thread IOThread;

            /**
             * @brief Map of the known clients
             */
            std::map<std::string, Sockets::UDPClientAddr> clients;

            /**
             * @brief Flag to control the threads
             */
            bool running = true;

            /**
             * @brief Semaphore to sync the threads
             */
            std::binary_semaphore lock{ 1 };

            /**
             * @brief Packet handlers for types other than "request"
             */
            map<std::string, PacketHandler> packetHandlers;
            
        public:
            /**
             * @brief Function used to log
             */
            Generic::LogHandler logHandler = nullptr;

            /**
             * @brief Main Cockpit route
             */
            Router main;

            /**
             * @brief Constructs Cockpit. Opens the socket
             */
            Cockpit (Generic::Config& config);

            /**
             * @brief Destroys cockpit. Closes the UDP socket
             */
            ~Cockpit ();


            /**
             * @brief Code for the input thread
             */
            void readInput();

            /**
             * @brief Code for the IOThread
             */
            bool loop();

            /**
             * @brief Logs something 
             */
            void log(std::string message, Generic::LogLevel = Generic::LOG);

            /**
             * @brief Look for the ID of a UDP Client
             * @param The UDP Client
             * @return The ID if known, else "unknown"
             */
            std::string findClient(Sockets::UDPClientAddr);

            /**
             * @brief Sends an announce packet to the client
             * @param client The client to send the packet to
             */
            void announce(Sockets::UDPClientAddr client);

            /**
             * @brief Sends a packet to the designated client. If no client is
             * specified (`client == ""`), the packet is sent to all known clients
             * @param data Data to send
             */
            void sendPacket(Packet& data);

            /**
             * @brief Sends a string to a client
             * @param client The client to send the string to
             * @param message The message to send
             */
            void sendString(Sockets::UDPClientAddr client, std::string message);

            /**
             * @brief Creates a frame to send on the UDP socket
             * @param op_code The code to prefix the packet
             * @param message The message to wrap
             * @return The frame to send
             */
            vector<uint8_t> makeFrame(uint8_t op_code, std::string message);

            /**
             * @brief Writes a frame to all clients
             * @param data Frame to send
             */
            void writeToAll(vector<uint8_t> &data);

            /**
             * @brief Sends a packet on the UDP socket
             * @param type Type of the packet
             * @param payload Payload of the packet
             * @param client 
             * @param uid 
             */
            void send(std::string type, std::string payload, std::string client = "", std::string uid= "");

            /**
             * @return Is Cockpit running
             */
            bool isRunning();

            /**
             * @brief Sets a client's address based on its IPv4 and port number
             * @param id The client's id
             * @param ipv4 It's IPv4
             * @param port It's port number
             */
            void setClient(std::string id, std::string ipv4, uint16_t port);

            /**
             * @brief Sets a packet handler for a peculiar type
             * @param type The type to handle
             * @param handler The handler function
             */
            void setPacketHandler(std::string type, PacketHandler handler);

            /**
             * @brief Removes a packet handler based on its handled type
             * @param type The type of the handler to remove
             */
            void rmPacketHandler(std::string type);

    };
    



}