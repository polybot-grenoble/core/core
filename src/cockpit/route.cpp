#include "route.hpp"

#include <iostream>

namespace Core::Cockpit {

    /*
    88  88 888888 88     88""Yb 
    88  88 88__   88     88__dP 
    888888 88""   88  .o 88"""  
    88  88 888888 88ood8 88     
    */

    nlohmann::json HelpArg::json () {

        nlohmann::json j;

        j["required"] = this->required;
        j["name"] = this->name;

        return j;

    }

    nlohmann::json HelpLine::json () {

        nlohmann::json j;

        j["id"] = this->id;
        j["type"] = this->type == CommandRoute ? "cmd" : "route";
        j["description"] = this->description;
    
        vector<nlohmann::json> json_args;
        transform(
            this->args.begin(), this->args.end(), 
            back_inserter(json_args),
            [](HelpArg arg) { return arg.json(); }
        );

        j["args"] = json_args;

        return j;

    }

    void HelpLine::addArgument(string name, bool required) {

        HelpArg arg;
        arg.name = name;
        arg.required = required;

        this->args.push_back(arg);

        if (required) {
            // If the last arg is required, so are the previous ones
            for (auto a: this->args) {
                a.required = true;
            }
        }

    }

    int HelpLine::requiredArgsCount () {

        int count = 0;
        auto size = this->args.size();
        for (size_t i = 0; i < size; i++) {
            count += args[i].required ? 1 : 0;
        }
        
        return count;

    }

    nlohmann::json HelpMessage::json () {

        nlohmann::json j;

        j["title"] = this->title;
        j["description"] = this->description;
        
        vector<nlohmann::json> json_lines;
        transform(
            this->lines.begin(), this->lines.end(),
            back_inserter(json_lines),
            [](HelpLine ln) { return ln.json(); }
        );
        j["lines"] = json_lines;

        return j;

    }
  
    /*
    88""Yb  dP"Yb  88   88 888888 888888 
    88__dP dP   Yb 88   88   88   88__   
    88"Yb  Yb   dP Y8   8P   88   88""   
    88  Yb  YbodP  `YbodP'   88   888888 
    */

    Router::Router (string id, string description) : 
        id(id), description(description) {

        // Add the help command to this route
        declare("help", "Shows the help message", [&](Request &req) {
            req.respond(help().json().dump(), "help");
        });

    }

    Router::~Router () {

    }

    void Router::route (Request& req) {

        // Checks if there is any args left
        if (!req.argsLeft()) {
            // We know it exists thanks to the constructor ^^
            return commands["help"]->handler(req);
        }
        
        string id = req.id();

        // Check if any command matches
        for (auto& [cmd_id, command] : commands) {

            if (cmd_id != id) {
                continue;
            }

            // The request has not enough arguments
            auto needed = command->declaration.requiredArgsCount();
            auto left = req.argsLeft();
            if (needed > left - 1) {
                req.tooFewArgs();
                return;
            }

            // Handling the request
            command->handler(req);
            return;

        }

        // Checks if there is a nested router that can handle the request
        for (auto& [route_id, route] : nested) {

            if (route_id != id) {
                continue;
            }

            // Passing the request to the next one
            req.dive();
            route->route(req);
            return;

        }

        // Fails
        req.notFound();
        return;

    }

    shared_ptr<Router> Router::use(std::string id, std::string description) {

        int count = 0;

        auto route = make_shared<Router>(id, description);

        for (auto& [cmd_id, cmd] : commands) {
            count += (id == cmd_id) ? 1 : 0;
        }

        for (auto& [route_id, route] : nested) {
            count += (id == route_id) ? 1 : 0;
        }

        if (count) {
            route->addSuffix(to_string(count));
        }

        nested[route->id] = route;

        return route;

    }   

    shared_ptr<RouterCommand> Router::declare (string id, string description, RequestCommandHandler handler) {

        int count = 0;

        for (auto& [cmd_id, cmd] : commands) {
            count += (id == cmd_id) ? 1 : 0;
        }

        for (auto& [route_id, route] : nested) {
            count += (id == route_id) ? 1 : 0;
        }

        if (count) {
            id += "_" + to_string(count);
        }

        auto command = make_shared<RouterCommand>();

        command->handler = handler;
        command->declaration.id = id;
        command->declaration.description = description;
        command->declaration.type = CommandRoute;

        commands[id] = command;

        return command;

    }
    
    HelpMessage Router::help () {

        HelpMessage m;
        HelpLine l;

        // Filling base infos
        m.title = id;
        m.description = description;

        // Adding lines for commands
        for (auto& [cmd_id, cmd] : commands) {
            m.lines.push_back(cmd->declaration);
        }

        // Adding lines for nested routers
        l.type = NestedRoute;
        for (auto& [route_id, route] : nested) {
            l.id = route_id;
            l.description = route->description;
            m.lines.push_back(l);
        }

        return m;

    }

    void Router::addSuffix(string suffix) {
        id += "_" + suffix;
    }

}