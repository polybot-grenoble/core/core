#include "commandParser.hpp"

#include <vector>
#include <string>

using namespace std;

namespace Core::Cockpit {

    bool isEmptySpaceChar (char c) {
        return c == ' ' 
            || c == '\r' 
            || c == '\n'
            || c == '\t';
    }

    vector<string> parseCommand (string cmd) {

        // The output
        vector<string> out;

        // Flags
        bool backslash = false;
        bool inQuote = false;
        bool inBit = false;

        // Last opened quote type
        char quoteType = 0;

        // The buffer
        string bit = "";
        
        for (size_t i = 0; i < cmd.length(); i++) {

            // Get the next char
            char c = cmd[i];

            // Checks if we have to escape the next 
            if (!backslash) {

                // If we have a \, we escape the next char
                if (c == '\\') {
                    backslash = true;
                    continue;
                } 
                
                // If we have a quote, we escape until the next similar quote
                else if (!(inBit || inQuote) && (c == '\'' || c == '"')) {
                    inQuote = true;
                    quoteType = c;
                    continue;
                }
                    
                // If we have a whitespace char, we push the current buffer into the output list
                else if (!inQuote && isEmptySpaceChar(c)) {
                    if (inBit) {
                        inBit = false;
                        out.push_back(bit);
                        bit.clear();
                    }
                    continue;
                }

                // Else we are in a word
                else if (!inQuote) {
                    inBit = true;
                }

            }

            if (!backslash && inQuote && quoteType == c) {
                // We found an unescaped similar quote while beeing inside of a string
                // We push the buffer in the output and go on to the next char
                inQuote = false;
                out.push_back(bit);
                bit.clear();
                continue;
            }

            if (backslash) {
                // We escaped the char, lower the flag
                backslash = false;
            }

            // We add the current chat in the buffer
            bit += c;
            
        }

        // If the buffer is not empty, we push it into the output
        if (!bit.empty()) {
            out.push_back(bit);
        }
        
        return out;

    }

}