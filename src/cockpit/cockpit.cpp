#include "cockpit.hpp"

#include <iostream>

namespace Core::Cockpit {
    
    /*
     dP""b8  dP"Yb   dP""b8 88  dP 88""Yb 88 888888 
    dP   `" dP   Yb dP   `" 88odP  88__dP 88   88   
    Yb      Yb   dP Yb      88"Yb  88"""  88   88   
     YboodP  YbodP   YboodP 88  Yb 88     88   88   
    */

    Cockpit::Cockpit(Generic::Config& config):
        main("Core", "The robot's brain") {

        open(config.cockpit.port);

        setClient("hmi", config.cockpit.hmiAddress, config.cockpit.hmiPort);

        main.declare("version", "Shows Core's version", [](Request req) {
            req.respond("1.0 - Test flight", "version");
        });

        inputThread = std::thread(&Cockpit::readInput, this);
        IOThread = std::thread(&Cockpit::run, this);

    }


    Cockpit::~Cockpit () {
        
        log("Stopping Cockpit");
        
        // Waiting for output to be emptied
        while (output.size()) {
            // Wait
        }

        // Stopping threads
        running = false;

        // Ending the socket
        end();

        // Waiting for Writer thread to stop
        if (IOThread.joinable()) {
            IOThread.join();
        }

        // Waiting for Reading thread to stop
        if (inputThread.joinable()) {
            inputThread.join();
        }

    }


    void Cockpit::readInput() {

        Sockets::UDPClientAddr addr;
        vector<uint8_t> data;

        Packet packet;

        while (running) {

            auto code = readData(addr, data);

            if (code) {
                log("Error while reading UDP (" + std::to_string(code) + "), stopping", Generic::ERROR);
                running = false;
                break;
            }

            if (data.size() < (sizeof(uint8_t) + sizeof(uint32_t))) {
                log("Received invalid UDP packet (data length < header)", Generic::DEBUG);
                continue;
            }

            // See cockpit udp data format for more details.
            auto data_size = &(data[1]);
            auto data_payload = &(data[5]);

            // Getting the type of packet
            uint8_t op_code = data[0];

            // Getting the size of the payload
            uint32_t size;
            std::copy(data_size, data_payload, &size);

            // Getting the payload
            auto data_end = data_payload + size;
            char* raw_payload = new char[size];
            std::copy(data_payload, data_end, raw_payload);
            std::string payload(raw_payload);
            delete [] raw_payload;

            switch (op_code)
            {
            case 0: {
                // Announce
                lock.acquire();
                clients[payload] = addr;
                lock.release();

                announce(addr);
                log("Got announce from " + payload, Generic::DEBUG);
                break;
            }

            case 1: {
                // JSON Data
                json j;

                try {
                    j = json::parse(payload);
                }
                catch (const std::exception& e) {
                    log(e.what(), Generic::ERROR);
                    break;
                }

                if (!packet.from(j)) {
                    log("Invalid Packet : " + j.dump(), Generic::ERROR);
                    break;
                }

                lock.acquire();
                input.push(packet);
                lock.release();

                break;
            }

            case 2: {
                // Raw request
                packet.uid = "";
                packet.client = findClient(addr);
                packet.type = "request";
                packet.payload = payload;

                lock.acquire();
                input.push(packet);
                lock.release();

                break;
            }

            default:
                log("Unknown UDP op_code " + std::to_string(op_code), Generic::DEBUG);
                break;
            }

        }

        log("Stopping input thread");

    }


    bool Cockpit::loop() {

        // Read incoming
        lock.acquire();
        auto n = input.size();
        lock.release();

        for (; n > 0; n--) {

            lock.acquire();
            auto p = input.front();
            input.pop();
            lock.release();

            // If request -> Router
            if (p.type == "request") {

                Request req(p);

                req.responseTunnel = [&](Packet& p) {
                    //log("Responding to request from " + p.client, Generic::DEBUG);
                    sendPacket(p);
                };

                //log("Received request from " + p.client, Generic::DEBUG);
                main.route(req);

                continue;
            }

            // If handled -> PacketHandler
            auto el = packetHandlers.find(p.type);
            if (el != packetHandlers.end()) {
                el->second(p);
                continue;
            }

            // Else print error
            log("No handler for packet of type : " + p.type, Generic::ERROR);

        }

        // Write outgoing
        lock.acquire();
        n = output.size();
        lock.release();

        for (; n > 0; n--) {
            
            lock.acquire();
            auto p = output.front();
            output.pop();
            lock.release();

            sendPacket(p);
        }

        // Wait next run
        return running;

    }


    void Cockpit::log(std::string message, Generic::LogLevel level) {

        if (logHandler) {
            logHandler("[Cockpit] " + message, level);
        }

    }


    std::string Cockpit::findClient(Sockets::UDPClientAddr addr) {

        for (auto el = clients.begin(); el != clients.end(); el++) {

            if (
                   el->second.addr.s_addr == addr.addr.s_addr 
                && el->second.port == addr.port
                ) {
                return el->first;
            }

        }

        return "unknown";

    }


    void Cockpit::announce(Sockets::UDPClientAddr client) {

        auto data = makeFrame(0, "core");

        writeData(client, data);

    }


    void Cockpit::sendPacket(Packet& data) {

        auto frame = makeFrame(1, data.raw().dump());

        if (data.client == "") {
            writeToAll(frame);
            return;
        }

        auto c = clients.find(data.client);
        if (c == clients.end()) {
            writeToAll(frame);
            return;
        }

        writeData(c->second, frame);

    }

    
    void Cockpit::sendString(Sockets::UDPClientAddr client, std::string message) {

        auto frame = makeFrame(2, message);
        writeData(client, frame);

    }


    vector<uint8_t> Cockpit::makeFrame(uint8_t op_code, std::string message) {
           
        vector<uint8_t> frame;

        frame.push_back(op_code);
        uint32_t size = message.length() + 1;
        const char* str = message.c_str();

        frame.resize(1 + sizeof(uint32_t) + size);
        auto data_size = frame.begin() + 1;
        auto data_payload = data_size + sizeof(uint32_t);

        std::copy((uint8_t*) &size, ((uint8_t*) &size) + sizeof(uint32_t), data_size);
        std::copy(str, str + size, data_payload);

        return frame;

    }

    void Cockpit::writeToAll(vector<uint8_t> &data) {

        lock.acquire();
        for (auto& [key, value] : clients) {
            writeData(value, data);
        }
        lock.release();

    }

    void Cockpit::send(std::string type, std::string payload, std::string client, std::string uid) {

        Packet p;

        p.client = client;
        p.payload = payload;
        p.type = type;
        p.uid = uid;

        lock.acquire();
        output.push(p);
        lock.release();

    }

    bool Cockpit::isRunning() {
        return running;
    }

    void Cockpit::setClient(std::string id, std::string ipv4, uint16_t port) {
        
        Sockets::UDPClientAddr addr;
        inet_pton(AF_INET, ipv4.c_str(), (struct in_addr*)&addr.addr);
        addr.port = htons(port);

        lock.acquire();
        clients[id] = addr;
        lock.release();

    }

    void Cockpit::setPacketHandler(std::string type, PacketHandler handler) {

        lock.acquire();
        packetHandlers[type] = handler;
        lock.release();

    }

    void Cockpit::rmPacketHandler(std::string type) {

        packetHandlers.erase(type);

    }

}