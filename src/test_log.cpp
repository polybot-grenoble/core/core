#include "log.hpp"
#include "logLevel.hpp"
using namespace Core::Generic;

int main(){
    Log logs("../build/logs");
    logs.writeInLog("Test de log", LOG);
    logs.writeInLog("Test de error", ERROR);
    logs.writeInLog("Test de debug", DEBUG);
}