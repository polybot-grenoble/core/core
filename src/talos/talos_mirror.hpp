#pragma once
#include <queue>
#include <string>
using namespace std;
namespace Core::Talos::Mirror {
    /**
    * @brief List of Talos' states
    */
    enum states {
        INIT = 0,
        DISCONNECTED = 1,
        RUNNING = 2,
        ERRORED = -1,
    };

    /**
     * @brief List of commands sent by Hermès
     */
    enum commands {
        doNothing = 0,
        reset = 99,
        initOK = 1,
        CANConnected = 2,
        CANDisconnected = 3,
        error = -1
    };

    /**
    * @brief Structure of a typical transition
    */
    struct transitions{
        states stateComing;
        commands transitCommand;
        states stateArriving;
    };

    /**
     * @brief A structure to inform the raspberry pi
     * about the current Talos' way
     */
    struct infos{
        states previousState;
        states currentState;
    };

    class Mirror {
        private :
            states currentState;
            commands command;
            int n;
            transitions* tab;

        public :
            queue<infos> FiFo;

            /**
             * @brief Construct a new Talos Mirror object
             */
            Mirror();

            /**
             * @brief Function which push the current and previous state
             * in the FiFo
             * @param previousState The previous state of the machine
             */
            void pushInFiFo(states previousState);

            /**
            * @brief Function which change the state of Talos
            * depending on the command
            */
            void nextState ();

            /**
             * @brief Function which permit to verify if there are transitions
             * in the queue
             * @return true if the queue is empty, false if not
             */
            bool isQueueEmpty();

            /**
             * @brief Function which returns the first info of the queue and 
             * removes it from the queue
             * 
             * @return the first object of the FIFO
             */
            infos takeFirstInfos();

            /**
             * @brief Function which changes the command of the state machine 
             * and executes it
             * 
             * @param newCommand The command to execute in the state machine
             */
            void changeCommand(commands newCommand);

            /**
             * @brief Function which returns the current state of talos_mirror
             * @return the current state of the machine
             */
            states getState();

            /**
             * @brief Function to overwrite the state of talos_mirror
             * (in emergency case). It will also send this change in the FiFo
             * @param newState The state which will overwrite the current state
             */
            void setState(states newState);

            /**
             * @brief Function which returns the current state in text form
             * @return a string containing the current state name
             */
            string getStateText();
    };
}
