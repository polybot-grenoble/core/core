#include "talos_mirror.hpp"

namespace Core::Talos::Mirror {

    transitions tabTransit[9] = {
        {INIT, initOK, DISCONNECTED},
        {DISCONNECTED, CANConnected, RUNNING},
        {RUNNING, CANDisconnected, DISCONNECTED},

        {DISCONNECTED, reset, INIT},
        {RUNNING, reset, INIT},
        {ERRORED, reset, INIT},

        {RUNNING, error, ERRORED},
        {INIT, error, ERRORED},
        {DISCONNECTED, error, ERRORED},
    };

    Mirror::Mirror() {
        command = doNothing;
        currentState = INIT;
        n = 9;
        tab = tabTransit;
    }

    void Mirror::pushInFiFo(states previousState){
        infos current_infos;
        current_infos.previousState = previousState;
        current_infos.currentState = currentState;
        FiFo.push(current_infos);
    }

    void Mirror::nextState(){
        states tempState = currentState;
        for(int i = 0 ; i < n ; i++){
            if((currentState == tab[i].stateComing)
            && (command == tab[i].transitCommand)){
                currentState = tab[i].stateArriving;
                break;
            }
        }
        command = doNothing;
        if (currentState == INIT) if(command == doNothing) command = initOK;
        if(tempState != currentState) pushInFiFo(tempState);
    }

    bool Mirror::isQueueEmpty(){
        return FiFo.empty();
    }

    infos Mirror::takeFirstInfos(){
        infos infos_to_return = FiFo.front();
        FiFo.pop();
        return infos_to_return;
    }

    void Mirror::changeCommand(commands newCommand){
        command = newCommand;
        nextState();
    }

    states Mirror::getState(){
        return currentState;
    }

    void Mirror::setState(states newState){
        currentState = newState;
        pushInFiFo(newState);
    }

    string Mirror::getStateText(){
        switch(currentState){
            case INIT :
                return "INIT";
            case DISCONNECTED :
                return "DISCONNECTED";
            case RUNNING :
                return "RUNNING";
            case ERRORED :
                return "ERRORED";
            default :
                return "UNKNOWN";
        }
    }
}