#include "gigaTalos.hpp"
using namespace std;

namespace Core::Talos::gigaTalos{

    gigaTalos::gigaTalos() {
        currentState = INIT;
        command = do_nothing;
        tabTransit.push_back({INIT, init_err, INIT_FAIL});
        tabTransit.push_back({INIT, init_ok, IDLE});

        tabTransit.push_back({INIT_FAIL, init_retry, INIT});
        tabTransit.push_back({INIT_FAIL, quit, END_CORE});

        tabTransit.push_back({IDLE, rc_start, REMOTE_CONTROL});
        tabTransit.push_back({IDLE, quit, END_CORE});
        tabTransit.push_back({IDLE, strat_start, STRAT_CHANGE});

        tabTransit.push_back({REMOTE_CONTROL, rc_stop, IDLE});

        tabTransit.push_back({STRAT_CHANGE, strat_not_found, STRAT_NONE_LEFT});
        tabTransit.push_back({STRAT_CHANGE, strat_found, STRAT_CHANGE_BIS});
        
        tabTransit.push_back({STRAT_NONE_LEFT, reset, IDLE});
        tabTransit.push_back({STRAT_NONE_LEFT, quit, END_CORE});

        tabTransit.push_back({STRAT_CHANGE_BIS, clear, WAITING_TIRETTE_INSERT});
        tabTransit.push_back({
            STRAT_CHANGE_BIS, strat_skip_setup,
            STRAT_SKIP_SETUP
        });
        
        tabTransit.push_back({WAITING_TIRETTE_INSERT, tirette_on, STRAT_SETUP});

        tabTransit.push_back({
            STRAT_SETUP, strat_setup_ok,
            WAITING_TIRETTE_PULL
        });

        tabTransit.push_back({STRAT_SKIP_SETUP, strat_skipped, RUNNING});

        tabTransit.push_back({WAITING_TIRETTE_PULL, tirette_off, RUNNING});
        
        tabTransit.push_back({
            RUNNING, strat_stop_exec_error,
            STRAT_STOP_EXEC_ERROR
        });
        tabTransit.push_back({RUNNING, strat_done_timeout, WAITING_MATCH_END});
        tabTransit.push_back({RUNNING, timeout, DONE});
        tabTransit.push_back({RUNNING, peripheral_loss, PERIPHERAL_LOSS});

        tabTransit.push_back({
            STRAT_STOP_EXEC_ERROR, strat_exec_error,
            STRAT_EXEC_ERROR
        });
        
        tabTransit.push_back({STRAT_EXEC_ERROR, quit, END_CORE});
        tabTransit.push_back({STRAT_EXEC_ERROR, reset, IDLE});

        tabTransit.push_back({WAITING_MATCH_END, timeout, DONE});

        tabTransit.push_back({DONE, reset, IDLE});
        tabTransit.push_back({DONE, quit, END_CORE});

        tabTransit.push_back({PERIPHERAL_LOSS, peripheral_unused, RUNNING});
        tabTransit.push_back({
            PERIPHERAL_LOSS, peripheral_required,
            STRAT_STOP_PERIPHERAL
        });

        tabTransit.push_back({
            STRAT_STOP_PERIPHERAL, strat_restart,
            STRAT_CHANGE
        }); 
    }

    gigaTalos::~gigaTalos(){}

    void gigaTalos::exec(){
        nextState();
        switch (currentState){
            case INIT :
                init();
                break;
            case INIT_FAIL:
                initFail();
                break;
            case REMOTE_CONTROL:
                remoteControl();
                break;
            case IDLE:
                idle();
                break;
            case STRAT_EXEC_ERROR:
                stratExecError();
                break;
            case STRAT_STOP_EXEC_ERROR:
                stratStopExecError();
                break;
            case STRAT_CHANGE:
                stratChange();
                break;
            case STRAT_CHANGE_BIS:
                stratChangeBis();
                break;
            case WAITING_TIRETTE_INSERT:
                waitingTiretteInsert();
                break;
            case STRAT_SETUP:
                stratSetup();
                break;
            case WAITING_TIRETTE_PULL:
                waitingTirettePull();
                break;
            case RUNNING:
                running();
                break;
            case WAITING_MATCH_END:
                waitingMatchEnd();
                break;
            case DONE:
                done();
                break;
            case STRAT_SKIP_SETUP:
                stratSkipSetup();
                break;
            case STRAT_NONE_LEFT:
                stratNoneLeft();
                break;
            case STRAT_STOP_PERIPHERAL:
                stratStopPeripheral();
                break;
            case PERIPHERAL_LOSS:
                peripheralLoss();
                break;
            case END_CORE:
                endCore();
                break;
            default:
                break;
        }
    }

    void gigaTalos::nextState(){
        perviousState = currentState;

        for (auto &transition : tabTransit) {
            if((currentState == transition.stateComing)
            && (command == transition.transitCommand)){
                currentState = transition.stateArriving;
                break;
            }
        }
        command = do_nothing;
    }

    void gigaTalos::changeCommand(commands newCommand){
        command = newCommand;
    }

    commands gigaTalos::getCurrentCommand(){
        return command;
    }

    string gigaTalos::printCurrentState(){
        switch (currentState){
            case INIT :
                return "INIT";
                break;
            case INIT_FAIL:
                return "INIT_FAIL";
                break;
            case REMOTE_CONTROL:
                return "REMOTE_CONTROL";
                break;
            case IDLE:
                return "IDLE";
                break;
            case STRAT_EXEC_ERROR:
                return "STRAT_EXEC_ERROR";
                break;
            case STRAT_STOP_EXEC_ERROR:
                return "STRAT_STOP_EXEC_ERROR";
                break;
            case STRAT_CHANGE:
                return "STRAT_CHANGE";
                break;
            case STRAT_CHANGE_BIS:
                return "STRAT_CHANGE_BIS";
                break;
            case WAITING_TIRETTE_INSERT:
                return "WAITING_TIRETTE_INSERT";
                break;
            case STRAT_SETUP:
                return "STRAT_SETUP";
                break;
            case WAITING_TIRETTE_PULL:
                return "WAITING_TIRETTE_PULL";
                break;
            case RUNNING:
                return "RUNNING";
                break;
            case WAITING_MATCH_END:
                return "WAITING_MATCH_END";
                break;
            case DONE:
                return "DONE";
                break;
            case STRAT_SKIP_SETUP:
                return "STRAT_SKIP_SETUP";
                break;
            case STRAT_NONE_LEFT:
                return "STRAT_NONE_LEFT";
                break;
            case STRAT_STOP_PERIPHERAL:
                return "STRAT_STOP_PERIPHERAL";
                break;
            case PERIPHERAL_LOSS:
                return "PERIPHERAL_LOSS";
                break;
            case END_CORE:
                return "END_CORE";
                break;
            default:
                return "Erreur etat non reconnu\n";
                break;
        }
    }

    bool gigaTalos::firstTime() {
        return perviousState != currentState;
    }
}