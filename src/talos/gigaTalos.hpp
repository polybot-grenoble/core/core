#pragma once
#include <vector>
#include <iostream>


/**
 * GigaTalos is the biggest state's machine, implemented in the Raspberry
 * PI to execute the strategy
*/

namespace Core::Talos::gigaTalos {
using namespace std;
    /**
    * @brief List of Talos' states
    */
    enum states{
        INIT,
        INIT_FAIL,
        REMOTE_CONTROL,
        IDLE,
        STRAT_EXEC_ERROR,
        STRAT_STOP_EXEC_ERROR,
        STRAT_CHANGE,
        STRAT_CHANGE_BIS,
        WAITING_TIRETTE_INSERT,
        STRAT_SETUP,
        WAITING_TIRETTE_PULL,
        RUNNING,
        WAITING_MATCH_END,
        DONE,
        STRAT_SKIP_SETUP,
        STRAT_NONE_LEFT,
        STRAT_STOP_PERIPHERAL,
        PERIPHERAL_LOSS,
        END_CORE
    };

    /**
     * @brief List of commands for the Raspberry PI
     */
    enum commands{
        init_retry,
        init_err,
        quit,
        init_ok,
        rc_stop,
        rc_start,
        reset,
        strat_start,
        strat_restart,
        strat_exec_error,
        strat_stop_exec_error,
        strat_found,
        clear,
        tirette_on,
        strat_setup_ok,
        tirette_off,
        strat_done_timeout,
        timeout,
        strat_skip_setup,
        strat_skipped,
        strat_not_found,
        peripheral_required,
        peripheral_unused,
        peripheral_loss,
        do_nothing
    };

    /**
    * @brief Structure of a typical transition
    */
    struct transitions{
        states stateComing;
        commands transitCommand;
        states stateArriving;
    };

    /**
     * @brief class which define a state machine to control the strategies of
     * the robot
     */
    class gigaTalos{
        private :
            /**
             * @brief The current state of gigaTalos
             */
            states currentState;

            /**
             * @brief Previous talos state
             */
            states perviousState = END_CORE;

            /**
             * @brief The last command executed on gigaTalos
             */
            commands command;
            
            /**
            * @brief Function which change the state of gigaTalos
            * depending on the command
            */
            void nextState();

        protected :
            /**
             * @brief The table with all the states and their corresponding
             * transitions
             */
            vector<transitions> tabTransit;
            
        public :
        /**
         * Constructors and destructors of the object gigaTalos
         */
        gigaTalos();

        virtual ~gigaTalos();

        /**
        * @brief Function which launches the state's machines
        */
        void exec();

        /**
         * @brief Wrapper to change the command of gigaTalos
         * @param newCommand The new command
         * which will change the gigaTalos' state
         */
        void changeCommand(commands newCommand);

        /**
         * @brief Getter which returns the current command
         * @return the current command
         */
        commands getCurrentCommand();

        /**
         * @brief Function which prints the current state
        */
        string printCurrentState();

        /**
         * @return Is the current execution the fist time
         * since the last state change ? 
         */
        bool firstTime();

        /**
         * @brief The functions that will be called by gigaTalos
         * when reaching a state
         * They are virtual to be rewrotten by user
         */
        virtual void initFail() = 0;
        virtual void init() = 0;
        virtual void endCore() = 0;
        virtual void idle() = 0;
        virtual void remoteControl() = 0;
        virtual void stratChange() = 0;
        virtual void stratChangeBis() = 0;
        virtual void stratNoneLeft() = 0;
        virtual void waitingTiretteInsert() = 0;
        virtual void stratSetup() = 0;
        virtual void waitingTirettePull() = 0;
        virtual void running() = 0;
        virtual void stratStopExecError() = 0;
        virtual void stratExecError() = 0;
        virtual void waitingMatchEnd() = 0;
        virtual void done() = 0;
        virtual void stratSkipSetup() = 0;
        virtual void peripheralLoss() = 0;
        virtual void stratStopPeripheral() = 0;
    };
}