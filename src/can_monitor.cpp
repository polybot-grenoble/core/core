/**
 * @file can_monitor.cpp
 * @author Julien Pierson
 * @brief This programs displays the traffic on a CAN bus which uses Hermes
 * @version 0.1
 * @date 2023-12-09
 */

#include <stdio.h>
#include <socketcan.hpp>
#include <stdint.h>
#include <filesystem>
#include <fstream>
#include <ctime>
#include <iostream>
#include <chrono>

extern "C" {
    #include <Hermes.h>
}

using namespace std::chrono;

/**
 * @brief Gets the .csv file name to store the monitored traffic
 * @param coreFolder The .core folder path
 * @return The file stream
 */
std::string get_csv(std::string coreFolder);

/**
 * @brief Initializes the csv file
 * @param out The output CSV file
 */
void init_csv(std::ofstream& out);

void write_csv(
    std::ofstream& out, 
    uint8_t from, uint8_t to,
    uint16_t command, bool isResponse, 
    uint32_t data_len, uint8_t data[8]
);

int main (int argc, char **argv) {

    
    if (argc < 2) {
        printf(
            "Usage: %s <can_interface>\n\rUse 'ip a' to find one\n\r",
            argv[0]
        );
        return 1;
    }

    /** Finding .core folder location */
    auto home = std::getenv("HOME");
    auto coreFolderPath = strlen(home)
        ? std::string(home) + "/.core/"
        : "/home/.core/";

    Core::Sockets::SocketCAN can;
    Core::Sockets::SocketReturnCode code;

    code = can.open(argv[1]);
    if (code) {
        printf("CAN OPEN ERROR: %d\n\r", code);
        return code;
    }

    auto name = get_csv(coreFolderPath + "csv/");
    if (name == "") {
        return -1;
    }
    std::ofstream record (name);
    init_csv(record);
    
    printf("\x1b[s\x1b[H\x1b[2J\r");
    printf("FROM | TO   | COMMAND | TYPE | DATA               | ASCII   \n\r");
    printf("-----|------|---------|------|--------------------|---------\n\r");

    Hermes_t hermes;
    Hermes_init(&hermes, 12, 8);

    uint32_t raw_id, data_len;
    uint8_t data[8];

    uint8_t reciever;
    uint8_t sender;
    uint16_t command;
    bool isResponse;

    for(;;) {

        code = can.readData(&raw_id, data, &data_len);
        
        if (code) {
            printf("CAN READ ERROR: %d\n\r", code);
            return code;
        }
        
        Hermes_parseCANFrameID(
            raw_id, &reciever, &command, &sender, &isResponse
        );

        printf(
            "0x%02x | 0x%02x |   0x%03x |  %s | ",
            sender, reciever, command,
            isResponse ? "RES" : "REQ"            
        );

        printf("0x");

        for (uint32_t i = 0; i < data_len; i++) {
            printf("%02x", data[i]);
        } 

        for (uint32_t i = 16; i > data_len * 2; i--) {
            printf(" ");
        }

        printf(" | ");
        
        for (uint32_t i = 0; i < data_len; i++) {
            if (data[i] >= 32 && data[i] < 127) {
                printf("%c", data[i]);
            } else {
                printf("∙");
            }
        }

        printf("\n\r");


        write_csv(record, sender, reciever, command, isResponse, data_len, data);

        /*printf("\x1b[s\x1b[H\x1b[2K\r");
        printf("FROM | TO   | COMMAND | TYPE | DATA               | ASCII   \n\r");
        printf("-----|------|---------|------|--------------------|---------\n\r");
        printf("\x1b[u");*/

    }



}

std::string get_csv(std::string coreFolder) {

    time_t t = time(nullptr);
    tm* now = localtime(&t);
    char buffer[128];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %X", now);
    
    std::string fileName(buffer);
    fileName = coreFolder + fileName + ".csv";
    
    std::filesystem::path path(fileName);

    if (!std::filesystem::exists(path.parent_path())) {
        // Tries to create the folder tree
        try {
            if (!std::filesystem::create_directories(path.parent_path())) {
                return "";
            }
        }
        catch (std::filesystem::filesystem_error& e) {
            std::cout << e.what() << std::endl;
            return "";
        }
    }

    return fileName;

}

void init_csv(std::ofstream& out) {

    out << "time,from,to,command,isResponse,data,ascii" << std::endl;

}

void write_csv(
    std::ofstream& out,
    uint8_t from, uint8_t to,
    uint16_t command, bool isResponse,
    uint32_t data_len, uint8_t data[8]
) {

    char tmp[3];
    char hexData[19];
    char asciiData[9];
        
    sprintf(hexData, "0x");
    for (uint32_t i = 0; i < data_len; i++) {
        sprintf(tmp, "%02x", data[i]);
        strcat(hexData, tmp);
        if (data[i] >= 32 && data[i] < 127 && data[i] != ',') {
            asciiData[i] = data[i];
        }
        else {
            asciiData[i] = '.';
        }
    }

    hexData[18] = 0;
    asciiData[data_len] = 0;

    // Timestamp
    auto time = duration_cast<milliseconds>(
        system_clock::now().time_since_epoch()
    );

    out << time.count() << "," << (int)from << "," << (int)to << ","
        << command << "," << isResponse << "," 
        << hexData << "," << asciiData << std::endl;

}