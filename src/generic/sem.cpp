#include "sem.hpp"

using namespace std;

namespace Core::Generic {

    /*
    .dP"Y8 888888 8b    d8    db    88""Yb 88  88  dP"Yb  88""Yb 888888 
    `Ybo." 88__   88b  d88   dPYb   88__dP 88  88 dP   Yb 88__dP 88__   
    o.`Y8b 88""   88YbdP88  dP__Yb  88"""  888888 Yb   dP 88"Yb  88""   
    8bodP' 888888 88 YY 88 dP""""Yb 88     88  88  YbodP  88  Yb 888888 
    */

    Semaphore::Semaphore (int n) {

        // Creating the semaphore
        this->_initNSemaphore(n, IPC_PRIVATE);

    }

    Semaphore::Semaphore(int n, string filePath) {

        // Creating the key
        key_t key = ftok(filePath.c_str(), 0);

        // Creating the semaphore
        this->_initNSemaphore(n, key);

    }

    void Semaphore::_initNSemaphore (int n, key_t key) {

        // Get the semaphores' id
        this->id = semget(key, n, IPC_CREAT|0666);
        if (this->id == -1) {
            throw runtime_error("Can't get semaphore");
        }

        // Inits all semaphores values to 0
        for (int i = 0; i < n; i++) {
            semctl(this->id, i, SETVAL, 0);
        }

    }

    Semaphore::~Semaphore () {

        // Removes the semaphore on class destruction
        if (semctl(this->id, 0, IPC_RMID, 0)) {
            fprintf(stderr, "Can't destroy semaphore");
        }

    }

    void Semaphore::P (int n) {

        struct sembuf sb;
        sb.sem_op = -1;
        sb.sem_flg = 0;
        sb.sem_num = n;

        if (semop(this->id, &sb, 1)) {
            throw runtime_error("Can't use semaphore (P)");
        }

    }

    void Semaphore::V (int n) {

        struct sembuf sb;
        sb.sem_op = 1;
        sb.sem_flg = 0;
        sb.sem_num = n;

        if (semop(this->id, &sb, 1)) {
            throw runtime_error("Can't use semaphore (V)");
        }

    }

}