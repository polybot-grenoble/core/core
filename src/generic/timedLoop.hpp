#pragma once

namespace Core::Generic {

    /**
     * @brief A loop that can run only a defined couple of times per second 
     */
    class TimedLoop {

        public:
            /** Maximum number of ticks in one second */
            int maxTPS = 50;
            /** Is the loop running */
            bool running;

            /** The number of ticks per second */
            double tps = 0;


        /**
         * @brief Stops the loop 
         */
        void stop ();

        /**
         * @brief Runs the loop 
         */
        void run ();

        /**
         * @brief Executes one loop and waits the necessary time before 
         * ending the tick.
         * 
         * @return true 
         * @return false 
         */
        bool step ();

        /**
         * @brief The loop. Has to be overriden to put the actual loop code. 
         *
         * @return true The loop should continue
         * @return false The loop should stop 
         */
        bool virtual loop ();

    };

}