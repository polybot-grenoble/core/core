#pragma once
#include <functional>
#include <string>

namespace Core::Generic {

	enum LogLevel {
		LOG,
		ERROR,
		DEBUG
	};

	typedef std::function<void(const std::string& message, LogLevel level)> LogHandler;

}