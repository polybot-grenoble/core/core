#pragma once
#include <mutex>
#include <chrono>
#include <thread>
#include <semaphore> // (even if @baptistemht does not like them)

namespace Core::Generic {

    /**
     * This class was made to be used by multiple threads at a time.
     * Each thread executes yield().
     * When pause() is called, the first thread to yield() pauses.
     * The thread won't starts running again until resume() is called.
     * 
     * It has been developed to allow multiple lua scripts to access the same
     * shared memory.
     * 
     * Made with <3 by Julien & Baptiste
    */

    class YieldLock {

        private:

        std::thread::id targetThread;

        /**
         * True if pause() has been called and not resume()
        */
        bool paused = false;

        /**
         * Mutex to access internal variables.
        */
        std::mutex internal_mutex;

        /**
         * @brief Semaphore to make the main thread wait
         */
        std::binary_semaphore wait{ 1 };
        
        /**
         * @brief Flag to abort execution using an exception
         */
        bool aborted = false;

        public:
        YieldLock();

        /**
         * Timeout time for the pause action
        */
        std::chrono::milliseconds timeout {1000};

        /**
         * Pauses any yielding thread
         * 
         * @returns true if paused, false if failed
        */
        bool pause ();

        /**
         * Resumes any paused thread
        */
        void resume ();

        /**
         * Locks the current thread if requested, does nothing else
        */
        void yield ();

        void becomeTarget ();

        /**
         * @brief Asks the running thread to abort
         */
        void abort();

        /**
         * @brief Clears the abort flag
         */
        void clearAbort();

    };

}