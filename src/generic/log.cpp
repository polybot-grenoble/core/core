#include "log.hpp"

namespace Core::Generic{
    Log::Log(string newFolder){
        folder = newFolder;
        logOpened = folderInit();
        folder = folder + (folder.ends_with("/") ? "" : "/");
        string adresseLog = folder + "latest.log";
        string adresseJson = folder + "latest.json";
        fichLog.open(adresseLog.c_str());
        fichJson.open(adresseJson.c_str());
        if(!fichLog || !fichJson) logOpened = false;
        logs = json::array({});
    }

    Log::~Log(){
        fichLog.close();
        fichJson.close();
        string titreFin = folder + currentDateTime() + ".log";
        string titreFinJson = folder + currentDateTime() + ".json";
        string adresseLog = folder + "latest.log";
        string adresseJson = folder + "latest.json";
        rename(adresseLog, titreFin);
        rename(adresseJson, titreFinJson);
    }

    bool Log::folderInit(){
        if(exists(folder) && !is_directory(folder)){
            cout << "The folder already exists as a file\n";
            return false;
        }
        if(!is_directory(folder)){
            bool created = create_directories(folder);
            if (!created) {
                cout << "The folder failed to be created\n";
                return false;
            }
        }
        return true;
    }

    void Log::writeInLog(string message, LogLevel level){
        writing.acquire();
        
        if(!logOpened) {
            cout << "Error during the writing\n";
            writing.release();
            return;
        }
        if(logHandler != nullptr) logHandler(message, level);
        json j;
        j["date"] = timeSinceEpochMillisec();
        j["message"] = message;
        switch (level){
            case LOG :
                fichLog << "[LOG  ] " << message << endl;
                cout << "\x1b[32m[LOG  ]\x1b[0m " << message << endl;
                j["level"] = "LOG";
                break;
            case ERROR :
                fichLog << "[ERROR] " << message << endl;
                cout << "\x1b[31m[ERROR]\x1b[0m " << message << endl;
                j["level"] = "ERROR";
                break;
            case DEBUG :
                fichLog << "[DEBUG] " << message << endl;
                cout << "\x1b[35m[DEBUG]\x1b[0m " << message << endl;
                j["level"] = "DEBUG";
                break;
            default :
                cout << "Erreur level non-reconnu\n";
                break;
        }
        logs.push_back(j);
        fichJson.seekp(0, ios::beg);
        fichJson << logs.dump(4) <<endl;
        writing.release();
    }

    string Log::currentDateTime() {
        time_t t = time(nullptr);
        tm* now = localtime(&t);
        char buffer[128];
        strftime(buffer, sizeof(buffer), "%Y-%m-%d %X", now);
        return buffer;
    }

    int Log::timeSinceEpochMillisec() {
        return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    }
}