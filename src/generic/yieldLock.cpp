#include "yieldLock.hpp"

namespace Core::Generic {

    YieldLock::YieldLock() {

    }

    bool YieldLock::pause () {
        //Lock internal variable.
        internal_mutex.lock();

        if (paused) {
            internal_mutex.unlock();
            //Do nothing
            return true;
        }
        
        internal_mutex.unlock();

        //Try to lock yield mutex in less than a second.
        auto tried_to_pause = wait.try_acquire_for(timeout);

        internal_mutex.lock();

        paused = tried_to_pause;

        internal_mutex.unlock();


        return tried_to_pause;
    }

    void YieldLock::resume () {
        
        //Lock internal variable.
        internal_mutex.lock();

        if (!paused) {
            //Do nothing
            internal_mutex.unlock();
            return;
        }

        paused = false;

        wait.release();

        //Unlock internal variables.
        internal_mutex.unlock();
    }

    void YieldLock::yield () {
        //Lock internal variable.
        internal_mutex.lock();

        if (aborted && targetThread == std::this_thread::get_id()) {
            aborted = false; // <- Reset the flag
            internal_mutex.unlock();
            throw std::runtime_error("Aborted by yieldLock");
        }

        if(!paused || targetThread != std::this_thread::get_id()) {
            //Unlock internal variables.
            internal_mutex.unlock();
            std::this_thread::yield();
            return;
        }
        
        //Unlock internal variables.
        internal_mutex.unlock();

        wait.acquire(); // <- If pause was requested, we should be blocked here

        wait.release();

        std::this_thread::yield();

    }

    void YieldLock::becomeTarget() {
        internal_mutex.lock();
        targetThread = std::this_thread::get_id();
        internal_mutex.unlock();
    }

    void YieldLock::abort() {
        internal_mutex.lock();
        aborted = true;
        internal_mutex.unlock();
    }

    void YieldLock::clearAbort() {
        internal_mutex.lock();
        aborted = false;
        internal_mutex.unlock();
    }


}