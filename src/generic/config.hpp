#pragma once
#include <string>
#include <vector>
#include <map>
#include <nlohmann/json.hpp>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>

using json = nlohmann::json;

/** -- SHOULD BE COMMENTED -- */

namespace Core::Generic {

    struct HermesUDPClient {
        /** Host IPv4 address of the client  */
        std::string address = "";
        /** Port of the client */
        uint16_t port = 0;
        /** ID of the peripheral */
        uint8_t hermesID = 0;
        
        /**
         * @brief Checks if a JSON object matches with this object
         */
        static bool isValid(json object);
        /**
         * @brief Loads this object from a JSON object
         */
        HermesUDPClient &operator=(json object);
        /**
         * @brief Dumps this object in a JSON object
         */
        json dump();
    };

    struct HermesConfig {
        /** ID of this device on the CAN bus */
        uint8_t hermesID = 254;
        /** SocketCAN interface to open */
        std::string canInterface = "vcan0";
        /** UDP Port to open */
        uint16_t UDPPort = 42069;
        /** List of Hermes-UDP clients */
        std::vector<HermesUDPClient> UDPClients;
        
        /**
         * @brief Checks if a JSON object matches with this object
         */
        static bool isValid(json object);
        /**
         * @brief Loads this object from a JSON object
         */
        HermesConfig &operator=(json object);
        /**
         * @brief Dumps this object in a JSON object
         */
        json dump();
    };

    struct LuaConfig {
        /** List of folders to add to Lua's require path */
        std::vector<std::string> libraryFolders;
        /** List of families, matching their ID to their file location */
        std::map<std::string, std::string> families;
        /** List of peripherals, matching their ID to their family */
        std::map<uint8_t, std::string> peripherals;
        /** List of folders in which are stored the strategies */
        std::vector<std::string> strategiesFolders;

        /**
         * @brief Checks if a JSON object matches with this object
         */
        static bool isValid(json object);
        /**
         * @brief Loads this object from a JSON object
         */
        LuaConfig &operator=(json object);
        /**
         * @brief Dumps this object in a JSON object
         */
        json dump();
    };


    struct CockpitConfig {
        /** Host IPv4 address of the HMI  */
        std::string hmiAddress = "127.0.0.1";

        /** Port of the HMI */
        uint16_t hmiPort = 42012;

        /** Port of the client */
        uint16_t port = 42042;
        
        /**
         * @brief Checks if a JSON object matches with this object
         */
        static bool isValid(json object);

        /**
         * @brief Loads this object from a JSON object
         */
        CockpitConfig& operator=(json object);

        /**
         * @brief Dumps this object in a JSON object
         */
        json dump();
    };



    struct StratManagerConfig {

        /**
         * @brief Game time in seconds.
         */
        uint32_t timeout=100;
        /**
         * @brief Number of restore attempts before failure.
         */
        uint8_t  maxRetry=5;
        /**
         * @brief Time between attempts.
         */
         uint32_t timeBetweenRetryMs=250;
        /**
         * @brief Backup strategy to be used during by the manager.
         */
        std::string backupMode="AUTO";

        /**
         * @brief Checks if a JSON object matches with this object
         */
        static bool isValid(json object);
        /**
         * @brief Loads this object from a JSON object
         */
        StratManagerConfig &operator=(json object);
        /**
         * @brief Dumps this object in a JSON object
         */
        json dump();
    };

    class Config {

        private:
        /**
         * @brief Location of the config file
         */
        std::string filePath = "/var/core/config.json";

        public:
        /**
         * @brief Settings to setup Hermes
         */
        HermesConfig hermes;

        /**
         * @brief Settings to setup the lua environment
         */
        LuaConfig lua;

        /**
         * @brief Settings to setup the stratmanager
         */
        StratManagerConfig stratManager;

        /**
         * @brief Settings for cockpit
         */
        CockpitConfig cockpit;

        Config ();

        Config (std::string path);

        /**
         * @brief Dumps the config to the JSON format
         * 
         * @param compact Should the JSON be minified
         * @return std::string The JSON config file
         */
        std::string dump (bool compact = false);

        /**
         * @brief Saves the JSON dump to the config file
         * 
         * @return false if the path is invalid
         */
        bool save ();

        /**
         * @brief Loads the config from the config file. Tries to create the 
         * file if missing.
         * 
         * @return false if the path is invalid
         */
        bool load ();

        /**
         * @brief Set the path of the config file. 
         * 
         * @param path The path to set
         * @return true The path is valid,
         * @return false The path is invalid
         */
        bool setPath (std::string path);

    };

}
