#include "timedLoop.hpp"
#include <time.h>
#include <chrono>
#include <thread>
#include <cmath>

namespace Core::Generic {

    void TimedLoop::stop () {
        running = false;
    }

    void TimedLoop::run () {

        // Initializing the loop        
        bool loopContinues = true;
        running = true;

        // We step and check each time if we must continue
        while (running && loopContinues) {
            loopContinues = step();
        }

    }

    bool TimedLoop::step () {
                
        std::chrono::time_point<std::chrono::system_clock> start, end;
        std::chrono::duration<double> elapsed_seconds, delay;

        bool shouldContinue;
        
        start = std::chrono::system_clock::now();

        // Running one loop
        shouldContinue = loop();

        // Calculating time delta        
        end = std::chrono::system_clock::now();
        elapsed_seconds = end - start;

        // Sleeping the missing time
        delay = std::chrono::duration<double> {1.0 / maxTPS};
        if (delay > elapsed_seconds) {
            // We took less time than a tick, let's sleep
            std::this_thread::sleep_for(
                std::chrono::milliseconds(
                    (int) (1e3 * (delay - elapsed_seconds).count())
                )
            );    
        }

        // Calculating the TPS
        end = std::chrono::system_clock::now();
        elapsed_seconds = end - start;
        tps = 1.0 / elapsed_seconds.count();

        return shouldContinue;

    }

    bool TimedLoop::loop () {
        // Do nothing by default
        // But indicates that the loop shall continue
        return true;
    }

}