#pragma once
#include <mutex>
#include <vector>
#include <thread>
#include <semaphore>
#include <iostream>
class Sync {
private:
    std::mutex internalLock;

    bool released = false;

    std::mutex mainLock;

    std::mutex priorityLock;

    std::counting_semaphore<1>* sem;

    /**
     * Number of locked threads.s
     */
    uint32_t nLocked = 0;

    std::vector<std::thread::id> subscribers;

    std::thread::id prioritySubscriber;

    bool priorityEnabled = false;

    bool priorityWaiting = false;

    /**
     * Checks if a specified pid is subscribed to the Sync.
     * @param pid
     * @return True if the thread's pid is subscribed, false otherwise.
     */
    bool isSubscribed(std::thread::id pid);

public:
    Sync();
    ~Sync();

    /**
     * Remains locked until all subscribed threads reach this method.
     * When it happens, the first to have reached this method will be unlocked.
     * If a thread reaches this method while having the priority, he will be the next to be freed.
     */
    void yield();

    /**
     * Subscribe the calling thread to the Sync. yield() will not lock.
     * @return True if success, false if already subscribed.
     */
    bool subscribe();

    /**
     * Unsubscribe the calling thread from the Sync.
     * @return True if success, false if not subscribed.
     */
    bool unsubscribe();

    /**
     * The calling thread takes the priority on every yield() until it releases it.
     * @return True if the thread takes the priority, false if another thread took it already.
     */
    bool takePriority();

    /**
     * The calling thread does not have the priority anymore.
     * @return True if the priority has been released, false if the calling thread does not have the priority.
     */
    bool releasePriority();
};