#pragma once

#include <string>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdexcept>

using namespace std;

namespace Core::Generic {

    class Semaphore {

        private:
            key_t key;
            void _initNSemaphore (int n, key_t key);

        public:
            int id;
            int n;
            
            Semaphore (int n = 1);
            Semaphore (int n, string filePath);

            ~Semaphore ();

            void P (int n);
            void V (int n);

    };

}