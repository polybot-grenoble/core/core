#include "config.hpp"

namespace Core::Generic {

    bool HermesUDPClient::isValid (json data) {
        return (
                data.is_object()
            &&  (data.contains("ip") && data["ip"].is_string())
            &&  (data.contains("port") && data["port"].is_number())
            &&  (data.contains("id") && data["id"].is_number())
        );
    }

    HermesUDPClient &HermesUDPClient::operator= (json object) {

        // Prevent segfault
        if (!HermesUDPClient::isValid(object)) return *this;

        address = object["ip"];
        port = object["port"];
        hermesID = object["id"];

        return *this;

    }

    json HermesUDPClient::dump () {
        json out;
        out["ip"] = address;
        out["port"] = port;
        out["id"] = hermesID;
        return out;
    }

    bool HermesConfig::isValid(json object) {
        return (
                object.is_object()
            &&  (object.contains("id") && object["id"].is_number())
            &&  (
                    object.contains("can-iface") 
                &&  object["can-iface"].is_string()
                )
            &&  (
                    object.contains("udp-port") 
                &&  object["udp-port"].is_number()
                )
            &&  (
                    object.contains("udp-clients")
                &&  object["udp-clients"].is_array()
                )
        );
    }

    HermesConfig &HermesConfig::operator= (json object) {

        // Prevent segfault
        if (!HermesConfig::isValid(object)) return *this;

        hermesID = object["id"];
        canInterface = object["can-iface"];
        UDPPort = object["udp-port"];
        
        // UDP Clients
        HermesUDPClient temp;
        UDPClients.clear();
        for (auto client : object["udp-clients"]) {
            if (HermesUDPClient::isValid(client)) {
                temp = client;
                UDPClients.push_back(temp);
            }
        }

        return *this;

    }

    json HermesConfig::dump () {
        json out;
        out["id"] = hermesID;
        out["can-iface"] = canInterface;
        out["udp-port"] = UDPPort;
        out["udp-clients"] = json::array({});
        for (size_t i = 0; i < UDPClients.size(); i++) {
            out["udp-clients"][i] = UDPClients[i].dump();
        }
        return out;
    }

    bool LuaConfig::isValid (json object) {
        return (
                object.is_object()
            &&  object["libraries"].is_array()
            &&  object["families"].is_object()
            &&  object["peripherals"].is_object()
            &&  object["strategies"].is_array()
        );
    }

    LuaConfig &LuaConfig::operator= (json object) {

        if (!isValid(object)) return *this;

        // Loading library folders
        for (auto &lib : object["libraries"].items()) {

            if (!lib.value().is_string()) {
                // Ignore wrong values
                continue;
            }

            libraryFolders.push_back(lib.value());

        }

        // Loading peripheral families
        for (auto &family : object["families"].items()) {

            if (!family.value().is_string()) {
                // Discard wrong types
                continue;
            }

            families[family.key()] = family.value();

        }

        // Loading peripherals
        for (auto &peripheral : object["peripherals"].items()) {

            if (!peripheral.value().is_string()) {
                // Discard wrong types
                continue;
            }

            auto k = std::stoi(peripheral.key());
            std::string v = peripheral.value(); 

            if (k < 0 || k > 255) {
                // Discard wrong ids
                continue;
            }

            peripherals[k] = v;

        }

        // Loading strats folders
        for (auto &folder : object["strategies"].items()) {

            if (!folder.value().is_string()) {
                // Discard wrong types
                continue;
            }

            strategiesFolders.push_back(folder.value());

        }

        return *this;

    }

    json LuaConfig::dump () {

        json out;

        // Dumping library folders
        auto libs = json::array();
        for (auto &lib : libraryFolders) {
            libs.push_back(lib);
        }
        out["libraries"] = libs;

        // Dumping peripheral families
        auto fams = json::object();
        for (auto &[name, file] : families) {
            fams[name] = file;
        }
        out["families"] = fams;

        // Dumping peripherals
        auto peris = json::object();
        for (auto &[id, file] : peripherals) {
            peris[std::to_string(id)] = file;
        }
        out["peripherals"] = peris;

        // Dumping strategie folders
        auto strats = json::array();
        for (auto &strat : strategiesFolders) {
            strats.push_back(strat);
        }
        out["strategies"] = strats;

        return out;

    }


    
    bool CockpitConfig::isValid(json object) {

        return (
                object.is_object()
            &&  (object.contains("hmiAddress") && object["hmiAddress"].is_string())
            &&  (object.contains("hmiPort") && object["hmiPort"].is_number())
            &&  (object.contains("port") && object["port"].is_number())
        );

    }

    CockpitConfig& CockpitConfig::operator=(json object) {

        if (!isValid(object)) {
            return *this;
        }

        hmiAddress = object["hmiAddress"];
        hmiPort = object["hmiPort"];
        port = object["port"];

        return *this;

    }

    json CockpitConfig::dump() {

        json obj;

        obj["hmiAddress"] = hmiAddress;
        obj["hmiPort"] = hmiPort;
        obj["port"] = port;

        return obj;

    }



    bool StratManagerConfig::isValid(json object){
        return(
                object.is_object()
                && (object.contains("timeout") && object["timeout"].is_number())
                && (object.contains("maxRetry") && object["maxRetry"].is_number())
                && (object.contains("timeBetweenRetryMs") && object["timeBetweenRetryMs"].is_number())
                && (object.contains("backupMode") &&
                        (object["backupMode"] == "SIMPLE" || object["backupMode"] == "AUTO"))
        );
    }

    StratManagerConfig &StratManagerConfig::operator=(json object){
        if(!StratManagerConfig::isValid(object)) return *this;

        timeout     = object["timeout"];
        maxRetry    = object["maxRetry"];
        timeBetweenRetryMs = object["timeBetweenRetryMs"];
        backupMode  = object["backupMode"];

        return *this;
    }

    json StratManagerConfig::dump(){
        json out;

        out["timeout"]      = timeout;
        out["maxRetry"]     = maxRetry;
        out["timeBetweenRetryMs"] = timeBetweenRetryMs;
        out["backupMode"]   = backupMode;

        return out;
    }

    Config::Config () {
        
    }

    Config::Config (std::string path): filePath(path) {
        Config();
    }

    std::string Config::dump (bool compact) {

        json out;

        out["hermes"] = hermes.dump();
        out["lua"] = lua.dump();
        out["stratManager"] = stratManager.dump();
        out["cockpit"] = cockpit.dump();

        return out.dump(compact ? -1 : 4);

    }

    bool Config::save () {

        using namespace std;
        filesystem::path path (filePath);

        // Checks if parent folder exists
        if (!filesystem::exists(path.parent_path())) {
            // Tries to create the folder tree
            try {
                if (!filesystem::create_directories(path.parent_path())) {
                    return false;
                }
            } catch (filesystem::filesystem_error &e) {
                return false;
            }
        } else if (filesystem::is_directory(path)) {
            // We can't write text in a folder
            return false;
        }

        // Saves the file
        ofstream file;
        file.open(filesystem::absolute(path), ios::out);

        // Failed to open the file
        if (file.bad()) {
            return false;
        }

        file << dump() << endl;

        file.close();

        return true;

    }

    bool Config::load () {

        using namespace std;

        filesystem::path path (filePath);

        if (
                !filesystem::exists(path) 
            ||  !filesystem::is_regular_file(path)
        ) {

            // Tries to create the file
            return save();
            
        }

        // Loads the file
        ifstream file;
        file.open(filesystem::absolute(path));

        ostringstream filestream;
        std::string contents;
        
        filestream << file.rdbuf();
        contents = filestream.str();

        file.close();

        // Parsing the file
        json data;
        try {
            data = json::parse(contents);
        } catch (json::parse_error &e) {
            return false;
        }

        // Loading the config
        hermes = data["hermes"];
        lua = data["lua"];
        stratManager = data["stratManager"];
        cockpit = data["cockpit"];

        return true;

    }

    bool Config::setPath (std::string path) {

        filePath = path;
        return load();

    }

}