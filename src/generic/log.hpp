#pragma once
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>
#include <filesystem>
#include <fstream>
#include "logLevel.hpp"
#include <ctime>
#include <semaphore>
#include <chrono>
using namespace std;
using namespace std::filesystem;
using namespace std::chrono;
using json = nlohmann::json;

namespace Core::Generic {
    class Log{
        private :
            /**
             * @brief Function which open or create the folder where the logs
             * will be written
             * @return a boolean to confirm if the folder has been created
             * or opened
             */
            bool folderInit();

            /**
             * @brief A function which creates a string with the current date
             * and hour
             * @return a string containing the current date and hour
             */
            string currentDateTime();

            /**
             * @brief a boolean saying if the log file has been opened
             */
            bool logOpened;

            /**
             * @brief the flux to write in the log file
             */
            ofstream fichLog;

            /**
             * @brief the flux to write in the json file
             */
            ofstream fichJson;

            /**
             * @brief the object json whiwh will be dumped in the json file
             */
            json logs;

            /**
             * @brief The path where you write the log
             */
            string folder;

            /**
             * @brief Semaphore which avoid the log's overwriting
             */
            binary_semaphore writing{1};

            /**
             * @brief Function to obtain the timestamp for the json file
             * @return the timestamp in int format
             */
            int timeSinceEpochMillisec();

        public :
            /**
             * @brief Constructor for the Log object
             * @param newFolder the folder where you write the .log and the .json
             */
            Log(string newFolder);

            /**
             * @brief Destructor of the object
             */
            ~Log();

            /**
             * @brief Function to write a log message in the .log, the .json and the terminal
             * @param message The message of the log
             * @param level The level of importance of the log
             */
            void writeInLog(string message, LogLevel level);

            /**
             * @brief A function where to write the log message to see it appears on the robot
             */
            Generic::LogHandler logHandler = nullptr;
    };
}