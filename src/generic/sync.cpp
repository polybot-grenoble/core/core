#include "sync.hpp"

Sync::Sync(){
    // Make sure the first thread to yield will be locked.
    sem = new std::counting_semaphore<1>(1);
    mainLock.lock();
    priorityLock.lock();
}

Sync::~Sync() {
    free(sem);
    mainLock.unlock();
    priorityLock.unlock();
    // YOLO
}

void Sync::yield() {
//    internalLock.lock();
//
//    if(subscribers.size() == 1){
//        // If the thread is not subscribed, do not lock it.
//        // If there is only subscribed thread, do not lock it otherwise it's dead.
//        internalLock.unlock();
//        return;
//    }
//
//    nLocked++;
//
//    //Unlock the one waiting.
//    if(nLocked == subscribers.size()){
//        mainLock.unlock();
//    }
//    internalLock.unlock();
//
//
//    // The subscriber arrived, lock it.
//    mainLock.lock();
//    //Same behaviour as for priorityLock, see above scope.
//    mainLock.unlock();
//
//    internalLock.lock();
//    nLocked--;
//    internalLock.unlock();

//bool sub = isSubscribed(std::this_thread::get_id());


    internalLock.lock();

    if(subscribers.size() == 1){
        // If the thread is not subscribed, do not lock it.
        // If there is only subscribed thread, do not lock it otherwise it's dead.
        internalLock.unlock();
        return;
    }

    nLocked++;

    if(nLocked == subscribers.size()){
        released = true;
        internalLock.unlock();
        std::cout << "release" << std::endl;
        sem->release();
        mainLock.lock();
    }else{
        internalLock.unlock();
    }

    sem->acquire();
    internalLock.lock();
    nLocked--;
    if(released){
        released = false;
        internalLock.unlock();
        mainLock.unlock();
    }else{
        internalLock.unlock();
    }


}

bool Sync::subscribe() {
    //if(isSubscribed(std::this_thread::get_id())) {
    //    return false;
    //}

    internalLock.lock();
    subscribers.push_back(std::this_thread::get_id());
    internalLock.unlock();

    return true;
}

bool Sync::unsubscribe() {
    internalLock.lock();

    for(auto it = subscribers.begin(); it != subscribers.end();){
        if(*it == std::this_thread::get_id()){
            subscribers.erase(it);
            internalLock.unlock();

            mainLock.unlock(); // If a thread is yielded, it needs to be free.

            return true;
        }
    }

    internalLock.unlock();
    return false;
}

bool Sync::isSubscribed(std::thread::id pid) {
    internalLock.lock();

    for(auto it = subscribers.begin(); it != subscribers.end();){
        if(*it == pid){
            internalLock.unlock();
            return true;
        }
    }

    internalLock.unlock();
    return false;
}

bool Sync::takePriority() {
    internalLock.lock();

    if(prioritySubscriber != std::this_thread::get_id()){
        return false;
    }

    priorityEnabled = true;
    prioritySubscriber = std::this_thread::get_id();

    internalLock.unlock();
    return true;
}

bool Sync::releasePriority() {
    internalLock.lock();

    if(prioritySubscriber != std::this_thread::get_id()){
        return false;
    }

    priorityEnabled = false;

    internalLock.unlock();
    return true;
}