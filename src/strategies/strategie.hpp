#pragma once
#include <string>
#include <vector>
#include <map>
#include "peripheralManager.hpp"

namespace Core::Strats {

    class Strategie {

        protected:
        /**
         * @brief Did we have an error during execution ?
         */
        bool errored = false;

        public:
        /**
         * Strategie name
        */
        std::string name = "o7";

        /**
         * List of the required peripherals
        */
        std::vector<std::string> requiredPeripherals;

        /**
         * Method that allows the peripheral to log things 
         */
        Generic::LogHandler logHandler = nullptr;

        /**
         * Internal peripheral manager reference 
        */
        Core::Peripherals::Manager &manager;

        /**
         * Peripheral cache
         */
        std::map<std::string, shared_ptr<Peripherals::Peripheral>> peripherals;

        /**
         * @brief Name of the fallback strategie. Can be an empty string if 
         * there is no fallback. 
         */
        std::string fallback = "";

        /**
         * Team number for this strategie
         * 
         * TEAM 0 IS INVALID
        */
        int team = 0;

        /**
         * @brief Latest error message
         */
        std::string latestErrorMessage = "";

        /**
         * Callback for the yield function
        */
        std::function<void(void)> yieldCallback = nullptr;

        /**
         * @brief Construct a new Strategie object
         * 
         * @param manager The peripheral manager
         */
        Strategie (Core::Peripherals::Manager &manager);

        /**
         * @brief Copies a Strategie object
         * @param strat The object to copy
         */
        Strategie (Strategie &strat);

        /**
         * @brief Caches the references to the used peripherals in a map
         * 
         * @return `true`  If all peripherals were successfully cached, 
         *         `false` If one or more peripherals are missing
         */
        bool setup ();

        /**
         * @brief Clears the peripheral cache
         */
        void unsetup ();

        /**
         * Init function for the strategie, executed before the 
         * tirette is pulled
        */
        virtual void init ();

        /**
         * Init function for a strategie ONLY when used as a fallback after
         * an other strategie just failed
         */
        virtual void panic ();

        /**
         * Main program of the strategie, after the tirette is pulled
        */
        virtual void main ();

        /**
         * Bit of code called each tick of the main loop
        */
        virtual void loop ();

        /**
         * Function called after the strategie has run
         */
        virtual void onEnd();

        /**
         * Checks wether all required peripherals are available
        */
        bool requirementsMet ();

        /**
         * Tries to reset faulty peripherals.
        */
        void restore ();

        /* Add logs 
         * @brief Logs a message to the console
         * 
         * @param message The message to log
         * @param level The level to log the message
         */
        void log (
            std::string message, 
            Core::Generic::LogLevel level = Core::Generic::LOG
        );

        /**
         * @brief Is this class wrapped by Lua ?
         */
        bool isLua();
        
        /**
         * @return There was an error during use
         */
        bool isErrored();

        /**
         * @brief Markes the strategie sleep
         * @param ms The number of milliseconds of sleep time 
         */
        void sleep(int ms);

        /**
         * @brief Gives the current epoch in milliseconds
         * @return The current epoch in milliseconds
         */
        int millis();

        void yield();

    };

}
