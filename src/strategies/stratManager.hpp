#pragma once

#include <chrono>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "strategie.hpp"
#include "logLevel.hpp"

using namespace std;

namespace Core::Strats {

/**
 * @brief Available strategie manager backup modes.
 */
typedef enum {
    BACKUP_MODE_SIMPLE/**Selects a backup strategies registered in the
                         * current one.*/,
    BACKUP_MODE_AUTO /**Looks out for the next available strategie, even if it
                        is not registered.*/
} StrategieManagerBackupMode;


/**
 * @brief The goal of this manager is to control strategies execution flow.
 * At instantiation, it starts a startWatchdog thread. This thread automatically tries to fix faulty strategies
 * and it can switch to a backup strategie if needed.
 * More details can be found in CoreDoc.
 */
class Manager {
   protected:

    /*=================== BEGIN OF STRATEGIES VARIABLES =================== */

    /**
     * @brief Map of all strategies.
     * Key : Strategie name
     * Value : Strategie class
     */
    map<string, shared_ptr<Strategie>> strategies;

    /**
     * @brief Selected strategie.
     */
    optional<shared_ptr<Strategie>> strategie = nullopt;

    /**
     * @brief True if the selected strategie is ready (init() or panic() ended).
     */
    bool stratReady = false;

    /**
     * @brief True if the selected strategie is running.
     */
    bool stratRunning = false;

    /**
     * @brief Thread used to initialize the selected strategie.
     * It calls init() or panic() depending on backup.
     */
    thread initThread;

    /**
     * @brief Thread used to execute the main() function of the running strategie.
     */
    thread runThread;

    /**
     * @brief Thread used to execute loop() of the running strategie.
     * DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS.
     */
    //thread loopThread;

    /*=================== END OF STRATEGIES VARIABLES =================== */

    /*=================== BEGIN OF WATCHDOG VARIABLES =================== */

    /**
     * @brief Thread used by the startWatchdog.
     */
    thread watchdogThread;

    /**
     * @brief Variable to stop the watchdog thread.
     */
    bool watchdogRunning = false;

    /**
     * @brief True if the system switched to backup mode.
     */
    bool backup = false;

    /**
     * @brief Number of restore attempts applied to the running strategie.
     */
    uint8_t restoreAttempt = 0;

    /**
     * @brief Strategie's start time.
     */
    chrono::time_point<chrono::system_clock> startTime;

    /*=================== END OF WATCHDOG VARIABLES =================== */

    /**
     * @brief Mutex used to access private variables shared between threads.
     */
    std::mutex internal_mutex;

    /**
     * @brief Mutex to make sure stop() is called one at a time.
     */
     std::mutex stop_lock;

     /**
      * @brief PeripheralManager adress used to communicate with peripherals.
      */
     Peripherals::Manager& peripheralManager;

    /**
    * @brief Starts the watchdog thread.
    */
    void startWatchdog();

    /**
    * @brief Checks if the running strategie is alive and tries to restore it if not.
    * @brief Called by the startWatchdog.
    * @return True if OK or not started.
    * @return False if strategie failed.
    */
    bool tick(shared_ptr<Strategie> localStrategie);

    /**
     * @brief Starts the strategie loop thread.
     * DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS.
     */
    //void startLoopThread();

   public:

    /**
     * @brief Lock to synchronize the main thread and the Lua strategie main() function.
     * Lua items can only be accessed by one person at a time. Otherwise SEGFAULT.
     * We had too much headaches about this.
     */
    Generic::YieldLock& globalLock;

    /*
     * @brief Maximum strategie duration.
     * Defined at instantiation.
     */
    const chrono::seconds TIMEOUT;

    /*
     * @brief Maximum number of strategy restore attempts before canceling the selected strategie.
     * Defined at instantiation.
     */
    const uint8_t MAX_ATTEMPT;

    /**
     * @brief Time between two restore attempt.
     * Defined at instantiation.
     */
    const chrono::milliseconds TIME_BETWEEN_ATTEMPT;

    /**
     * @brief Manager backup mode. Specifies which backup strategie to select.
     * Defined at instantiation.
     */
    const StrategieManagerBackupMode BACKUP_MODE;

    /**
     * @brief Method that allows the strategie manager to log things
     */
    Generic::LogHandler logHandler = nullptr;

    /**
     * @brief Called when the strategie cannot be restored.
     * Can be used to control higher level logic like Giga Talos.
     */
    std::function<void()> onFailure = nullptr;

    /**
     * @brief Team id used to filter strategies.
     */
    int team = 0;

    /**
     * @brief Manages strategies execution flow and backup mechanism.
     * @param timeout Game duration (ie: 100 seconds)
     * @param maxRetry Maximum number of restore retry before backup mode is
     * enabled.
     */
    Manager(Peripherals::Manager& peripheralManager,
            chrono::seconds timeout = chrono::seconds(100),
            uint8_t maxRetry = 5,
            chrono::milliseconds timeBetweenRetry = chrono::milliseconds(250),
            StrategieManagerBackupMode mode = BACKUP_MODE_SIMPLE);

    /**
     * @brief destroys the class and stops running threads.
     */
    ~Manager();

    /**
     * @brief Get the current strategie.
     */
    optional<shared_ptr<Strategie>> getCurrentStrategie();

    /**
     * @brief Trie to select the specified strategie.
     * @return True if success, false if failure.
     */
    bool setCurrentStrategie(string stratName);

    /**
     * @brief Tries to find the next available strategies.
     * Based on their requierements.
     * @return nullopt if no strategie is available.
     */
    optional<shared_ptr<Strategie>> findBackupStrategie();

    /**
     * @brief List strategies based on team and peripheral requirements.
     */
    string list();

    /**
     * @brief tries to add a strategie to the map.
     * @param S Strategie to be added.
     */
    bool add(const shared_ptr<Strategie> &S);

    /**
     * @brief tries to get the specified strategie.
     * Checks for team id and peripheral requirements.
     */
    optional<shared_ptr<Strategie>> get(const string &strategieName);

    /**
     * @brief Empties the strategies map.
     */
    void empty();

    /**
     * @brief Tries to init the selected strategie.
     */
    bool init();

    /**
     * @brief Tries to run the selected strategie.
     */
    bool run();

    /**
     * @brief Tries to stop the selected strategie.
     */
    void stop();

    /**
     * @brief Resets the manager to its initial state.
     * Stops all the strategie related threads, except the startWatchdog.
     */
    void reset();

    /**
     * @brief Checks if the current strategie is ready.
     */
    bool isReady();

    /**
     * @brief Checks if the current strategie is running.
     */
    bool isRunning();

    /**
     * @brief Check if the elapsed time exceeded TIMEOUT.
     */
    bool hasTimeout();

    /**
     * @brief Checks if the manager went into backup mode.
     */
    bool isBackupMode();

    /**
     * @brief Get the elapsed time since the beginning of the selected strategie.
     * It is not reset when a backup strategie is used.
     * @return Duration in seconds.
     */
    chrono::duration<double> getElapsedTime();

    /**
     * @brief Logs a message with the [StrategieManager] tag and specified level.
     * @param message Message to log
     * @param level Message severity
     */
    void log(const string &message, Generic::LogLevel level= Generic::LOG);

    /**
     * @brief Logs the manager configuration.
     */
    void logConfiguration();

    /**
     * @brief Generates a summary of actions since the last reset.
     */
    void summary();

    /**
     * @brief Runs the current strategie loop function
     * (if the strategie is running)
     * DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS AND HEADACHES.
     */
     // void runLoop();
};

}  // namespace Core::Strats