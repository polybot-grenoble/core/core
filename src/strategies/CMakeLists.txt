include_directories(${CMAKE_SOURCE_DIR}/lib)

add_library(STRATEGIES
    strategie.cpp
    stratManager.hpp
    stratManager.cpp
)

target_link_libraries(STRATEGIES
    PUBLIC
    TALOS
    Polybot-Hermes
    PERIPHERALS
)

target_include_directories(STRATEGIES PUBLIC
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
)