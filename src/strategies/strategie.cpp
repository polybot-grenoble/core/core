#include "strategie.hpp"

namespace Core::Strats {

    Strategie::Strategie (Core::Peripherals::Manager &manager):
        manager(manager) {
        
    }

    Strategie::Strategie (Strategie &strat):
        name(strat.name),
        requiredPeripherals(strat.requiredPeripherals),
        logHandler(strat.logHandler),
        manager(strat.manager),
        peripherals(strat.peripherals),
        fallback(strat.fallback) {

    }

    bool Strategie::setup () {
        
        // Missing peripherals
        if (!requirementsMet()) {
            log("Requirements not met", Generic::ERROR);
            return false;
        }

        for (auto &id : requiredPeripherals) {
            auto p = manager.find(id);
            // If the peripheral dies between the steps (SHOULD NEVER HAPPEN)
            if (p == nullopt) {
                log("Failed to cache peripheral " + id, Generic::ERROR);
                return false;
            }
            peripherals[id] = p.value();
        }

        errored = false;

        return true;

    }

    void Strategie::unsetup () {

        peripherals.clear();

    }

    void Strategie::init () {
        this_thread::sleep_for(chrono::seconds(1));
    }

    void Strategie::panic () {

    }

    void Strategie::main () {
        while(true){ }
    }

    void Strategie::loop () {

    }

    void Strategie::onEnd() {

    }

    bool Strategie::requirementsMet () {

        bool ok = true;

        for (auto &p : requiredPeripherals) {
            ok = ok && manager.hasPeripheral(p);
        }

        return ok;

    }

    void Strategie::restore() {
        for (auto& p : requiredPeripherals) {
            if (!manager.hasPeripheral(p)) {
                //p does not exist or is errored.

                auto periph = manager.get(p);

                if (periph == nullopt) {
                    //p does not exist.
                    return;
                }

                //p exists, lets reset it.
                periph.value()->reset();
            }
        }
    }
    
    void Strategie::log (
        std::string message, 
        Core::Generic::LogLevel level
    ) {

        yield();
        
        if (logHandler == nullptr) {
            return;
        }

        logHandler("[" + name + "] " + message, level);

    }

    bool Strategie::isLua() {
        return false;
    }

    bool Strategie::isErrored() {
        return errored;
    }

    void Strategie::sleep(int ms) {
        for (int i = 0; i < ms; i++) {
            yield();
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    int Strategie::millis() {

        return std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()
        ).count();

    }

    void Strategie::yield() {
        if (yieldCallback != nullptr) {
            yieldCallback();
        }
    }

}