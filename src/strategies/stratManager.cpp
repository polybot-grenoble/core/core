#include "stratManager.hpp"

#include <sstream>

namespace Core::Strats {

Manager::Manager(Peripherals::Manager& peripheralManager,
                 chrono::seconds timeout, uint8_t maxAttempt,
                 chrono::milliseconds timeBetweenRetry,
                 StrategieManagerBackupMode mode)
    : startTime(chrono::system_clock::now()),
      peripheralManager(peripheralManager),
      globalLock(peripheralManager.globalLock),
      TIMEOUT(timeout), 
      MAX_ATTEMPT(maxAttempt),
      TIME_BETWEEN_ATTEMPT(timeBetweenRetry),
      BACKUP_MODE(mode) {

    startWatchdog();

    // startLoopThread();
    // DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS.
}

Manager::~Manager() {
    stop();

    watchdogRunning = false;

    /*pthread_cancel(loopThread.native_handle());
    if(loopThread.joinable()){
        loopThread.join();
    }*/
    // DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS.

    if(watchdogThread.joinable()){
        watchdogThread.join();
    }
}

optional<shared_ptr<Strategie>> Manager::getCurrentStrategie() {
    return strategie;
}

bool Manager::setCurrentStrategie(string stratName) {
    if (isReady() || isRunning()) {
        log("Could not set strategie. Another one is already ready or running.", Generic::ERROR);
        return false;
    }

    auto strat = get(stratName);

    if (strat == nullopt) {
        log("Unable to select strategie." + stratName + " Not found.", Generic::ERROR);
        return false;
    }

    if(strat.value()->team != team){
        log("Unable to select strategie. Team id does not match.", Generic::ERROR);
        return false;
    }

    if(!strat.value()->requirementsMet()){
        log("Unable to select strategie. Peripheral requirements are not met.", Generic::ERROR);
        return false;
    }

    internal_mutex.lock();
    strategie = strat;
    internal_mutex.unlock();

    log("Selected strategie: " + stratName, Generic::LOG);

    return true;
}

optional<shared_ptr<Strategie>> Manager::findBackupStrategie() {
    switch (BACKUP_MODE) {
        case BACKUP_MODE_SIMPLE: {

            if(strategie != nullopt){
                optional<shared_ptr<Strategie>> nextStrat = get(strategie.value()->fallback);

                while(nextStrat != nullopt){
                    nextStrat = get(nextStrat.value()->fallback);
                }
            }

            break;
        }
        case BACKUP_MODE_AUTO: {

            for (auto &[key, value]: strategies) {
                if (value->requirementsMet()) {
                    return value;
                }
            }

            break;
        }
    }

    log("Could not find a backup strategie.", Generic::ERROR);

    return nullopt;
}

bool Manager::init() {
    auto strat = getCurrentStrategie();

    if (strat == nullopt || isReady() || isRunning()) {
        log("Strategie failed to init.", Generic::ERROR);
        return false;
    }

    strat.value()->setup();

    initThread = thread([&, strat] {
        peripheralManager.globalLock.becomeTarget();

        if (isBackupMode()) {
            strat.value()->panic();
            log("Strategie "+ strategie.value()->name +" finished his panic.");
        } else {
            strat.value()->init();
            log("Strategie "+ strategie.value()->name +" finished his init.");
        }

        internal_mutex.lock();
        stratReady = true;
        internal_mutex.unlock();
    });

    log("Strategie "+ strategie.value()->name +" initiating...");

    return true;
}

bool Manager::run() {
    auto strat = getCurrentStrategie();

    //You must leave enough time between init() and run() because run() requires isReady() to be true.
    if (strat == nullopt || !isReady() || isRunning()) {
        log("Strategie failed to run.", Generic::ERROR);
        return false;
    }

    if (!isBackupMode()) {
        // Set start time at the beginning of the match only.
        internal_mutex.lock();
        startTime = chrono::system_clock::now();
        internal_mutex.unlock();
    }

    runThread = thread([&, strat] {
        peripheralManager.globalLock.becomeTarget();

        internal_mutex.lock();
        stratRunning = true;
        internal_mutex.unlock();

        strat.value()->main();

        internal_mutex.lock();
        stratRunning = false;
        internal_mutex.unlock();

        log("Strategie "+ strategie.value()->name +" finished is run.");
    });

    log("Strategie "+ strategie.value()->name +" running...");
    return true;
}

//void Manager::startLoopThread() {
    // DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS.
    //loopThread = thread([&] {
    //    while(watchdogRunning) {
    //        // Is it running?
    //        if(isRunning()){
    //            // Is it well defined?
    //            auto localStrategie = getCurrentStrategie();
    //            if (localStrategie == nullopt) {
    //                continue;
    //            }

    //            localStrategie.value()->loop();
    //        }

    //        
    //        this_thread::yield();
    //    }
    //});
//}

//void Manager::runLoop() {
// DO NOT TRY TO USE IT. LOOP LOGIC HAS BEEN DISCARDED FOR SEGFAULT REASONS.
//
//    if (!isRunning()) {
//        return;
//    }
//
//    auto localStrategie = getCurrentStrategie();
//    if (localStrategie == nullopt) {
//        return;
//    }
//
//    localStrategie.value()->loop();
//
//}

void Manager::startWatchdog() {
    internal_mutex.lock();
    if(watchdogRunning){
        internal_mutex.unlock();
        return;
    }

    watchdogRunning = true;
    internal_mutex.unlock();

    watchdogThread = thread([&] {
        log("Watchdog up and running.");

        bool up;
        optional<shared_ptr<Strategie>> localStrategie;

        do {
            // Checking if it needs to be stopped.
            internal_mutex.lock();
            up = watchdogRunning;
            internal_mutex.unlock();

            localStrategie = getCurrentStrategie();

            // is it running?
            if (localStrategie == nullopt || !isRunning()) {
                // Prevents the CPU from becoming a frying pan
                this_thread::sleep_for(std::chrono::milliseconds(1));
                continue;
            }

            if(!tick(localStrategie.value())){
                // Strategie is dead. Enable backup
                internal_mutex.lock();
                backup = true;
                internal_mutex.unlock();

                if (onFailure) onFailure();
            }

            this_thread::sleep_for(TIME_BETWEEN_ATTEMPT);
        }while(up);

        log("Watchdog stopped.");
    });
}

bool Manager::tick(const shared_ptr<Strategie> localStrategie) {
    if(localStrategie->isErrored()){
        log("Strategie "+ localStrategie->name +" errored.", Generic::ERROR);
        restoreAttempt = 0;
        return false;
    }

    // Everything alright?
    if(localStrategie->requirementsMet()){
        restoreAttempt = 0;
        return true;
    }

    // Time to escape?
    if (restoreAttempt == MAX_ATTEMPT) {
        log("Strategie "+ localStrategie->name +" cannot be restored.", Generic::ERROR);
        restoreAttempt = 0;
        return false;
    }

    //attempt to restore faulty peripherals linked to the strategie.
    localStrategie->restore();
    restoreAttempt++;

    log("Restore attempt " + to_string(restoreAttempt) + "/" +
        to_string(MAX_ATTEMPT),
        Generic::ERROR);

    return true;
}

void Manager::stop() {
    stop_lock.lock();

    log("Stopping threads...", Generic::LOG);

    //@vulcanix This is run outside of peripheral pause/resume.
    // It could be dangerous, I guess.
    if (strategie != nullopt) {
        strategie.value()->onEnd();
    }

    internal_mutex.lock();
    stratRunning = false;
    stratReady = false;
    internal_mutex.unlock();

    if (initThread.joinable()) {

        globalLock.abort();
        // Abort all pending peripheral requests
        peripheralManager.abort();

        initThread.join();
        globalLock.clearAbort();
    }

    if (runThread.joinable()) {

        globalLock.abort();
        // Abort all pending peripheral requests
        peripheralManager.abort();

        runThread.join();
        globalLock.clearAbort();
    }

    if(strategie != nullopt){
        strategie.value()->unsetup();
    }

    log("Threads stopped.");

    stop_lock.unlock();
}

void Manager::reset() {
    log("Resetting...");

    if (isRunning()) {
        stop();
    }

    summary();

    internal_mutex.lock();

    startTime = chrono::system_clock::now();
    strategie = nullopt;
    restoreAttempt = 0;
    backup = false;
    //team = 0; // <- Please stop, this is aweful at runtime

    internal_mutex.unlock();

    log("Back to default state.");
}

bool Manager::isReady() {
    internal_mutex.lock();
    bool res = stratReady;
    internal_mutex.unlock();

    return res;
}

bool Manager::isRunning() {
    internal_mutex.lock();
    bool res = stratRunning;
    internal_mutex.unlock();

    return res;
}

bool Manager::isBackupMode() {
    internal_mutex.lock();
    bool res = backup;
    internal_mutex.unlock();
    return res;
}

bool Manager::hasTimeout() { return getElapsedTime() >= TIMEOUT; }

chrono::duration<double> Manager::getElapsedTime() {
    internal_mutex.lock();
    chrono::duration<double> computedTime =
        chrono::system_clock::now() - startTime;
    internal_mutex.unlock();

    return computedTime;
}

string Manager::list() {
    ostringstream stream;

    for (auto &[key, s] : strategies) {
        if ((s->team == team) && s->requirementsMet()) {
            stream << s->name << ";";
        }
    }

    return stream.str();
}

bool Manager::add(const shared_ptr<Strategie> &S) {
    if (strategies.contains(S->name)) {
        return false;
    }

    strategies.insert({S->name, S});
    return true;
}

void Manager::empty() {
    strategies.clear();
    log("Strategies map emptied.", Generic::LOG);
}

optional<shared_ptr<Strategie>> Manager::get(const string &strategieName) {
    if (!strategies.contains(strategieName)) {
        return nullopt;
    }

    for (auto &[key, value] : strategies) {
        if (key == strategieName && value->requirementsMet() && value->team == team) {
            return value;
        }
    }

    return nullopt;
}

void Manager::log(const string &message, Generic::LogLevel level) {
    string msg = "[Strategie Manager] " + message;

    if (logHandler != nullptr) {
        // We let logHandler handle the message
        logHandler(msg, level);
        return;
    }

    switch (level) {
        case Generic::LOG:
            msg = "[LOG] " + msg;
            break;
        case Generic::ERROR:
            msg = "[ERROR] " + msg;
            break;
        case Generic::DEBUG:
            msg = "[DEBUG] " + msg;
            break;
    }

    cout << msg << endl;
}

void Manager::logConfiguration() {
    ostringstream sMode;
    sMode << "Mode: " << (BACKUP_MODE == BACKUP_MODE_SIMPLE ? "SIMPLE" : "AUTO");
    ostringstream sTime;
    sTime << "Timeout: " << (TIMEOUT - chrono::seconds(0)).count() << "s";
    ostringstream sAttempts;
    sAttempts << "Attempts: " << to_string(MAX_ATTEMPT);
    ostringstream  sAttemptsTime;
    sAttemptsTime << "Time between attempts: " << (TIME_BETWEEN_ATTEMPT - chrono::milliseconds(0)).count() << "ms";

    log("---Configuration---");
    log(sMode.str());
    log(sTime.str());
    log(sAttempts.str());
    log(sAttemptsTime.str());
    log("---Configuration---");
}

void Manager::summary() {
    if(strategie == nullopt){
        log("---Summary---");
        log("Nothing to show.");
        log("---Summary---");
        return;
    }

    ostringstream sStrat;
    sStrat << "Last strategie: " << strategie.value()->name;
    ostringstream sAttempts;
    sAttempts << "Restore attempts: " << to_string(restoreAttempt);
    ostringstream sBackup;
    sBackup << "Backup: " << to_string(isBackupMode()) << "s";

    log("---Summary---");
    log(sStrat.str());
    log(sAttempts.str());
    log(sBackup.str());
    log("---Summary---");
}

}  // namespace Core::Strats