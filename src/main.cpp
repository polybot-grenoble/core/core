#include <iostream>
#include <string>

//#include "commandParser.hpp"
#include "cockpit.hpp"
//#include "route.hpp"
//#include "mainroute.hpp"
//#include "luaRoute.hpp"

#include "loop-v1.hpp"

#include "config.hpp"
#include "log.hpp"

using namespace std;
using namespace nlohmann;
using namespace Core::Cockpit;

int main (int argc, char **argv) {

    /** Finding .core folder location */
    auto home = std::getenv("HOME");
    auto coreFolderPath = strlen(home)
        ? std::string(home) + "/.core/"
        : "/home/.core/";

    /**
     * Log files
     */
    Core::Generic::Log logger(coreFolderPath + "logs");

    /**
     * Config load
     */
    Core::Generic::Config config;

    if (!config.setPath(coreFolderPath + "config.json")) {
	std::cout << "Could not open " << coreFolderPath << "config.json\n" 
		  << "Please check if there is no typo in the config file." << std::endl;
        return -1;
        // Dummy return right now but we should let the user know.
    }

    Cockpit cockpit(config);

    sol::state Lua;

    cockpit.log("Warming up !");
    Core::Lua::Init(Lua);

    // Log function
    auto log_fn = [&](std::string message, Core::Generic::LogLevel level) {
        logger.writeInLog(message, level);
    };

    logger.logHandler = [&](std::string message, Core::Generic::LogLevel level) {
        switch (level)
        {
        case Core::Generic::LOG:
            cockpit.send("log", message);
            break;
        case Core::Generic::ERROR:
            cockpit.send("log_error", message);
            break;
        case Core::Generic::DEBUG:
            cockpit.send("log_debug", message);
            break;
        default:
            break;
        }
    };

    cockpit.logHandler = log_fn;

    // Init The loop
    Core::Loops::LoopV1 loop(
        config, Lua, cockpit,
        log_fn
    );

    // Running the loop
    loop.run();
            
    return 0;
}
