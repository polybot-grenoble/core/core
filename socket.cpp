#include "socket.hpp"

namespace Core::Sockets {

    SocketReturnCode Socket::open (char *iface) {

        return SOCK_SOCKET_CREATE_FAIL;

    }

    SocketReturnCode Socket::writeData (
        uint32_t id, void *data, uint32_t len
    ) {

        return SOCK_WRITE_FAIL;

    }

    SocketReturnCode Socket::readData (
        uint32_t *id, void *data, uint32_t *len
    ) {

        return SOCK_READ_FAIL;

    }

}